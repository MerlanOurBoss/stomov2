﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtualFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtualFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding>
struct Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>
struct Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D;
// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.Boolean>
struct Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A;
// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String>
struct Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_tE1F0D41563EE092E5E5540B061449FDE88F1DC00;
// System.Func`2<System.Object,System.Object>
struct Func_2_tACBF5A1656250800CE861707354491F0611F6624;
// System.Collections.Generic.IEnumerable`1<UniGLTF.Zip.CentralDirectoryFileHeader>
struct IEnumerable_1_tCEBCB05DA90D14278221EDB1F9FCD1AE91B7B6C5;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath>
struct IEnumerable_1_tFB08D1F3BD39D06909F81B8970F3EE1C5BFCA985;
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath>
struct IEnumerator_1_t310635646FFD6D85F990F91808E4E20E4A401E26;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UniGLTF.ShaderPropExporter.ShaderProps>
struct KeyCollection_t24B272AD3F2FE27C0BDA4583D9F35DDA4ED4A42C;
// System.Collections.Generic.List`1<UniGLTF.Zip.CentralDirectoryFileHeader>
struct List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t4C228DE57804012969575431CFF12D57C875552D;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UniGLTF.ShaderPropExporter.ShaderProps>
struct ValueCollection_t4496DB31D1E9D68C2A2FBD98DBA6916C2DA25995;
// System.Collections.Generic.Dictionary`2/Entry<System.String,UniGLTF.ShaderPropExporter.ShaderProps>[]
struct EntryU5BU5D_t145CF1479EA7091117B6799AEE35A911D01CF6BE;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// UniGLTF.Zip.CentralDirectoryFileHeader[]
struct CentralDirectoryFileHeaderU5BU5D_tE3E09B9BD3F5A2DED9D749E46443AA1B333B3774;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// UniGLTF.ShaderPropExporter.ShaderProperty[]
struct ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// UniGLTF.ShaderPropExporter.SupportedShader[]
struct SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA;
// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// UniGLTF.Zip.CentralDirectoryFileHeader
struct CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2;
// UniGLTF.Zip.CommonHeader
struct CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37;
// System.Text.Decoder
struct Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC;
// System.Text.DecoderFallback
struct DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90;
// System.IO.Compression.DeflateStream
struct DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104;
// System.IO.Compression.DeflateStreamNative
struct DeflateStreamNative_t06B674E1D2EFD46989197EFB1E33E0B6564793CD;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UniGLTF.Zip.EOCD
struct EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380;
// System.Text.EncoderFallback
struct EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2;
// System.Exception
struct Exception_t;
// System.IO.FileNotFoundException
struct FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// UniGLTF.Zip.LocalFileHeader
struct LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.IO.MemoryStream
struct MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.NotImplementedException
struct NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UniGLTF.ShaderPropExporter.PreExportShaderAttribute
struct PreExportShaderAttribute_t8D9809E8CFB4A4557430A5755A66061EB87269F5;
// UniGLTF.ShaderPropExporter.PreExportShadersAttribute
struct PreExportShadersAttribute_tFAC060D43264C6113FE055289B240F2FFC03B885;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2;
// UniGLTF.ShaderPropExporter.ShaderProps
struct ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD;
// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE;
// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B;
// System.String
struct String_t;
// System.Threading.Tasks.Task
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
// System.Type
struct Type_t;
// UniGLTF.UniGLTFException
struct UniGLTFException_t34581B33464F9CE4820C4F4A2A441F7E9C22365F;
// UniGLTF.UniGLTFNotSupportedException
struct UniGLTFNotSupportedException_t67ABEDC648B5033BE849FB4637988BDC4A6FCF19;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UniGLTF.Zip.ZipArchiveStorage
struct ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820;
// UniGLTF.Zip.ZipParseException
struct ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05;
// UniGLTF.UnityPath/<TravserseDir>d__38
struct U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31;
// UniGLTF.UnityPath/<get_ChildDirs>d__40
struct U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74;
// UniGLTF.UnityPath/<get_ChildFiles>d__42
struct U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35;
// UniGLTF.Zip.ZipArchiveStorage/<>c
struct U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84;
// UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3;

IL2CPP_EXTERN_C RuntimeClass* BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CompressionMethod_tBFF2C1E75DB65EAC2D266E0D2892BE12C84728B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_tFB08D1F3BD39D06909F81B8970F3EE1C5BFCA985_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t310635646FFD6D85F990F91808E4E20E4A401E26_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____57B0A7E913A6CBB7C5554095EB31FFE93AB6628DCBEC34C82369E40829116FDC_0_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD;
IL2CPP_EXTERN_C String_t* _stringLiteral02251F8FB149E78081497DCBE92A7B79B41B4D41;
IL2CPP_EXTERN_C String_t* _stringLiteral04013643CAA39E451E8DA36D6C67223A195929B7;
IL2CPP_EXTERN_C String_t* _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40;
IL2CPP_EXTERN_C String_t* _stringLiteral225A37DFF8BC62C67372C9BB874220E7FD56C613;
IL2CPP_EXTERN_C String_t* _stringLiteral238A3EDAA39C6330200DAB01655AF427B9A18243;
IL2CPP_EXTERN_C String_t* _stringLiteral23E4211C15099DBD2E1D188F8EDABE56912B6625;
IL2CPP_EXTERN_C String_t* _stringLiteral2620038B8A71EF21A205CC921576171A3CA9B0F4;
IL2CPP_EXTERN_C String_t* _stringLiteral3223D004C9379F2B3083B62944F7924A92190F90;
IL2CPP_EXTERN_C String_t* _stringLiteral326129114EB43E5A03AD980A3709D55FE7934916;
IL2CPP_EXTERN_C String_t* _stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F;
IL2CPP_EXTERN_C String_t* _stringLiteral38B8C8C0BD25C5963BB0276F350E52AE4F3288D3;
IL2CPP_EXTERN_C String_t* _stringLiteral3D93A89666F831FB9324883A9347EA29365E69DF;
IL2CPP_EXTERN_C String_t* _stringLiteral40BFB095782D36D1B276A40A276C76911EF9318F;
IL2CPP_EXTERN_C String_t* _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE;
IL2CPP_EXTERN_C String_t* _stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21;
IL2CPP_EXTERN_C String_t* _stringLiteral4FC108C6FF0A706B87BCEE07E7584DC45FD16B4B;
IL2CPP_EXTERN_C String_t* _stringLiteral51311F107A22ACB2B9982782F66881085ABC044E;
IL2CPP_EXTERN_C String_t* _stringLiteral51E77BE760A981E17113C4BA376BD85874B58A45;
IL2CPP_EXTERN_C String_t* _stringLiteral52CBE4A5A42509939BB9DB58A4B9A2C9BBA81C1C;
IL2CPP_EXTERN_C String_t* _stringLiteral58D77E1ECF3579ADA2EDE01E1640D1E1CA4A37E8;
IL2CPP_EXTERN_C String_t* _stringLiteral59B259A263D1796F7617C20534034F94A19001FE;
IL2CPP_EXTERN_C String_t* _stringLiteral5EAA19E6B3D79923759AC0F3C611403170C03B32;
IL2CPP_EXTERN_C String_t* _stringLiteral60BECFD9EBE7638FECA5779A69100169C8558400;
IL2CPP_EXTERN_C String_t* _stringLiteral6512EC05424BE00D0370103E600EE9C9C81C4D9E;
IL2CPP_EXTERN_C String_t* _stringLiteral684F9F5743C3D1DE5AF358333F20040FBA298574;
IL2CPP_EXTERN_C String_t* _stringLiteral6ACFE7690361B95BC457B7B5F163EC8F83FC4D37;
IL2CPP_EXTERN_C String_t* _stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E;
IL2CPP_EXTERN_C String_t* _stringLiteral6E08E58AD13E5769D3AFD33FB33C17E306122492;
IL2CPP_EXTERN_C String_t* _stringLiteral72FD210EDBBBC26FA48DE0D21939BDF6630D46E0;
IL2CPP_EXTERN_C String_t* _stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A;
IL2CPP_EXTERN_C String_t* _stringLiteral7D61FA9D9BE7581D7E2EE28C775ABE0D4B8C3D69;
IL2CPP_EXTERN_C String_t* _stringLiteral7F94A8E8715AB28B4A3A63F016AFABE058C94FF0;
IL2CPP_EXTERN_C String_t* _stringLiteral813424E8552DDA36C36D9633CD0EF610A1CC62E6;
IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;
IL2CPP_EXTERN_C String_t* _stringLiteral915923FF23DAC30B2D70516B4F5D56256E060201;
IL2CPP_EXTERN_C String_t* _stringLiteral9BF9FDA83C54B3E4D921C2ABBCF673E2E2E37FBA;
IL2CPP_EXTERN_C String_t* _stringLiteral9D069221DE352372E43A85A2868AE71709AFBC3F;
IL2CPP_EXTERN_C String_t* _stringLiteral9D11759E924BA1F921B14B215821EB4CBEB88859;
IL2CPP_EXTERN_C String_t* _stringLiteralA2A3DF99788A73C2F0FE7267B988915F72D2C1F1;
IL2CPP_EXTERN_C String_t* _stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F;
IL2CPP_EXTERN_C String_t* _stringLiteralAC2205CB3AEC457605CBAE18F9FEEECC950BD105;
IL2CPP_EXTERN_C String_t* _stringLiteralAC5B3304C047D60B4E3EC2809E3CE3FA62191A79;
IL2CPP_EXTERN_C String_t* _stringLiteralB00134732BE2DF9721B4BB74F2FB1BEE05CAFF55;
IL2CPP_EXTERN_C String_t* _stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C;
IL2CPP_EXTERN_C String_t* _stringLiteralB32F137D7398FFB53E5E7ACA2526882ADE8473A6;
IL2CPP_EXTERN_C String_t* _stringLiteralB92EF51E45166C91E2762DB6C9F27C8BD6EBE466;
IL2CPP_EXTERN_C String_t* _stringLiteralB9AD78CBFE96EFE3227B6F467DA563E5F6136C6B;
IL2CPP_EXTERN_C String_t* _stringLiteralC8E54DC0584021FDD77DA842B94FD97F28B5A628;
IL2CPP_EXTERN_C String_t* _stringLiteralCB06293E3070D888955542383617A31852FFF8DF;
IL2CPP_EXTERN_C String_t* _stringLiteralCBCD3D866AF896F9B010A0FEE7F961DBC91A08C5;
IL2CPP_EXTERN_C String_t* _stringLiteralCCE1912E091B2153DFAE28F4F55D34CD3C4EF3D4;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDF17A9BFA8A9CE193E1BCDFA78953986FDA621F1;
IL2CPP_EXTERN_C String_t* _stringLiteralE147FC8F66BE740F2F8674E00CBC75BC21B73934;
IL2CPP_EXTERN_C String_t* _stringLiteralEA079692DED56FA02201B916BF75CCB06628ED3B;
IL2CPP_EXTERN_C String_t* _stringLiteralEAA5A7F71CEAF22FCCBE9ECEEBBD1FF99E220991;
IL2CPP_EXTERN_C String_t* _stringLiteralF0D9104AB624D4BF63F12ED168216DC1948D19B8;
IL2CPP_EXTERN_C String_t* _stringLiteralF318A9CBF6133558944579D6309707D3FF4760E1;
IL2CPP_EXTERN_C String_t* _stringLiteralF42B6EC895E3DC70F8183E72033DF05F5B5CF6D2;
IL2CPP_EXTERN_C String_t* _stringLiteralF62160BCA579C7E0F393DF2FA5C372C809AB4B48;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1__ctor_m8A879E5F534A391C62D3D65CDF9B8F0A7E1AED86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CommonHeader__ctor_mC4C89B79E39052AC7FF022A55C70AF64AC1F2BAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m48A495425B604D5E9111229379AA45DF27806631_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m4C31AFC006F2A82A3464C2C5F620AEE8809B47A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mF1E5D7383BFC26A82A10D42650A4EE167D9B091B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EOCD_FindEOCD_mE6FA16E8F09A0ABAE86EC9D8E519235F3A9B8049_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EOCD_Parse_mF448F8F575BD23EE19FAC2660F16B4528C7D6B05_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Any_TisRuntimeObject_m67CFBD544CF1D1C0C7E7457FDBDB81649DE26847_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_FirstOrDefault_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_m13A276AEA97B6522EBC40FFC60147FB43E1CD02E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_TisString_t_mC7273138476D2A6E6C69BD0A6CA9F902E90B4619_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisString_t_m3B23EE2DD15B2996E7D2ECA6E74696DA892AA194_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Key_m3E0AEB8F3C3953ABFEC50FE77BF3B32552C80D22_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Value_m0E88FF128ADD89043FB8F078C432663C7BCE4648_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mC00BE642E7A70274A2C74253C0E44C69AE0A9F29_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m6BCCFB8857EC3B1BF31B34F9E7CD0B46C8AC0A3C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_Reset_mBEC50C35327A76E8A8AC3A4D1E64B95B8CBF3CCB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CToStringU3Eb__0_0_m5D303FFA4C09E204453E7DCBB16C7762C9358746_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass5_0_U3CGetU3Eb__0_m1B0B45338FC9A07977990BEC148DB1AEC6E8189D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_Reset_mDD69EDB64602CD1FD701CFBF9D380F8D74B2DFEB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_Reset_m7DCD6738D4EB8D0ECB157C454986048E32DBFD17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityPath_Child_m1EF92234D9BD610C0F3ACB6F09FDA397FDB7AEFB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityPath_GetAssetFolder_mCBCA3B9D808ADF3DBEF3DAB1E561253E8F387D9A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ZipArchiveStorage_Get_m7EAD69633A8FD293B8E78A4C2BCCC5BCEE38FC4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* PreExportShaderAttribute_t8D9809E8CFB4A4557430A5755A66061EB87269F5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct PropertyInfoU5BU5D_tD81C248B41D0C76207C42DB9C332DC79F490B1D7;
struct ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
struct SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>
struct Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t145CF1479EA7091117B6799AEE35A911D01CF6BE* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t24B272AD3F2FE27C0BDA4583D9F35DDA4ED4A42C* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t4496DB31D1E9D68C2A2FBD98DBA6916C2DA25995* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<UniGLTF.Zip.CentralDirectoryFileHeader>
struct List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	CentralDirectoryFileHeaderU5BU5D_tE3E09B9BD3F5A2DED9D749E46443AA1B333B3774* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};

// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158  : public RuntimeObject
{
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;
};

// UniGLTF.Zip.CommonHeader
struct CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37  : public RuntimeObject
{
	// System.Text.Encoding UniGLTF.Zip.CommonHeader::Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___Encoding_0;
	// System.Byte[] UniGLTF.Zip.CommonHeader::Bytes
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Bytes_1;
	// System.Int32 UniGLTF.Zip.CommonHeader::Offset
	int32_t ___Offset_2;
	// System.UInt16 UniGLTF.Zip.CommonHeader::VersionNeededToExtract
	uint16_t ___VersionNeededToExtract_3;
	// System.UInt16 UniGLTF.Zip.CommonHeader::GeneralPurposeBitFlag
	uint16_t ___GeneralPurposeBitFlag_4;
	// UniGLTF.Zip.CompressionMethod UniGLTF.Zip.CommonHeader::CompressionMethod
	uint16_t ___CompressionMethod_5;
	// System.UInt16 UniGLTF.Zip.CommonHeader::FileLastModificationTime
	uint16_t ___FileLastModificationTime_6;
	// System.UInt16 UniGLTF.Zip.CommonHeader::FileLastModificationDate
	uint16_t ___FileLastModificationDate_7;
	// System.Int32 UniGLTF.Zip.CommonHeader::CRC32
	int32_t ___CRC32_8;
	// System.Int32 UniGLTF.Zip.CommonHeader::CompressedSize
	int32_t ___CompressedSize_9;
	// System.Int32 UniGLTF.Zip.CommonHeader::UncompressedSize
	int32_t ___UncompressedSize_10;
	// System.UInt16 UniGLTF.Zip.CommonHeader::FileNameLength
	uint16_t ___FileNameLength_11;
	// System.UInt16 UniGLTF.Zip.CommonHeader::ExtraFieldLength
	uint16_t ___ExtraFieldLength_12;
};

// UniGLTF.Zip.EOCD
struct EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380  : public RuntimeObject
{
	// System.UInt16 UniGLTF.Zip.EOCD::NumberOfThisDisk
	uint16_t ___NumberOfThisDisk_0;
	// System.UInt16 UniGLTF.Zip.EOCD::DiskWhereCentralDirectoryStarts
	uint16_t ___DiskWhereCentralDirectoryStarts_1;
	// System.UInt16 UniGLTF.Zip.EOCD::NumberOfCentralDirectoryRecordsOnThisDisk
	uint16_t ___NumberOfCentralDirectoryRecordsOnThisDisk_2;
	// System.UInt16 UniGLTF.Zip.EOCD::TotalNumberOfCentralDirectoryRecords
	uint16_t ___TotalNumberOfCentralDirectoryRecords_3;
	// System.Int32 UniGLTF.Zip.EOCD::SizeOfCentralDirectoryBytes
	int32_t ___SizeOfCentralDirectoryBytes_4;
	// System.Int32 UniGLTF.Zip.EOCD::OffsetOfStartOfCentralDirectory
	int32_t ___OffsetOfStartOfCentralDirectory_5;
	// System.String UniGLTF.Zip.EOCD::Comment
	String_t* ___Comment_6;
};

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095  : public RuntimeObject
{
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2* ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293* ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90* ___decoderFallback_14;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// UniGLTF.ShaderPropExporter.PreShaderPropExporter
struct PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7  : public RuntimeObject
{
};

// UniGLTF.ShaderPropExporter.ShaderProps
struct ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD  : public RuntimeObject
{
	// UniGLTF.ShaderPropExporter.ShaderProperty[] UniGLTF.ShaderPropExporter.ShaderProps::Properties
	ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* ___Properties_0;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// UniGLTF.UniGLTFVersion
struct UniGLTFVersion_t05C0081DAA5DEDD02F8F9B030E004E9A747FB89F  : public RuntimeObject
{
};

// UniGLTF.UniUnlit.Utils
struct Utils_t7EDEA2D5DD2AAF420FA8B21C3C6B47E6CDC5F736  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UniGLTF.Zip.ZipArchiveStorage
struct ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820  : public RuntimeObject
{
	// System.Collections.Generic.List`1<UniGLTF.Zip.CentralDirectoryFileHeader> UniGLTF.Zip.ZipArchiveStorage::Entries
	List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* ___Entries_0;
};

// UniGLTF.Zip.ZipArchiveStorage/<>c
struct U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84  : public RuntimeObject
{
};

// UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3  : public RuntimeObject
{
	// System.String UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0::url
	String_t* ___url_0;
};

// System.ArraySegment`1<System.Byte>
struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 
{
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____array_1;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_2;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_3;
};

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject* ___value_1;
};

// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>
struct KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* ___value_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// UniGLTF.Zip.CentralDirectoryFileHeader
struct CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C  : public CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37
{
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::VersionMadeBy
	uint16_t ___VersionMadeBy_13;
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::FileCommentLength
	uint16_t ___FileCommentLength_14;
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::DiskNumberWhereFileStarts
	uint16_t ___DiskNumberWhereFileStarts_15;
	// System.UInt16 UniGLTF.Zip.CentralDirectoryFileHeader::InternalFileAttributes
	uint16_t ___InternalFileAttributes_16;
	// System.Int32 UniGLTF.Zip.CentralDirectoryFileHeader::ExternalFileAttributes
	int32_t ___ExternalFileAttributes_17;
	// System.Int32 UniGLTF.Zip.CentralDirectoryFileHeader::RelativeOffsetOfLocalFileHeader
	int32_t ___RelativeOffsetOfLocalFileHeader_18;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UniGLTF.Zip.LocalFileHeader
struct LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6  : public CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37
{
};

// UniGLTF.ShaderPropExporter.PreExportShaderAttribute
struct PreExportShaderAttribute_t8D9809E8CFB4A4557430A5755A66061EB87269F5  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UniGLTF.ShaderPropExporter.PreExportShadersAttribute
struct PreExportShadersAttribute_tFAC060D43264C6113FE055289B240F2FFC03B885  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Reflection.PropertyInfo
struct PropertyInfo_t  : public MemberInfo_t
{
};

// UniGLTF.ShaderPropExporter.ShaderProperty
struct ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F 
{
	// System.String UniGLTF.ShaderPropExporter.ShaderProperty::Key
	String_t* ___Key_0;
	// UniGLTF.ShaderPropExporter.ShaderPropertyType UniGLTF.ShaderPropExporter.ShaderProperty::ShaderPropertyType
	int32_t ___ShaderPropertyType_1;
};
// Native definition for P/Invoke marshalling of UniGLTF.ShaderPropExporter.ShaderProperty
struct ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_pinvoke
{
	char* ___Key_0;
	int32_t ___ShaderPropertyType_1;
};
// Native definition for COM marshalling of UniGLTF.ShaderPropExporter.ShaderProperty
struct ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_com
{
	Il2CppChar* ___Key_0;
	int32_t ___ShaderPropertyType_1;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05* ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2* ____asyncActiveSemaphore_3;
};

// UniGLTF.ShaderPropExporter.SupportedShader
struct SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD 
{
	// System.String UniGLTF.ShaderPropExporter.SupportedShader::TargetFolder
	String_t* ___TargetFolder_0;
	// System.String UniGLTF.ShaderPropExporter.SupportedShader::ShaderName
	String_t* ___ShaderName_1;
};
// Native definition for P/Invoke marshalling of UniGLTF.ShaderPropExporter.SupportedShader
struct SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_pinvoke
{
	char* ___TargetFolder_0;
	char* ___ShaderName_1;
};
// Native definition for COM marshalling of UniGLTF.ShaderPropExporter.SupportedShader
struct SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_com
{
	Il2CppChar* ___TargetFolder_0;
	Il2CppChar* ___ShaderName_1;
};

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
};

// System.UInt16
struct UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455 
{
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// UniGLTF.UnityPath
struct UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 
{
	// System.String UniGLTF.UnityPath::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UniGLTF.UnityPath
struct UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_pinvoke
{
	char* ___U3CValueU3Ek__BackingField_0;
};
// Native definition for COM marshalling of UniGLTF.UnityPath
struct UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_com
{
	Il2CppChar* ___U3CValueU3Ek__BackingField_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct __StaticArrayInitTypeSizeU3D16_tFB2D94E174C3DFBC336BBEE6AD92E07462831A23 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_tFB2D94E174C3DFBC336BBEE6AD92E07462831A23__padding[16];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=18
struct __StaticArrayInitTypeSizeU3D18_t6E0330D09464182596CB51323A9C0A4DC19FC9AF 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D18_t6E0330D09464182596CB51323A9C0A4DC19FC9AF__padding[18];
	};
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA  : public RuntimeObject
{
};

// System.IO.Compression.DeflateStream
struct DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104  : public Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE
{
	// System.IO.Stream System.IO.Compression.DeflateStream::base_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___base_stream_4;
	// System.IO.Compression.CompressionMode System.IO.Compression.DeflateStream::mode
	int32_t ___mode_5;
	// System.Boolean System.IO.Compression.DeflateStream::leaveOpen
	bool ___leaveOpen_6;
	// System.Boolean System.IO.Compression.DeflateStream::disposed
	bool ___disposed_7;
	// System.IO.Compression.DeflateStreamNative System.IO.Compression.DeflateStream::native
	DeflateStreamNative_t06B674E1D2EFD46989197EFB1E33E0B6564793CD* ___native_8;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// System.IO.MemoryStream
struct MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2  : public Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE
{
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____buffer_4;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_5;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_6;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_7;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_8;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_9;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_10;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_11;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_12;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t4C228DE57804012969575431CFF12D57C875552D* ____lastReadTask_13;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeFieldHandle
struct RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 
{
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B  : public TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7
{
	// System.IO.Stream System.IO.StreamReader::_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ____stream_3;
	// System.Text.Encoding System.IO.StreamReader::_encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ____encoding_4;
	// System.Text.Decoder System.IO.StreamReader::_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ____decoder_5;
	// System.Byte[] System.IO.StreamReader::_byteBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____byteBuffer_6;
	// System.Char[] System.IO.StreamReader::_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____charBuffer_7;
	// System.Int32 System.IO.StreamReader::_charPos
	int32_t ____charPos_8;
	// System.Int32 System.IO.StreamReader::_charLen
	int32_t ____charLen_9;
	// System.Int32 System.IO.StreamReader::_byteLen
	int32_t ____byteLen_10;
	// System.Int32 System.IO.StreamReader::_bytePos
	int32_t ____bytePos_11;
	// System.Int32 System.IO.StreamReader::_maxCharsPerBuffer
	int32_t ____maxCharsPerBuffer_12;
	// System.Boolean System.IO.StreamReader::_detectEncoding
	bool ____detectEncoding_13;
	// System.Boolean System.IO.StreamReader::_checkPreamble
	bool ____checkPreamble_14;
	// System.Boolean System.IO.StreamReader::_isBlocked
	bool ____isBlocked_15;
	// System.Boolean System.IO.StreamReader::_closable
	bool ____closable_16;
	// System.Threading.Tasks.Task System.IO.StreamReader::_asyncReadTask
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ____asyncReadTask_17;
};

// UniGLTF.UnityPath/<TravserseDir>d__38
struct U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31  : public RuntimeObject
{
	// System.Int32 UniGLTF.UnityPath/<TravserseDir>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<>2__current
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityPath/<TravserseDir>d__38::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<>4__this
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E4__this_3;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<>3__<>4__this
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E3__U3CU3E4__this_4;
	// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::<>s__1
	RuntimeObject* ___U3CU3Es__1_5;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<child>5__2
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CchildU3E5__2_6;
	// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::<>s__3
	RuntimeObject* ___U3CU3Es__3_7;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::<x>5__4
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CxU3E5__4_8;
};

// UniGLTF.UnityPath/<get_ChildDirs>d__40
struct U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74  : public RuntimeObject
{
	// System.Int32 UniGLTF.UnityPath/<get_ChildDirs>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::<>2__current
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityPath/<get_ChildDirs>d__40::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::<>4__this
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E4__this_3;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::<>3__<>4__this
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E3__U3CU3E4__this_4;
	// System.String[] UniGLTF.UnityPath/<get_ChildDirs>d__40::<>s__1
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___U3CU3Es__1_5;
	// System.Int32 UniGLTF.UnityPath/<get_ChildDirs>d__40::<>s__2
	int32_t ___U3CU3Es__2_6;
	// System.String UniGLTF.UnityPath/<get_ChildDirs>d__40::<x>5__3
	String_t* ___U3CxU3E5__3_7;
};

// UniGLTF.UnityPath/<get_ChildFiles>d__42
struct U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35  : public RuntimeObject
{
	// System.Int32 UniGLTF.UnityPath/<get_ChildFiles>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::<>2__current
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E2__current_1;
	// System.Int32 UniGLTF.UnityPath/<get_ChildFiles>d__42::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::<>4__this
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E4__this_3;
	// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::<>3__<>4__this
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 ___U3CU3E3__U3CU3E4__this_4;
	// System.String[] UniGLTF.UnityPath/<get_ChildFiles>d__42::<>s__1
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___U3CU3Es__1_5;
	// System.Int32 UniGLTF.UnityPath/<get_ChildFiles>d__42::<>s__2
	int32_t ___U3CU3Es__2_6;
	// System.String UniGLTF.UnityPath/<get_ChildFiles>d__42::<x>5__3
	String_t* ___U3CxU3E5__3_7;
};

// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// UniGLTF.UniGLTFException
struct UniGLTFException_t34581B33464F9CE4820C4F4A2A441F7E9C22365F  : public Exception_t
{
};

// UniGLTF.Zip.ZipParseException
struct ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7  : public Exception_t
{
};

// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.Boolean>
struct Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A  : public MulticastDelegate_t
{
};

// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String>
struct Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8  : public MulticastDelegate_t
{
};

// System.IO.IOException
struct IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.NotImplementedException
struct NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UniGLTF.UniGLTFNotSupportedException
struct UniGLTFNotSupportedException_t67ABEDC648B5033BE849FB4637988BDC4A6FCF19  : public UniGLTFException_t34581B33464F9CE4820C4F4A2A441F7E9C22365F
{
};

// System.IO.FileNotFoundException
struct FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A  : public IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910
{
	// System.String System.IO.FileNotFoundException::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_18;
	// System.String System.IO.FileNotFoundException::<FusionLog>k__BackingField
	String_t* ___U3CFusionLogU3Ek__BackingField_19;
};

// System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>

// System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>

// System.Collections.Generic.List`1<UniGLTF.Zip.CentralDirectoryFileHeader>
struct List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	CentralDirectoryFileHeaderU5BU5D_tE3E09B9BD3F5A2DED9D749E46443AA1B333B3774* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UniGLTF.Zip.CentralDirectoryFileHeader>

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>

// System.Attribute

// System.Attribute

// System.IO.BinaryReader

// System.IO.BinaryReader

// UniGLTF.Zip.CommonHeader

// UniGLTF.Zip.CommonHeader

// UniGLTF.Zip.EOCD

// UniGLTF.Zip.EOCD

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095_StaticFields
{
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___latin1Encoding_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding> modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54* ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject* ___s_InternalSyncObject_15;
};

// System.Text.Encoding

// System.MarshalByRefObject

// System.MarshalByRefObject

// System.Reflection.MemberInfo

// System.Reflection.MemberInfo

// UniGLTF.ShaderPropExporter.PreShaderPropExporter
struct PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields
{
	// UniGLTF.ShaderPropExporter.SupportedShader[] UniGLTF.ShaderPropExporter.PreShaderPropExporter::SupportedShaders
	SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* ___SupportedShaders_1;
	// System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps> UniGLTF.ShaderPropExporter.PreShaderPropExporter::m_shaderPropMap
	Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* ___m_shaderPropMap_2;
};

// UniGLTF.ShaderPropExporter.PreShaderPropExporter

// UniGLTF.ShaderPropExporter.ShaderProps

// UniGLTF.ShaderPropExporter.ShaderProps

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// UniGLTF.UniGLTFVersion

// UniGLTF.UniGLTFVersion

// UniGLTF.UniUnlit.Utils

// UniGLTF.UniUnlit.Utils

// System.ValueType

// System.ValueType

// UniGLTF.Zip.ZipArchiveStorage

// UniGLTF.Zip.ZipArchiveStorage

// UniGLTF.Zip.ZipArchiveStorage/<>c
struct U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_StaticFields
{
	// UniGLTF.Zip.ZipArchiveStorage/<>c UniGLTF.Zip.ZipArchiveStorage/<>c::<>9
	U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84* ___U3CU3E9_0;
	// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String> UniGLTF.Zip.ZipArchiveStorage/<>c::<>9__0_0
	Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* ___U3CU3E9__0_0_1;
};

// UniGLTF.Zip.ZipArchiveStorage/<>c

// UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0

// UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0

// System.ArraySegment`1<System.Byte>
struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093_StaticFields
{
	// System.ArraySegment`1<T> System.ArraySegment`1::<Empty>k__BackingField
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___U3CEmptyU3Ek__BackingField_0;
};

// System.ArraySegment`1<System.Byte>

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>

// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>

// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// UniGLTF.Zip.CentralDirectoryFileHeader

// UniGLTF.Zip.CentralDirectoryFileHeader

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_StaticFields
{
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___enumSeperatorCharArray_0;
};

// System.Enum

// System.Int32

// System.Int32

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UniGLTF.Zip.LocalFileHeader

// UniGLTF.Zip.LocalFileHeader

// UniGLTF.ShaderPropExporter.PreExportShaderAttribute

// UniGLTF.ShaderPropExporter.PreExportShaderAttribute

// UniGLTF.ShaderPropExporter.PreExportShadersAttribute

// UniGLTF.ShaderPropExporter.PreExportShadersAttribute

// System.Reflection.PropertyInfo

// System.Reflection.PropertyInfo

// UniGLTF.ShaderPropExporter.ShaderProperty

// UniGLTF.ShaderPropExporter.ShaderProperty

// System.Single

// System.Single

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE_StaticFields
{
	// System.IO.Stream System.IO.Stream::Null
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Null_1;
};

// System.IO.Stream

// UniGLTF.ShaderPropExporter.SupportedShader

// UniGLTF.ShaderPropExporter.SupportedShader

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7_StaticFields
{
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* ___Null_1;
};

// System.IO.TextReader

// System.UInt16

// System.UInt16

// System.UInt32

// System.UInt32

// UniGLTF.UnityPath
struct UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields
{
	// System.Char[] UniGLTF.UnityPath::EscapeChars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___EscapeChars_1;
	// System.String UniGLTF.UnityPath::s_basePath
	String_t* ___s_basePath_2;
};

// UniGLTF.UnityPath

// System.Void

// System.Void

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=18

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=18

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA_StaticFields
{
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=18 <PrivateImplementationDetails>::57B0A7E913A6CBB7C5554095EB31FFE93AB6628DCBEC34C82369E40829116FDC
	__StaticArrayInitTypeSizeU3D18_t6E0330D09464182596CB51323A9C0A4DC19FC9AF ___57B0A7E913A6CBB7C5554095EB31FFE93AB6628DCBEC34C82369E40829116FDC_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::F6BB1294DA2F78CD935B01C7656280DF5EAA0439E9D97BC03775825A41A508E4
	__StaticArrayInitTypeSizeU3D16_tFB2D94E174C3DFBC336BBEE6AD92E07462831A23 ___F6BB1294DA2F78CD935B01C7656280DF5EAA0439E9D97BC03775825A41A508E4_1;
};

// <PrivateImplementationDetails>

// System.IO.Compression.DeflateStream

// System.IO.Compression.DeflateStream

// System.Delegate

// System.Delegate

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// System.IO.MemoryStream

// System.IO.MemoryStream

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// System.RuntimeFieldHandle

// System.RuntimeFieldHandle

// System.RuntimeTypeHandle

// System.RuntimeTypeHandle

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_StaticFields
{
	// System.IO.StreamReader System.IO.StreamReader::Null
	StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* ___Null_2;
};

// System.IO.StreamReader

// UniGLTF.UnityPath/<TravserseDir>d__38

// UniGLTF.UnityPath/<TravserseDir>d__38

// UniGLTF.UnityPath/<get_ChildDirs>d__40

// UniGLTF.UnityPath/<get_ChildDirs>d__40

// UniGLTF.UnityPath/<get_ChildFiles>d__42

// UniGLTF.UnityPath/<get_ChildFiles>d__42

// UnityEngine.Material

// UnityEngine.Material

// System.MulticastDelegate

// System.MulticastDelegate

// System.SystemException

// System.SystemException

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// UniGLTF.UniGLTFException

// UniGLTF.UniGLTFException

// UniGLTF.Zip.ZipParseException

// UniGLTF.Zip.ZipParseException

// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.Boolean>

// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.Boolean>

// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String>

// System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String>

// System.IO.IOException

// System.IO.IOException

// System.NotImplementedException

// System.NotImplementedException

// System.NotSupportedException

// System.NotSupportedException

// UniGLTF.UniGLTFNotSupportedException

// UniGLTF.UniGLTFNotSupportedException

// System.IO.FileNotFoundException

// System.IO.FileNotFoundException
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_tD81C248B41D0C76207C42DB9C332DC79F490B1D7  : public RuntimeArray
{
	ALIGN_FIELD (8) PropertyInfo_t* m_Items[1];

	inline PropertyInfo_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PropertyInfo_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PropertyInfo_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline PropertyInfo_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PropertyInfo_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PropertyInfo_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UniGLTF.ShaderPropExporter.ShaderProperty[]
struct ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88  : public RuntimeArray
{
	ALIGN_FIELD (8) ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F m_Items[1];

	inline ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Key_0), (void*)NULL);
	}
	inline ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Key_0), (void*)NULL);
	}
};
// UniGLTF.ShaderPropExporter.SupportedShader[]
struct SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C  : public RuntimeArray
{
	ALIGN_FIELD (8) SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD m_Items[1];

	inline SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___TargetFolder_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___ShaderName_1), (void*)NULL);
		#endif
	}
	inline SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___TargetFolder_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___ShaderName_1), (void*)NULL);
		#endif
	}
};


// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_gshared (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_array, int32_t ___1_offset, int32_t ___2_count, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m67C538A5EBF57C4844107A8EF25DB2CAAFBAF8FB_gshared (RuntimeObject* ___0_source, Func_2_tACBF5A1656250800CE861707354491F0611F6624* ___1_selector, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Enumerable_ToArray_TisRuntimeObject_mA54265C2C8A0864929ECD300B75E4952D553D17D_gshared (RuntimeObject* ___0_source, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m13C0A7F33154D861E2A041B52E88461832DA1697_gshared (Func_2_tE1F0D41563EE092E5E5540B061449FDE88F1DC00* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) ;
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_FirstOrDefault_TisRuntimeObject_mFACC750E4D7AF7B43F5B866C84F613B3ECC41994_gshared (RuntimeObject* ___0_source, Func_2_tE1F0D41563EE092E5E5540B061449FDE88F1DC00* ___1_predicate, const RuntimeMethod* method) ;
// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m8A879E5F534A391C62D3D65CDF9B8F0A7E1AED86_gshared (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_array, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerable_Any_TisRuntimeObject_m67CFBD544CF1D1C0C7E7457FDBDB81649DE26847_gshared (RuntimeObject* ___0_source, const RuntimeMethod* method) ;
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* KeyValuePair_2_get_Key_mBD8EA7557C27E6956F2AF29DA3F7499B2F51A282_gshared_inline (KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230* __this, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* KeyValuePair_2_get_Value_mC6BD8075F9C9DDEF7B4D731E5C38EC19103988E7_gshared_inline (KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mD15380A4ED7CDEE99EA45881577D26BA9CE1B849_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, RuntimeObject** ___1_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KeyValuePair_2__ctor_m0D5C3E0BE2D37252D3F4E0F0EA9A13C9458D4950_gshared (KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230* __this, RuntimeObject* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;

// System.String UniGLTF.UnityPath::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath::set_Value(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6_inline (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8 (String_t* ___0_format, RuntimeObject* ___1_arg0, const RuntimeMethod* method) ;
// System.String UniGLTF.UnityPath::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_ToString_m56BAEA8A30E60050E4ECFA71DA865121BE2F4296 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Boolean UniGLTF.UnityPath::get_IsNull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1 (String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) ;
// System.Boolean System.String::StartsWith(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0 (String_t* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Boolean UniGLTF.UnityPath::get_IsUnderAssetsFolder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsUnderAssetsFolder_mF052E6B9E01854DF1E3B816EBAC238C09CF520CE (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.String System.IO.Path::GetFileNameWithoutExtension(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetFileNameWithoutExtension_m2D14CCBAB9C60DC8D32C2443CCE3D34644822FAF (String_t* ___0_path, const RuntimeMethod* method) ;
// System.String UniGLTF.UnityPath::get_FileNameWithoutExtension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_FileNameWithoutExtension_m033B52ECE15B98E072F5D7C6F32C2161513DE423 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.String System.IO.Path::GetExtension(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetExtension_m6FEAA9E14451BFD210B9D1AEC2430C813F570FE5 (String_t* ___0_path, const RuntimeMethod* method) ;
// System.String UniGLTF.UnityPath::get_Extension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_Extension_m2D86C2FD1704264AE5AF42829096A414E1A484E1 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.String System.IO.Path::GetDirectoryName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetDirectoryName_m428BADBE493A3927B51A13DEF658929B430516F6 (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// UniGLTF.UnityPath UniGLTF.UnityPath::get_Parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_get_Parent_m730217B9BDBCD8088F4B7FE88903FF272146E4D9 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478 (String_t* ___0_value, const RuntimeMethod* method) ;
// System.Boolean UniGLTF.UnityPath::get_HasParent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_HasParent_m4FF133BBE9F4F213758EBA3C8C1C8070AFEBF209 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.String System.String::Replace(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_m86403DC5F422D8D5E1CFAAF255B103CB807EDAAF (String_t* __this, Il2CppChar ___0_oldChar, Il2CppChar ___1_newChar, const RuntimeMethod* method) ;
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B (String_t* ___0_str0, String_t* ___1_str1, String_t* ___2_str2, const RuntimeMethod* method) ;
// UniGLTF.UnityPath UniGLTF.UnityPath::Child(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_Child_m1EF92234D9BD610C0F3ACB6F09FDA397FDB7AEFB (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_name, const RuntimeMethod* method) ;
// System.Int32 UniGLTF.UnityPath::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityPath_GetHashCode_m10C09137D286D528E82BF59E4231E0C9ADEB45C0 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Boolean UniGLTF.UnityPath::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_Equals_m167179E2120861BB5DC8EB6AFB507A16682F2734 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA0534D6E2AE4D67A6BD8D45B3321323930EB930C (String_t* ___0_format, RuntimeObject* ___1_arg0, RuntimeObject* ___2_arg1, RuntimeObject* ___3_arg2, const RuntimeMethod* method) ;
// UniGLTF.UnityPath UniGLTF.UnityPath::GetAssetFolder(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_GetAssetFolder_mCBCA3B9D808ADF3DBEF3DAB1E561253E8F387D9A (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_suffix, const RuntimeMethod* method) ;
// System.String System.String::Replace(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166 (String_t* __this, String_t* ___0_oldValue, String_t* ___1_newValue, const RuntimeMethod* method) ;
// System.String System.IO.Path::GetFullPath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetFullPath_m9E485D7D38A868A6A5863CBD24677231288EECE2 (String_t* ___0_path, const RuntimeMethod* method) ;
// UniGLTF.UnityPath UniGLTF.UnityPath::FromFullpath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_FromFullpath_m9B741FAFE56705ADA157667B1CEB2880293EC6D9 (String_t* ___0_fullPath, const RuntimeMethod* method) ;
// System.String UnityEngine.Application::get_dataPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_dataPath_m4C8412CBEE4EAAAB6711CC9BEFFA73CEE5BDBEF7 (const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9E3155FB84015C823606188F53B47CB44C444991 (String_t* ___0_str0, String_t* ___1_str1, const RuntimeMethod* method) ;
// System.String UniGLTF.UnityPath::get_BaseFullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_BaseFullPath_m375AE31235A8D7E6ED0690803B72F8A93389E0B0 (const RuntimeMethod* method) ;
// System.String System.IO.Path::Combine(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE (String_t* ___0_path1, String_t* ___1_path2, const RuntimeMethod* method) ;
// System.String UniGLTF.UnityPath::get_FullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_m95E329ABBE3EAD6750FE1989BBA6884457136D4A (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Boolean UniGLTF.UnityPath::get_IsFileExists()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsFileExists_mA8B0112A365374EF47945D962BBDC57278320329 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Boolean System.IO.Directory::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Directory_Exists_m3D125E9E88C291CF11113444F961A64DD83AE1C7 (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Boolean UniGLTF.UnityPath::get_IsDirectoryExists()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsDirectoryExists_mA16B832020034FE21285DA9F89B197D33FA6C7B1 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.String System.String::Substring(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m6BA4A3FA3800FE92662D0847CC8E1EEF940DF472 (String_t* __this, int32_t ___0_startIndex, const RuntimeMethod* method) ;
// System.String UniGLTF.UnityPath::get_AssetFullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_AssetFullPath_m68A5F1C1B3EAA3BCF1FFA4C89318DDD050612441 (const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38__ctor_mD53E5B3DB6363090C92EB54659036813FA81557F (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::TravserseDir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_TravserseDir_m7F543EC20E1D086B71902D307A686AD9AC2E02DF (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40__ctor_m075883A29603CBDABA82EA828B9537AB4F2C52BE (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::get_ChildDirs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_get_ChildDirs_mC69F9F662DBBD446BFCC738929BEB776F7FF839B (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42__ctor_m47F9CE437A24FE8B4385933C997775D49A7CB8E9 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::get_ChildFiles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_get_ChildFiles_m8161F17486FA9E8DD3DC054A621494F23D49BDF7 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) ;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B (RuntimeArray* ___0_array, RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 ___1_fldHandle, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Int32 System.Environment::get_CurrentManagedThreadId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Environment_get_CurrentManagedThreadId_m66483AADCCC13272EBDCD94D31D2E52603C24BDF (const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_m836DFCF69E62A1CD912EE3D5A6A97E7E6669BFC7 (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m07C881555177FBA1B9E288AAF6DD15240B6629CF (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_System_IDisposable_Dispose_mDA429B4DB1D8469BB75DC0AE06F726BEED7ADFB4 (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m976C22CB56F3061BF80191C72CE73438C09C61B0 (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) ;
// System.String[] System.IO.Directory::GetDirectories(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* Directory_GetDirectories_m071EF47103F7A38FBF882C900F0A0AFB4326631F (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mEF13C0755B5C7882DCCF7A7CE6855A0F1957054F (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) ;
// System.String[] System.IO.Directory::GetFiles(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* Directory_GetFiles_m3E6AA407767C85CD62C5FD2750747274D1C4EA76 (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m78470E87107F961637FE6335558FB1402513DA25 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m918500C1EFB475181349A79989BB79BB36102894 (String_t* ___0_format, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___1_args, const RuntimeMethod* method) ;
// System.Void UniGLTF.UniGLTFException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniGLTFException__ctor_m1DB21B5842F46FF3E9D916D20F88C027A333B6FD (UniGLTFException_t34581B33464F9CE4820C4F4A2A441F7E9C22365F* __this, String_t* ___0_msg, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F (Exception_t* __this, String_t* ___0_message, const RuntimeMethod* method) ;
// System.Void UniGLTF.UniGLTFNotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniGLTFNotSupportedException__ctor_m2A0E272781AEADB0BA1E81CFE83D740F84329E9C (UniGLTFNotSupportedException_t67ABEDC648B5033BE849FB4637988BDC4A6FCF19* __this, String_t* ___0_msg, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___0_name, int32_t ___1_value, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Material::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Material_GetInt_mA772B615274DD11B37A352BC66EFA81BFD9C13EA (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___0_name, const RuntimeMethod* method) ;
// System.Single UnityEngine.Material::GetFloat(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Material_GetFloat_m2A77F10E6AA13EA3FA56166EFEA897115A14FA5A (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___0_name, const RuntimeMethod* method) ;
// System.Void UniGLTF.UniUnlit.Utils::SetupBlendMode(UnityEngine.Material,UniGLTF.UniUnlit.UniUnlitRenderMode,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetupBlendMode_m67BD9C8AEB6487A1BDDAEE412728A8ED80CD5C30 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, int32_t ___1_renderMode, bool ___2_isRenderModeChangedByUser, const RuntimeMethod* method) ;
// System.Void UniGLTF.UniUnlit.Utils::SetupVertexColorBlendOp(UnityEngine.Material,UniGLTF.UniUnlit.UniUnlitVertexColorBlendOp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetupVertexColorBlendOp_mF04EB4BCCB123D9AF4CC930EF9A4790994BE8614 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, int32_t ___1_vColBlendOp, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetOverrideTag(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetOverrideTag_mD68833CD28EBAF71CB6AF127B38075629B74FE08 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___0_tag, String_t* ___1_val, const RuntimeMethod* method) ;
// System.Void UniGLTF.UniUnlit.Utils::SetKeyword(UnityEngine.Material,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_mat, String_t* ___1_keyword, bool ___2_required, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::set_renderQueue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_renderQueue_mFBB72A781DCCF0D4B85670B597788EC2D02D1C14 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::EnableKeyword(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_EnableKeyword_mE8523EF6CF694284DF976D47ADEDE9363A1174AC (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___0_keyword, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::DisableKeyword(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_DisableKeyword_mC123927EBF2F2A19220A4456C8EA19F2BA416E8C (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___0_keyword, const RuntimeMethod* method) ;
// System.Void UniGLTF.Zip.ZipParseException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipParseException__ctor_m09C74A3829DD1EA07929D6B4427C76405E8995AD (ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7* __this, String_t* ___0_msg, const RuntimeMethod* method) ;
// System.Int32 UniGLTF.Zip.EOCD::FindEOCD(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EOCD_FindEOCD_mE6FA16E8F09A0ABAE86EC9D8E519235F3A9B8049 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, const RuntimeMethod* method) ;
// System.Void System.IO.MemoryStream::.ctor(System.Byte[],System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MemoryStream__ctor_mC2A08AF3FC30A1DF60B2CFC5668637DF88B66444 (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_buffer, int32_t ___1_index, int32_t ___2_count, bool ___3_writable, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_input, const RuntimeMethod* method) ;
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5 (int32_t* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.Zip.EOCD::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EOCD__ctor_mC33629D5B65648FDAC1D13D0766DB704D595D966 (EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* __this, const RuntimeMethod* method) ;
// System.Text.Encoding System.Text.Encoding::get_ASCII()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* Encoding_get_ASCII_mCC61B512D320FD4E2E71CC0DFDF8DDF3CD215C65 (const RuntimeMethod* method) ;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* Encoding_get_UTF8_m9FA98A53CE96FD6D02982625C5246DD36C1235C9 (const RuntimeMethod* method) ;
// System.Int32 System.BitConverter::ToInt32(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BitConverter_ToInt32_m259B4E62995575B80C4086347C867EB3C8D7DAB3 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_value, int32_t ___1_startIndex, const RuntimeMethod* method) ;
// System.Void UniGLTF.Zip.CommonHeader::Read(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommonHeader_Read_m47229F24CB9C58CD9B5F568F388D0D97E8E179A6 (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_r, const RuntimeMethod* method) ;
// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[],System.Int32,System.Int32)
inline void ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20 (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_array, int32_t ___1_offset, int32_t ___2_count, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t, const RuntimeMethod*))ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_gshared)(__this, ___0_array, ___1_offset, ___2_count, method);
}
// System.String UniGLTF.Zip.CommonHeader::get_FileName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CommonHeader_get_FileName_m1B58286382761DBAFD9F5911351D311D09DEF7B3 (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.Zip.CommonHeader::.ctor(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommonHeader__ctor_mC4C89B79E39052AC7FF022A55C70AF64AC1F2BAD (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, int32_t ___1_offset, const RuntimeMethod* method) ;
// System.Void System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m1A10A14901A3DCFAC3D85DE886E7D0817A95C26C (Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___0_object, ___1_method, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UniGLTF.Zip.CentralDirectoryFileHeader,System.String>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_TisString_t_mC7273138476D2A6E6C69BD0A6CA9F902E90B4619 (RuntimeObject* ___0_source, Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* ___1_selector, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8*, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m67C538A5EBF57C4844107A8EF25DB2CAAFBAF8FB_gshared)(___0_source, ___1_selector, method);
}
// TSource[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<TSource>)
inline StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* Enumerable_ToArray_TisString_t_m3B23EE2DD15B2996E7D2ECA6E74696DA892AA194 (RuntimeObject* ___0_source, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_mA54265C2C8A0864929ECD300B75E4952D553D17D_gshared)(___0_source, method);
}
// System.String System.String::Join(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Join_m557B6B554B87C1742FA0B128500073B421ED0BFD (String_t* ___0_separator, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___1_value, const RuntimeMethod* method) ;
// UniGLTF.Zip.EOCD UniGLTF.Zip.EOCD::Parse(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* EOCD_Parse_mF448F8F575BD23EE19FAC2660F16B4528C7D6B05 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, const RuntimeMethod* method) ;
// System.Void UniGLTF.Zip.ZipArchiveStorage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipArchiveStorage__ctor_mF847A1CDD38254B27EDC947A3AE5FDFC398D43B5 (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.Zip.CentralDirectoryFileHeader::.ctor(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CentralDirectoryFileHeader__ctor_mF7669E89649B2656FC2648DB2096BE0438EDC245 (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, int32_t ___1_offset, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UniGLTF.Zip.CentralDirectoryFileHeader>::Add(T)
inline void List_1_Add_mC00BE642E7A70274A2C74253C0E44C69AE0A9F29_inline (List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* __this, CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1*, CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___0_item, method);
}
// System.Void UniGLTF.Zip.LocalFileHeader::.ctor(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalFileHeader__ctor_m638E30171367A591F726F0B3FCB7A9C3F597E6EE (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, int32_t ___1_offset, const RuntimeMethod* method) ;
// System.Void System.IO.Compression.DeflateStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflateStream__ctor_m344C1EF8B83E612C4FC662F0152DF1D2A5636829 (DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, int32_t ___1_mode, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Text.Encoding)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_m7712DDC735E99B6833E2666ADFD8A06CB96A58B1 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, const RuntimeMethod* method) ;
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass5_0__ctor_m7ED750F1E4A531C81F17A2DC644B8DE77BDCE1B0 (U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* __this, const RuntimeMethod* method) ;
// System.Void System.Func`2<UniGLTF.Zip.CentralDirectoryFileHeader,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mDA06EA659644633B6732E314C1A2934B265C1B88 (Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m13C0A7F33154D861E2A041B52E88461832DA1697_gshared)(__this, ___0_object, ___1_method, method);
}
// TSource System.Linq.Enumerable::FirstOrDefault<UniGLTF.Zip.CentralDirectoryFileHeader>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
inline CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* Enumerable_FirstOrDefault_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_m13A276AEA97B6522EBC40FFC60147FB43E1CD02E (RuntimeObject* ___0_source, Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A* ___1_predicate, const RuntimeMethod* method)
{
	return ((  CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* (*) (RuntimeObject*, Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A*, const RuntimeMethod*))Enumerable_FirstOrDefault_TisRuntimeObject_mFACC750E4D7AF7B43F5B866C84F613B3ECC41994_gshared)(___0_source, ___1_predicate, method);
}
// System.Void System.IO.FileNotFoundException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileNotFoundException__ctor_mA8C9C93DB8C5B96D6B5E59B2AE07154F265FB1A1 (FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A* __this, String_t* ___0_message, const RuntimeMethod* method) ;
// System.Byte[] UniGLTF.Zip.ZipArchiveStorage::Extract(UniGLTF.Zip.CentralDirectoryFileHeader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ZipArchiveStorage_Extract_m3AAD8FDCA8FAD438FCB82B48290D9CE8031577E6 (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* ___0_header, const RuntimeMethod* method) ;
// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[])
inline void ArraySegment_1__ctor_m8A879E5F534A391C62D3D65CDF9B8F0A7E1AED86 (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_array, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, const RuntimeMethod*))ArraySegment_1__ctor_m8A879E5F534A391C62D3D65CDF9B8F0A7E1AED86_gshared)(__this, ___0_array, method);
}
// System.String System.Enum::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Enum_ToString_m946B0B83C4470457D0FF555D862022C72BB55741 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void System.NotImplementedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742 (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* __this, String_t* ___0_message, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UniGLTF.Zip.CentralDirectoryFileHeader>::.ctor()
inline void List_1__ctor_m6BCCFB8857EC3B1BF31B34F9E7CD0B46C8AC0A3C (List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6D6C41B3498BF95557C5BDFD94BA73461CA74133 (U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84* __this, const RuntimeMethod* method) ;
// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m79ED1BF1EE36D1E417BA89A0D9F91F8AAD8D19E2 (Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.ShaderPropExporter.SupportedShader::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46 (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD* __this, String_t* ___0_targetFolder, String_t* ___1_shaderName, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>::.ctor()
inline void Dictionary_2__ctor_mF1E5D7383BFC26A82A10D42650A4EE167D9B091B (Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57 (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___0_handle, const RuntimeMethod* method) ;
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
inline bool Enumerable_Any_TisRuntimeObject_m67CFBD544CF1D1C0C7E7457FDBDB81649DE26847 (RuntimeObject* ___0_source, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m67CFBD544CF1D1C0C7E7457FDBDB81649DE26847_gshared)(___0_source, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>::get_Key()
inline String_t* KeyValuePair_2_get_Key_m3E0AEB8F3C3953ABFEC50FE77BF3B32552C80D22_inline (KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802* __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802*, const RuntimeMethod*))KeyValuePair_2_get_Key_mBD8EA7557C27E6956F2AF29DA3F7499B2F51A282_gshared_inline)(__this, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>::get_Value()
inline ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* KeyValuePair_2_get_Value_m0E88FF128ADD89043FB8F078C432663C7BCE4648_inline (KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802* __this, const RuntimeMethod* method)
{
	return ((  ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* (*) (KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802*, const RuntimeMethod*))KeyValuePair_2_get_Value_mC6BD8075F9C9DDEF7B4D731E5C38EC19103988E7_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>::Add(TKey,TValue)
inline void Dictionary_2_Add_m48A495425B604D5E9111229379AA45DF27806631 (Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* __this, String_t* ___0_key, ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D*, String_t*, ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*, const RuntimeMethod*))Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>::TryGetValue(TKey,TValue&)
inline bool Dictionary_2_TryGetValue_m4C31AFC006F2A82A3464C2C5F620AEE8809B47A8 (Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* __this, String_t* ___0_key, ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD** ___1_value, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D*, String_t*, ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD**, const RuntimeMethod*))Dictionary_2_TryGetValue_mD15380A4ED7CDEE99EA45881577D26BA9CE1B849_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Void UniGLTF.ShaderPropExporter.ShaderProps::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9 (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* __this, const RuntimeMethod* method) ;
// System.Void UniGLTF.ShaderPropExporter.ShaderProperty::.ctor(System.String,UniGLTF.ShaderPropExporter.ShaderPropertyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4 (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F* __this, String_t* ___0_key, int32_t ___1_propType, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps>::.ctor(TKey,TValue)
inline void KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209 (KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802* __this, String_t* ___0_key, ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802*, String_t*, ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*, const RuntimeMethod*))KeyValuePair_2__ctor_m0D5C3E0BE2D37252D3F4E0F0EA9A13C9458D4950_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3 (String_t* __this, int32_t ___0_index, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.UnityPath
IL2CPP_EXTERN_C void UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshal_pinvoke(const UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044& unmarshaled, UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_pinvoke& marshaled)
{
	marshaled.___U3CValueU3Ek__BackingField_0 = il2cpp_codegen_marshal_string(unmarshaled.___U3CValueU3Ek__BackingField_0);
}
IL2CPP_EXTERN_C void UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshal_pinvoke_back(const UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_pinvoke& marshaled, UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044& unmarshaled)
{
	unmarshaled.___U3CValueU3Ek__BackingField_0 = il2cpp_codegen_marshal_string_result(marshaled.___U3CValueU3Ek__BackingField_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___U3CValueU3Ek__BackingField_0), (void*)il2cpp_codegen_marshal_string_result(marshaled.___U3CValueU3Ek__BackingField_0));
}
// Conversion method for clean up from marshalling of: UniGLTF.UnityPath
IL2CPP_EXTERN_C void UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshal_pinvoke_cleanup(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___U3CValueU3Ek__BackingField_0);
	marshaled.___U3CValueU3Ek__BackingField_0 = NULL;
}
// Conversion methods for marshalling of: UniGLTF.UnityPath
IL2CPP_EXTERN_C void UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshal_com(const UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044& unmarshaled, UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_com& marshaled)
{
	marshaled.___U3CValueU3Ek__BackingField_0 = il2cpp_codegen_marshal_bstring(unmarshaled.___U3CValueU3Ek__BackingField_0);
}
IL2CPP_EXTERN_C void UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshal_com_back(const UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_com& marshaled, UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044& unmarshaled)
{
	unmarshaled.___U3CValueU3Ek__BackingField_0 = il2cpp_codegen_marshal_bstring_result(marshaled.___U3CValueU3Ek__BackingField_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___U3CValueU3Ek__BackingField_0), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___U3CValueU3Ek__BackingField_0));
}
// Conversion method for clean up from marshalling of: UniGLTF.UnityPath
IL2CPP_EXTERN_C void UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshal_com_cleanup(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___U3CValueU3Ek__BackingField_0);
	marshaled.___U3CValueU3Ek__BackingField_0 = NULL;
}
// System.String UniGLTF.UnityPath::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	{
		// get;
		String_t* L_0 = __this->___U3CValueU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_EXTERN_C  String_t* UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UniGLTF.UnityPath::set_Value(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		// private set;
		String_t* L_0 = ___0_value;
		__this->___U3CValueU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CValueU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6_AdjustorThunk (RuntimeObject* __this, String_t* ___0_value, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6_inline(_thisAdjusted, ___0_value, method);
}
// System.String UniGLTF.UnityPath::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_ToString_m56BAEA8A30E60050E4ECFA71DA865121BE2F4296 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral23E4211C15099DBD2E1D188F8EDABE56912B6625);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return string.Format("unity://{0}", Value);
		String_t* L_0;
		L_0 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		String_t* L_1;
		L_1 = String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8(_stringLiteral23E4211C15099DBD2E1D188F8EDABE56912B6625, L_0, NULL);
		V_0 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		// }
		String_t* L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  String_t* UnityPath_ToString_m56BAEA8A30E60050E4ECFA71DA865121BE2F4296_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = UnityPath_ToString_m56BAEA8A30E60050E4ECFA71DA865121BE2F4296(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UniGLTF.UnityPath::get_IsNull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		// get { return Value == null; }
		String_t* L_0;
		L_0 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		V_0 = (bool)((((RuntimeObject*)(String_t*)L_0) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		goto IL_000d;
	}

IL_000d:
	{
		// get { return Value == null; }
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	bool _returnValue;
	_returnValue = UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UniGLTF.UnityPath::get_IsUnderAssetsFolder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsUnderAssetsFolder_mF052E6B9E01854DF1E3B816EBAC238C09CF520CE (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51311F107A22ACB2B9982782F66881085ABC044E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAA5A7F71CEAF22FCCBE9ECEEBBD1FF99E220991);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B5_0 = 0;
	{
		// if (IsNull)
		bool L_0;
		L_0 = UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738(__this, NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_0038;
	}

IL_0010:
	{
		// return Value == "Assets" || Value.StartsWith("Assets/");
		String_t* L_2;
		L_2 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		bool L_3;
		L_3 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_2, _stringLiteral51311F107A22ACB2B9982782F66881085ABC044E, NULL);
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_4;
		L_4 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		NullCheck(L_4);
		bool L_5;
		L_5 = String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0(L_4, _stringLiteralEAA5A7F71CEAF22FCCBE9ECEEBBD1FF99E220991, NULL);
		G_B5_0 = ((int32_t)(L_5));
		goto IL_0035;
	}

IL_0034:
	{
		G_B5_0 = 1;
	}

IL_0035:
	{
		V_1 = (bool)G_B5_0;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
IL2CPP_EXTERN_C  bool UnityPath_get_IsUnderAssetsFolder_mF052E6B9E01854DF1E3B816EBAC238C09CF520CE_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	bool _returnValue;
	_returnValue = UnityPath_get_IsUnderAssetsFolder_mF052E6B9E01854DF1E3B816EBAC238C09CF520CE(_thisAdjusted, method);
	return _returnValue;
}
// System.String UniGLTF.UnityPath::get_FileNameWithoutExtension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_FileNameWithoutExtension_m033B52ECE15B98E072F5D7C6F32C2161513DE423 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// get { return Path.GetFileNameWithoutExtension(Value); }
		String_t* L_0;
		L_0 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Path_GetFileNameWithoutExtension_m2D14CCBAB9C60DC8D32C2443CCE3D34644822FAF(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// get { return Path.GetFileNameWithoutExtension(Value); }
		String_t* L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  String_t* UnityPath_get_FileNameWithoutExtension_m033B52ECE15B98E072F5D7C6F32C2161513DE423_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = UnityPath_get_FileNameWithoutExtension_m033B52ECE15B98E072F5D7C6F32C2161513DE423(_thisAdjusted, method);
	return _returnValue;
}
// System.String UniGLTF.UnityPath::get_Extension()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_Extension_m2D86C2FD1704264AE5AF42829096A414E1A484E1 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// get { return Path.GetExtension(Value); }
		String_t* L_0;
		L_0 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Path_GetExtension_m6FEAA9E14451BFD210B9D1AEC2430C813F570FE5(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// get { return Path.GetExtension(Value); }
		String_t* L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  String_t* UnityPath_get_Extension_m2D86C2FD1704264AE5AF42829096A414E1A484E1_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = UnityPath_get_Extension_m2D86C2FD1704264AE5AF42829096A414E1A484E1(_thisAdjusted, method);
	return _returnValue;
}
// UniGLTF.UnityPath UniGLTF.UnityPath::get_Parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_get_Parent_m730217B9BDBCD8088F4B7FE88903FF272146E4D9 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_1;
	memset((&V_1), 0, sizeof(V_1));
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (IsNull)
		bool L_0;
		L_0 = UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738(__this, NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return default(UnityPath);
		il2cpp_codegen_initobj((&V_1), sizeof(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044));
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_2 = V_1;
		V_2 = L_2;
		goto IL_002b;
	}

IL_0018:
	{
		// return new UnityPath(Path.GetDirectoryName(Value));
		String_t* L_3;
		L_3 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_4;
		L_4 = Path_GetDirectoryName_m428BADBE493A3927B51A13DEF658929B430516F6(L_3, NULL);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_5;
		memset((&L_5), 0, sizeof(L_5));
		UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22((&L_5), L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_002b;
	}

IL_002b:
	{
		// }
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_6 = V_2;
		return L_6;
	}
}
IL2CPP_EXTERN_C  UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_get_Parent_m730217B9BDBCD8088F4B7FE88903FF272146E4D9_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 _returnValue;
	_returnValue = UnityPath_get_Parent_m730217B9BDBCD8088F4B7FE88903FF272146E4D9(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UniGLTF.UnityPath::get_HasParent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_HasParent_m4FF133BBE9F4F213758EBA3C8C1C8070AFEBF209 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		// return !string.IsNullOrEmpty(Value);
		String_t* L_0;
		L_0 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0012:
	{
		// }
		bool L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  bool UnityPath_get_HasParent_m4FF133BBE9F4F213758EBA3C8C1C8070AFEBF209_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	bool _returnValue;
	_returnValue = UnityPath_get_HasParent_m4FF133BBE9F4F213758EBA3C8C1C8070AFEBF209(_thisAdjusted, method);
	return _returnValue;
}
// System.String UniGLTF.UnityPath::EscapeFilePath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_EscapeFilePath_m91BE64399890FF8B13028A83856588C73278B986 (String_t* ___0_path, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	String_t* V_3 = NULL;
	{
		// foreach (var x in EscapeChars)
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = ((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields*)il2cpp_codegen_static_fields_for(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))->___EscapeChars_1;
		V_0 = L_0;
		V_1 = 0;
		goto IL_0021;
	}

IL_000c:
	{
		// foreach (var x in EscapeChars)
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		uint16_t L_4 = (uint16_t)(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// path = path.Replace(x, '+');
		String_t* L_5 = ___0_path;
		Il2CppChar L_6 = V_2;
		NullCheck(L_5);
		String_t* L_7;
		L_7 = String_Replace_m86403DC5F422D8D5E1CFAAF255B103CB807EDAAF(L_5, L_6, ((int32_t)43), NULL);
		___0_path = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_8, 1));
	}

IL_0021:
	{
		// foreach (var x in EscapeChars)
		int32_t L_9 = V_1;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))))
		{
			goto IL_000c;
		}
	}
	{
		// return path;
		String_t* L_11 = ___0_path;
		V_3 = L_11;
		goto IL_002b;
	}

IL_002b:
	{
		// }
		String_t* L_12 = V_3;
		return L_12;
	}
}
// UniGLTF.UnityPath UniGLTF.UnityPath::Child(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_Child_m1EF92234D9BD610C0F3ACB6F09FDA397FDB7AEFB (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_name, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (IsNull)
		bool L_0;
		L_0 = UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738(__this, NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		// throw new NotImplementedException();
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_2 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_2);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_2, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&UnityPath_Child_m1EF92234D9BD610C0F3ACB6F09FDA397FDB7AEFB_RuntimeMethod_var)));
	}

IL_0012:
	{
		// else if (Value == "")
		String_t* L_3;
		L_3 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		bool L_4;
		L_4 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_3, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		// return new UnityPath(name);
		String_t* L_6 = ___0_name;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_7;
		memset((&L_7), 0, sizeof(L_7));
		UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22((&L_7), L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_004a;
	}

IL_0030:
	{
		// return new UnityPath(Value + "/" + name);
		String_t* L_8;
		L_8 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		String_t* L_9 = ___0_name;
		String_t* L_10;
		L_10 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(L_8, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, L_9, NULL);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_11;
		memset((&L_11), 0, sizeof(L_11));
		UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22((&L_11), L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		goto IL_004a;
	}

IL_004a:
	{
		// }
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_12 = V_2;
		return L_12;
	}
}
IL2CPP_EXTERN_C  UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_Child_m1EF92234D9BD610C0F3ACB6F09FDA397FDB7AEFB_AdjustorThunk (RuntimeObject* __this, String_t* ___0_name, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 _returnValue;
	_returnValue = UnityPath_Child_m1EF92234D9BD610C0F3ACB6F09FDA397FDB7AEFB(_thisAdjusted, ___0_name, method);
	return _returnValue;
}
// System.Int32 UniGLTF.UnityPath::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityPath_GetHashCode_m10C09137D286D528E82BF59E4231E0C9ADEB45C0 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		// if (IsNull)
		bool L_0;
		L_0 = UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738(__this, NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// return 0;
		V_1 = 0;
		goto IL_001e;
	}

IL_0010:
	{
		// return Value.GetHashCode();
		String_t* L_2;
		L_2 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		NullCheck(L_2);
		int32_t L_3;
		L_3 = VirtualFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		V_1 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		// }
		int32_t L_4 = V_1;
		return L_4;
	}
}
IL2CPP_EXTERN_C  int32_t UnityPath_GetHashCode_m10C09137D286D528E82BF59E4231E0C9ADEB45C0_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	int32_t _returnValue;
	_returnValue = UnityPath_GetHashCode_m10C09137D286D528E82BF59E4231E0C9ADEB45C0(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UniGLTF.UnityPath::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_Equals_m167179E2120861BB5DC8EB6AFB507A16682F2734 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_1;
	memset((&V_1), 0, sizeof(V_1));
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	int32_t G_B4_0 = 0;
	{
		// if(obj is UnityPath)
		RuntimeObject* L_0 = ___0_obj;
		V_0 = (bool)((!(((RuntimeObject*)(RuntimeObject*)((RuntimeObject*)IsInstSealed((RuntimeObject*)L_0, UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0073;
		}
	}
	{
		// var rhs = (UnityPath)obj;
		RuntimeObject* L_2 = ___0_obj;
		V_1 = ((*(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*)((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*)(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*)UnBox(L_2, UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))));
		// if(Value==null && rhs.Value == null)
		String_t* L_3;
		L_3 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_4;
		L_4 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline((&V_1), NULL);
		G_B4_0 = ((((RuntimeObject*)(String_t*)L_4) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 0;
	}

IL_002b:
	{
		V_2 = (bool)G_B4_0;
		bool L_5 = V_2;
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		// return true;
		V_3 = (bool)1;
		goto IL_0078;
	}

IL_0034:
	{
		// else if (Value == null)
		String_t* L_6;
		L_6 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		V_4 = (bool)((((RuntimeObject*)(String_t*)L_6) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_7 = V_4;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		// return false;
		V_3 = (bool)0;
		goto IL_0078;
	}

IL_0048:
	{
		// else if (rhs.Value == null)
		String_t* L_8;
		L_8 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline((&V_1), NULL);
		V_5 = (bool)((((RuntimeObject*)(String_t*)L_8) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_9 = V_5;
		if (!L_9)
		{
			goto IL_005d;
		}
	}
	{
		// return false;
		V_3 = (bool)0;
		goto IL_0078;
	}

IL_005d:
	{
		// return Value == rhs.Value;
		String_t* L_10;
		L_10 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		String_t* L_11;
		L_11 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline((&V_1), NULL);
		bool L_12;
		L_12 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_10, L_11, NULL);
		V_3 = L_12;
		goto IL_0078;
	}

IL_0073:
	{
		// return false;
		V_3 = (bool)0;
		goto IL_0078;
	}

IL_0078:
	{
		// }
		bool L_13 = V_3;
		return L_13;
	}
}
IL2CPP_EXTERN_C  bool UnityPath_Equals_m167179E2120861BB5DC8EB6AFB507A16682F2734_AdjustorThunk (RuntimeObject* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	bool _returnValue;
	_returnValue = UnityPath_Equals_m167179E2120861BB5DC8EB6AFB507A16682F2734(_thisAdjusted, ___0_obj, method);
	return _returnValue;
}
// UniGLTF.UnityPath UniGLTF.UnityPath::GetAssetFolder(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_GetAssetFolder_mCBCA3B9D808ADF3DBEF3DAB1E561253E8F387D9A (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_suffix, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral813424E8552DDA36C36D9633CD0EF610A1CC62E6);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_1;
	memset((&V_1), 0, sizeof(V_1));
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (!IsUnderAssetsFolder)
		bool L_0;
		L_0 = UnityPath_get_IsUnderAssetsFolder_mF052E6B9E01854DF1E3B816EBAC238C09CF520CE(__this, NULL);
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// throw new NotImplementedException();
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_2 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_2);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_2, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&UnityPath_GetAssetFolder_mCBCA3B9D808ADF3DBEF3DAB1E561253E8F387D9A_RuntimeMethod_var)));
	}

IL_0015:
	{
		// return new UnityPath(
		//     string.Format("{0}/{1}{2}",
		//     Parent.Value,
		//     FileNameWithoutExtension,
		//     suffix
		//     ));
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_3;
		L_3 = UnityPath_get_Parent_m730217B9BDBCD8088F4B7FE88903FF272146E4D9(__this, NULL);
		V_1 = L_3;
		String_t* L_4;
		L_4 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline((&V_1), NULL);
		String_t* L_5;
		L_5 = UnityPath_get_FileNameWithoutExtension_m033B52ECE15B98E072F5D7C6F32C2161513DE423(__this, NULL);
		String_t* L_6 = ___0_suffix;
		String_t* L_7;
		L_7 = String_Format_mA0534D6E2AE4D67A6BD8D45B3321323930EB930C(_stringLiteral813424E8552DDA36C36D9633CD0EF610A1CC62E6, L_4, L_5, L_6, NULL);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_8;
		memset((&L_8), 0, sizeof(L_8));
		UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22((&L_8), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_003c;
	}

IL_003c:
	{
		// }
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_9 = V_2;
		return L_9;
	}
}
IL2CPP_EXTERN_C  UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_GetAssetFolder_mCBCA3B9D808ADF3DBEF3DAB1E561253E8F387D9A_AdjustorThunk (RuntimeObject* __this, String_t* ___0_suffix, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 _returnValue;
	_returnValue = UnityPath_GetAssetFolder_mCBCA3B9D808ADF3DBEF3DAB1E561253E8F387D9A(_thisAdjusted, ___0_suffix, method);
	return _returnValue;
}
// System.Void UniGLTF.UnityPath::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Value = value.Replace("\\", "/");
		String_t* L_0 = ___0_value;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166(L_0, _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, NULL);
		UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6_inline(__this, L_1, NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22_AdjustorThunk (RuntimeObject* __this, String_t* ___0_value, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22(_thisAdjusted, ___0_value, method);
}
// UniGLTF.UnityPath UniGLTF.UnityPath::FromUnityPath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_FromUnityPath_m1ACC30671C095E8AAC73A3A60DA0ADE7B4A7EF37 (String_t* ___0_unityPath, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_1;
	memset((&V_1), 0, sizeof(V_1));
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (String.IsNullOrEmpty(unityPath))
		String_t* L_0 = ___0_unityPath;
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		// return new UnityPath
		// {
		//     Value=""
		// };
		il2cpp_codegen_initobj((&V_1), sizeof(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044));
		UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6_inline((&V_1), _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_3 = V_1;
		V_2 = L_3;
		goto IL_0033;
	}

IL_0025:
	{
		// return FromFullpath(Path.GetFullPath(unityPath));
		String_t* L_4 = ___0_unityPath;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_5;
		L_5 = Path_GetFullPath_m9E485D7D38A868A6A5863CBD24677231288EECE2(L_4, NULL);
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_6;
		L_6 = UnityPath_FromFullpath_m9B741FAFE56705ADA157667B1CEB2880293EC6D9(L_5, NULL);
		V_2 = L_6;
		goto IL_0033;
	}

IL_0033:
	{
		// }
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_7 = V_2;
		return L_7;
	}
}
// System.String UniGLTF.UnityPath::get_BaseFullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_BaseFullPath_m375AE31235A8D7E6ED0690803B72F8A93389E0B0 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral72FD210EDBBBC26FA48DE0D21939BDF6630D46E0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	{
		// if (string.IsNullOrEmpty(s_basePath))
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_0 = ((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields*)il2cpp_codegen_static_fields_for(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))->___s_basePath_2;
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		// s_basePath = Path.GetFullPath(Application.dataPath + "/..").Replace("\\", "/");
		String_t* L_3;
		L_3 = Application_get_dataPath_m4C8412CBEE4EAAAB6711CC9BEFFA73CEE5BDBEF7(NULL);
		String_t* L_4;
		L_4 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_3, _stringLiteral72FD210EDBBBC26FA48DE0D21939BDF6630D46E0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_5;
		L_5 = Path_GetFullPath_m9E485D7D38A868A6A5863CBD24677231288EECE2(L_4, NULL);
		NullCheck(L_5);
		String_t* L_6;
		L_6 = String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166(L_5, _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, NULL);
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields*)il2cpp_codegen_static_fields_for(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))->___s_basePath_2 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields*)il2cpp_codegen_static_fields_for(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))->___s_basePath_2), (void*)L_6);
	}

IL_0039:
	{
		// return s_basePath;
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_7 = ((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields*)il2cpp_codegen_static_fields_for(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))->___s_basePath_2;
		V_1 = L_7;
		goto IL_0041;
	}

IL_0041:
	{
		// }
		String_t* L_8 = V_1;
		return L_8;
	}
}
// System.String UniGLTF.UnityPath::get_AssetFullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_AssetFullPath_m68A5F1C1B3EAA3BCF1FFA4C89318DDD050612441 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D11759E924BA1F921B14B215821EB4CBEB88859);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return BaseFullPath + "/Assets";
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_0;
		L_0 = UnityPath_get_BaseFullPath_m375AE31235A8D7E6ED0690803B72F8A93389E0B0(NULL);
		String_t* L_1;
		L_1 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_0, _stringLiteral9D11759E924BA1F921B14B215821EB4CBEB88859, NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.String UniGLTF.UnityPath::get_FullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	{
		// if (IsNull)
		bool L_0;
		L_0 = UnityPath_get_IsNull_mA9E445A84B5A402861EBB1159AC1753869068738(__this, NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		// throw new NotImplementedException();
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_2 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_2);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_2, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322_RuntimeMethod_var)));
	}

IL_0012:
	{
		// return Path.Combine(BaseFullPath, Value).Replace("\\", "/");
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_3;
		L_3 = UnityPath_get_BaseFullPath_m375AE31235A8D7E6ED0690803B72F8A93389E0B0(NULL);
		String_t* L_4;
		L_4 = UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_5;
		L_5 = Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE(L_3, L_4, NULL);
		NullCheck(L_5);
		String_t* L_6;
		L_6 = String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166(L_5, _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, NULL);
		V_1 = L_6;
		goto IL_0034;
	}

IL_0034:
	{
		// }
		String_t* L_7 = V_1;
		return L_7;
	}
}
IL2CPP_EXTERN_C  String_t* UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UniGLTF.UnityPath::get_IsFileExists()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsFileExists_mA8B0112A365374EF47945D962BBDC57278320329 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		// get { return File.Exists(FullPath); }
		String_t* L_0;
		L_0 = UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322(__this, NULL);
		bool L_1;
		L_1 = File_Exists_m95E329ABBE3EAD6750FE1989BBA6884457136D4A(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// get { return File.Exists(FullPath); }
		bool L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  bool UnityPath_get_IsFileExists_mA8B0112A365374EF47945D962BBDC57278320329_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	bool _returnValue;
	_returnValue = UnityPath_get_IsFileExists_mA8B0112A365374EF47945D962BBDC57278320329(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UniGLTF.UnityPath::get_IsDirectoryExists()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_get_IsDirectoryExists_mA16B832020034FE21285DA9F89B197D33FA6C7B1 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		// get { return Directory.Exists(FullPath); }
		String_t* L_0;
		L_0 = UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322(__this, NULL);
		bool L_1;
		L_1 = Directory_Exists_m3D125E9E88C291CF11113444F961A64DD83AE1C7(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// get { return Directory.Exists(FullPath); }
		bool L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  bool UnityPath_get_IsDirectoryExists_mA16B832020034FE21285DA9F89B197D33FA6C7B1_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	bool _returnValue;
	_returnValue = UnityPath_get_IsDirectoryExists_mA16B832020034FE21285DA9F89B197D33FA6C7B1(_thisAdjusted, method);
	return _returnValue;
}
// UniGLTF.UnityPath UniGLTF.UnityPath::FromFullpath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 UnityPath_FromFullpath_m9B741FAFE56705ADA157667B1CEB2880293EC6D9 (String_t* ___0_fullPath, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_2;
	memset((&V_2), 0, sizeof(V_2));
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 V_3;
	memset((&V_3), 0, sizeof(V_3));
	bool V_4 = false;
	{
		// if(fullPath == null)
		String_t* L_0 = ___0_fullPath;
		V_0 = (bool)((((RuntimeObject*)(String_t*)L_0) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		// fullPath = "";
		___0_fullPath = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0012:
	{
		// fullPath = fullPath.Replace("\\", "/");
		String_t* L_2 = ___0_fullPath;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166(L_2, _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, NULL);
		___0_fullPath = L_3;
		// if (fullPath == BaseFullPath) {
		String_t* L_4 = ___0_fullPath;
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_5;
		L_5 = UnityPath_get_BaseFullPath_m375AE31235A8D7E6ED0690803B72F8A93389E0B0(NULL);
		bool L_6;
		L_6 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_4, L_5, NULL);
		V_1 = L_6;
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		// return new UnityPath
		// {
		//     Value=""
		// };
		il2cpp_codegen_initobj((&V_2), sizeof(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044));
		UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6_inline((&V_2), _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_8 = V_2;
		V_3 = L_8;
		goto IL_0090;
	}

IL_004d:
	{
		// else if(fullPath.StartsWith(BaseFullPath + "/"))
		String_t* L_9 = ___0_fullPath;
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_10;
		L_10 = UnityPath_get_BaseFullPath_m375AE31235A8D7E6ED0690803B72F8A93389E0B0(NULL);
		String_t* L_11;
		L_11 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_10, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, NULL);
		NullCheck(L_9);
		bool L_12;
		L_12 = String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0(L_9, L_11, NULL);
		V_4 = L_12;
		bool L_13 = V_4;
		if (!L_13)
		{
			goto IL_0083;
		}
	}
	{
		// return new UnityPath(fullPath.Substring(BaseFullPath.Length + 1));
		String_t* L_14 = ___0_fullPath;
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_15;
		L_15 = UnityPath_get_BaseFullPath_m375AE31235A8D7E6ED0690803B72F8A93389E0B0(NULL);
		NullCheck(L_15);
		int32_t L_16;
		L_16 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_15, NULL);
		NullCheck(L_14);
		String_t* L_17;
		L_17 = String_Substring_m6BA4A3FA3800FE92662D0847CC8E1EEF940DF472(L_14, ((int32_t)il2cpp_codegen_add(L_16, 1)), NULL);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_18;
		memset((&L_18), 0, sizeof(L_18));
		UnityPath__ctor_m1EE54591A7FC332E1B04E50C073444F9092E7D22((&L_18), L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		goto IL_0090;
	}

IL_0083:
	{
		// return default(UnityPath);
		il2cpp_codegen_initobj((&V_2), sizeof(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044));
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_19 = V_2;
		V_3 = L_19;
		goto IL_0090;
	}

IL_0090:
	{
		// }
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_20 = V_3;
		return L_20;
	}
}
// System.Boolean UniGLTF.UnityPath::IsUnderAssetFolder(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityPath_IsUnderAssetFolder_m46B9357817C4BA97BE16B56826193B889D33A473 (String_t* ___0_fullPath, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// return fullPath.Replace("\\", "/").StartsWith(AssetFullPath);
		String_t* L_0 = ___0_fullPath;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166(L_0, _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, NULL);
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = UnityPath_get_AssetFullPath_m68A5F1C1B3EAA3BCF1FFA4C89318DDD050612441(NULL);
		NullCheck(L_1);
		bool L_3;
		L_3 = String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0(L_1, L_2, NULL);
		V_0 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		// }
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::TravserseDir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_TravserseDir_m7F543EC20E1D086B71902D307A686AD9AC2E02DF (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* L_0 = (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31*)il2cpp_codegen_object_new(U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CTravserseDirU3Ed__38__ctor_mD53E5B3DB6363090C92EB54659036813FA81557F(L_0, ((int32_t)-2), NULL);
		U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* L_1 = L_0;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_2 = (*(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*)__this);
		NullCheck(L_1);
		L_1->___U3CU3E3__U3CU3E4__this_4 = L_2;
		Il2CppCodeGenWriteBarrier((void**)&(((&L_1->___U3CU3E3__U3CU3E4__this_4))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* UnityPath_TravserseDir_m7F543EC20E1D086B71902D307A686AD9AC2E02DF_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	RuntimeObject* _returnValue;
	_returnValue = UnityPath_TravserseDir_m7F543EC20E1D086B71902D307A686AD9AC2E02DF(_thisAdjusted, method);
	return _returnValue;
}
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::get_ChildDirs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_get_ChildDirs_mC69F9F662DBBD446BFCC738929BEB776F7FF839B (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* L_0 = (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74*)il2cpp_codegen_object_new(U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3Cget_ChildDirsU3Ed__40__ctor_m075883A29603CBDABA82EA828B9537AB4F2C52BE(L_0, ((int32_t)-2), NULL);
		V_0 = L_0;
		U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* L_1 = V_0;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_2 = (*(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*)__this);
		NullCheck(L_1);
		L_1->___U3CU3E3__U3CU3E4__this_4 = L_2;
		Il2CppCodeGenWriteBarrier((void**)&(((&L_1->___U3CU3E3__U3CU3E4__this_4))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* L_3 = V_0;
		V_1 = L_3;
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* UnityPath_get_ChildDirs_mC69F9F662DBBD446BFCC738929BEB776F7FF839B_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	RuntimeObject* _returnValue;
	_returnValue = UnityPath_get_ChildDirs_mC69F9F662DBBD446BFCC738929BEB776F7FF839B(_thisAdjusted, method);
	return _returnValue;
}
// System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath> UniGLTF.UnityPath::get_ChildFiles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityPath_get_ChildFiles_m8161F17486FA9E8DD3DC054A621494F23D49BDF7 (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* L_0 = (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35*)il2cpp_codegen_object_new(U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3Cget_ChildFilesU3Ed__42__ctor_m47F9CE437A24FE8B4385933C997775D49A7CB8E9(L_0, ((int32_t)-2), NULL);
		V_0 = L_0;
		U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* L_1 = V_0;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_2 = (*(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*)__this);
		NullCheck(L_1);
		L_1->___U3CU3E3__U3CU3E4__this_4 = L_2;
		Il2CppCodeGenWriteBarrier((void**)&(((&L_1->___U3CU3E3__U3CU3E4__this_4))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* L_3 = V_0;
		V_1 = L_3;
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* UnityPath_get_ChildFiles_m8161F17486FA9E8DD3DC054A621494F23D49BDF7_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044*>(__this + _offset);
	RuntimeObject* _returnValue;
	_returnValue = UnityPath_get_ChildFiles_m8161F17486FA9E8DD3DC054A621494F23D49BDF7(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UniGLTF.UnityPath::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityPath__cctor_m73A459F9A4881EB0CA7EE8E7D3F13849925D357B (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____57B0A7E913A6CBB7C5554095EB31FFE93AB6628DCBEC34C82369E40829116FDC_0_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static readonly char[] EscapeChars = new char[]
		// {
		//     '\\',
		//     '/',
		//     ':',
		//     '*',
		//     '?',
		//     '"',
		//     '<',
		//     '>',
		//     '|',
		// };
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_1 = L_0;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____57B0A7E913A6CBB7C5554095EB31FFE93AB6628DCBEC34C82369E40829116FDC_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_1, L_2, NULL);
		((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields*)il2cpp_codegen_static_fields_for(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))->___EscapeChars_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_StaticFields*)il2cpp_codegen_static_fields_for(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var))->___EscapeChars_1), (void*)L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38__ctor_mD53E5B3DB6363090C92EB54659036813FA81557F (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___0_U3CU3E1__state;
		__this->___U3CU3E1__state_0 = L_0;
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m66483AADCCC13272EBDCD94D31D2E52603C24BDF(NULL);
		__this->___U3CU3El__initialThreadId_2 = L_1;
		return;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_System_IDisposable_Dispose_mDA429B4DB1D8469BB75DC0AE06F726BEED7ADFB4 (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract(L_1, ((int32_t)-4)))) > ((uint32_t)1))))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0010;
	}

IL_0010:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_003b;
	}

IL_0016:
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0032:
			{// begin finally (depth: 1)
				U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_m836DFCF69E62A1CD912EE3D5A6A97E7E6669BFC7(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				int32_t L_3 = V_0;
				if ((((int32_t)L_3) == ((int32_t)((int32_t)-4))))
				{
					goto IL_0024_1;
				}
			}
			{
				goto IL_001e_1;
			}

IL_001e_1:
			{
				int32_t L_4 = V_0;
				if ((((int32_t)L_4) == ((int32_t)2)))
				{
					goto IL_0024_1;
				}
			}
			{
				goto IL_0030_1;
			}

IL_0024_1:
			{
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_0027_1:
					{// begin finally (depth: 2)
						U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m07C881555177FBA1B9E288AAF6DD15240B6629CF(__this, NULL);
						return;
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					goto IL_002e_1;
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_002e_1:
			{
				goto IL_0030_1;
			}

IL_0030_1:
			{
				goto IL_0039;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0039:
	{
		goto IL_003b;
	}

IL_003b:
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityPath/<TravserseDir>d__38::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTravserseDirU3Ed__38_MoveNext_m33A87F949659CCBB3F48694B31A51E5B0CE1E47E (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_tFB08D1F3BD39D06909F81B8970F3EE1C5BFCA985_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t310635646FFD6D85F990F91808E4E20E4A401E26_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		auto __finallyBlock = il2cpp::utils::Fault([&]
		{

FAULT_0147:
			{// begin fault (depth: 1)
				U3CTravserseDirU3Ed__38_System_IDisposable_Dispose_mDA429B4DB1D8469BB75DC0AE06F726BEED7ADFB4(__this, NULL);
				return;
			}// end fault
		});
		try
		{// begin try (depth: 1)
			{
				int32_t L_0 = __this->___U3CU3E1__state_0;
				V_1 = L_0;
				int32_t L_1 = V_1;
				switch (L_1)
				{
					case 0:
					{
						goto IL_001b_1;
					}
					case 1:
					{
						goto IL_001d_1;
					}
					case 2:
					{
						goto IL_001f_1;
					}
				}
			}
			{
				goto IL_0024_1;
			}

IL_001b_1:
			{
				goto IL_002b_1;
			}

IL_001d_1:
			{
				goto IL_0060_1;
			}

IL_001f_1:
			{
				goto IL_00e7_1;
			}

IL_0024_1:
			{
				V_0 = (bool)0;
				goto IL_014f;
			}

IL_002b_1:
			{
				__this->___U3CU3E1__state_0 = (-1);
				// if (IsDirectoryExists)
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* L_2 = (&__this->___U3CU3E4__this_3);
				bool L_3;
				L_3 = UnityPath_get_IsDirectoryExists_mA16B832020034FE21285DA9F89B197D33FA6C7B1(L_2, NULL);
				V_2 = L_3;
				bool L_4 = V_2;
				if (!L_4)
				{
					goto IL_0143_1;
				}
			}
			{
				// yield return this;
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_5 = __this->___U3CU3E4__this_3;
				__this->___U3CU3E2__current_1 = L_5;
				Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CU3E2__current_1))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
				__this->___U3CU3E1__state_0 = 1;
				V_0 = (bool)1;
				goto IL_014f;
			}

IL_0060_1:
			{
				__this->___U3CU3E1__state_0 = (-1);
				// foreach(var child in ChildDirs)
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* L_6 = (&__this->___U3CU3E4__this_3);
				RuntimeObject* L_7;
				L_7 = UnityPath_get_ChildDirs_mC69F9F662DBBD446BFCC738929BEB776F7FF839B(L_6, NULL);
				NullCheck(L_7);
				RuntimeObject* L_8;
				L_8 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath>::GetEnumerator() */, IEnumerable_1_tFB08D1F3BD39D06909F81B8970F3EE1C5BFCA985_il2cpp_TypeInfo_var, L_7);
				__this->___U3CU3Es__1_5 = L_8;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__1_5), (void*)L_8);
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				goto IL_0124_1;
			}

IL_008b_1:
			{
				// foreach(var child in ChildDirs)
				RuntimeObject* L_9 = __this->___U3CU3Es__1_5;
				NullCheck(L_9);
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_10;
				L_10 = InterfaceFuncInvoker0< UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath>::get_Current() */, IEnumerator_1_t310635646FFD6D85F990F91808E4E20E4A401E26_il2cpp_TypeInfo_var, L_9);
				__this->___U3CchildU3E5__2_6 = L_10;
				Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CchildU3E5__2_6))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
				// foreach(var x in child.TravserseDir())
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* L_11 = (&__this->___U3CchildU3E5__2_6);
				RuntimeObject* L_12;
				L_12 = UnityPath_TravserseDir_m7F543EC20E1D086B71902D307A686AD9AC2E02DF(L_11, NULL);
				NullCheck(L_12);
				RuntimeObject* L_13;
				L_13 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UniGLTF.UnityPath>::GetEnumerator() */, IEnumerable_1_tFB08D1F3BD39D06909F81B8970F3EE1C5BFCA985_il2cpp_TypeInfo_var, L_12);
				__this->___U3CU3Es__3_7 = L_13;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__3_7), (void*)L_13);
				__this->___U3CU3E1__state_0 = ((int32_t)-4);
				goto IL_00fc_1;
			}

IL_00be_1:
			{
				// foreach(var x in child.TravserseDir())
				RuntimeObject* L_14 = __this->___U3CU3Es__3_7;
				NullCheck(L_14);
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_15;
				L_15 = InterfaceFuncInvoker0< UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath>::get_Current() */, IEnumerator_1_t310635646FFD6D85F990F91808E4E20E4A401E26_il2cpp_TypeInfo_var, L_14);
				__this->___U3CxU3E5__4_8 = L_15;
				Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CxU3E5__4_8))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
				// yield return x;
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_16 = __this->___U3CxU3E5__4_8;
				__this->___U3CU3E2__current_1 = L_16;
				Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CU3E2__current_1))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
				__this->___U3CU3E1__state_0 = 2;
				V_0 = (bool)1;
				goto IL_014f;
			}

IL_00e7_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-4);
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* L_17 = (&__this->___U3CxU3E5__4_8);
				il2cpp_codegen_initobj(L_17, sizeof(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044));
			}

IL_00fc_1:
			{
				// foreach(var x in child.TravserseDir())
				RuntimeObject* L_18 = __this->___U3CU3Es__3_7;
				NullCheck(L_18);
				bool L_19;
				L_19 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_18);
				if (L_19)
				{
					goto IL_00be_1;
				}
			}
			{
				U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m07C881555177FBA1B9E288AAF6DD15240B6629CF(__this, NULL);
				__this->___U3CU3Es__3_7 = (RuntimeObject*)NULL;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__3_7), (void*)(RuntimeObject*)NULL);
				UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* L_20 = (&__this->___U3CchildU3E5__2_6);
				il2cpp_codegen_initobj(L_20, sizeof(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044));
			}

IL_0124_1:
			{
				// foreach(var child in ChildDirs)
				RuntimeObject* L_21 = __this->___U3CU3Es__1_5;
				NullCheck(L_21);
				bool L_22;
				L_22 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_21);
				if (L_22)
				{
					goto IL_008b_1;
				}
			}
			{
				U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_m836DFCF69E62A1CD912EE3D5A6A97E7E6669BFC7(__this, NULL);
				__this->___U3CU3Es__1_5 = (RuntimeObject*)NULL;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__1_5), (void*)(RuntimeObject*)NULL);
			}

IL_0143_1:
			{
				// }
				V_0 = (bool)0;
				goto IL_014f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_014f:
	{
		bool L_23 = V_0;
		return L_23;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally1_m836DFCF69E62A1CD912EE3D5A6A97E7E6669BFC7 (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___U3CU3E1__state_0 = (-1);
		RuntimeObject* L_0 = __this->___U3CU3Es__1_5;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_1 = __this->___U3CU3Es__1_5;
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_1);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::<>m__Finally2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_U3CU3Em__Finally2_m07C881555177FBA1B9E288AAF6DD15240B6629CF (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___U3CU3E1__state_0 = ((int32_t)-3);
		RuntimeObject* L_0 = __this->___U3CU3Es__3_7;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_1 = __this->___U3CU3Es__3_7;
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_1);
	}

IL_001c:
	{
		return;
	}
}
// UniGLTF.UnityPath UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.Generic.IEnumerator<UniGLTF.UnityPath>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumeratorU3CUniGLTF_UnityPathU3E_get_Current_mDD81A8EB84D8822D14D2E7963292DE5EF9449B3F (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	{
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_Reset_mBEC50C35327A76E8A8AC3A4D1E64B95B8CBF3CCB (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_Reset_mBEC50C35327A76E8A8AC3A4D1E64B95B8CBF3CCB_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTravserseDirU3Ed__38_System_Collections_IEnumerator_get_Current_mE7EB3467D765F8D4BE0636A9A1CA589853A3960B (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_0 = __this->___U3CU3E2__current_1;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_1 = L_0;
		RuntimeObject* L_2 = Box(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m976C22CB56F3061BF80191C72CE73438C09C61B0 (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* V_0 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->___U3CU3El__initialThreadId_2;
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m66483AADCCC13272EBDCD94D31D2E52603C24BDF(NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->___U3CU3E1__state_0 = 0;
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* L_3 = (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31*)il2cpp_codegen_object_new(U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		U3CTravserseDirU3Ed__38__ctor_mD53E5B3DB6363090C92EB54659036813FA81557F(L_3, 0, NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* L_4 = V_0;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_5 = __this->___U3CU3E3__U3CU3E4__this_4;
		NullCheck(L_4);
		L_4->___U3CU3E4__this_3 = L_5;
		Il2CppCodeGenWriteBarrier((void**)&(((&L_4->___U3CU3E4__this_3))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityPath/<TravserseDir>d__38::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CTravserseDirU3Ed__38_System_Collections_IEnumerable_GetEnumerator_m1FAEE9F7EB5E3B26FC1A23F2BE3B0B373CDA06A2 (U3CTravserseDirU3Ed__38_t6D4A86AB329D58180E3D2C714308B8695FF25E31* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = U3CTravserseDirU3Ed__38_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m976C22CB56F3061BF80191C72CE73438C09C61B0(__this, NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40__ctor_m075883A29603CBDABA82EA828B9537AB4F2C52BE (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___0_U3CU3E1__state;
		__this->___U3CU3E1__state_0 = L_0;
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m66483AADCCC13272EBDCD94D31D2E52603C24BDF(NULL);
		__this->___U3CU3El__initialThreadId_2 = L_1;
		return;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40_System_IDisposable_Dispose_mA46E6D517002AAEFDE17B58EA2536BB78A6049C5 (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityPath/<get_ChildDirs>d__40::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3Cget_ChildDirsU3Ed__40_MoveNext_mE063D8AE309472FE40A4E13F810227696412E837 (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_006e;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// foreach(var x in Directory.GetDirectories(FullPath))
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* L_3 = (&__this->___U3CU3E4__this_3);
		String_t* L_4;
		L_4 = UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322(L_3, NULL);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5;
		L_5 = Directory_GetDirectories_m071EF47103F7A38FBF882C900F0A0AFB4326631F(L_4, NULL);
		__this->___U3CU3Es__1_5 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__1_5), (void*)L_5);
		__this->___U3CU3Es__2_6 = 0;
		goto IL_008b;
	}

IL_0040:
	{
		// foreach(var x in Directory.GetDirectories(FullPath))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = __this->___U3CU3Es__1_5;
		int32_t L_7 = __this->___U3CU3Es__2_6;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		__this->___U3CxU3E5__3_7 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CxU3E5__3_7), (void*)L_9);
		// yield return UnityPath.FromFullpath(x);
		String_t* L_10 = __this->___U3CxU3E5__3_7;
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_11;
		L_11 = UnityPath_FromFullpath_m9B741FAFE56705ADA157667B1CEB2880293EC6D9(L_10, NULL);
		__this->___U3CU3E2__current_1 = L_11;
		Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CU3E2__current_1))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_006e:
	{
		__this->___U3CU3E1__state_0 = (-1);
		__this->___U3CxU3E5__3_7 = (String_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CxU3E5__3_7), (void*)(String_t*)NULL);
		int32_t L_12 = __this->___U3CU3Es__2_6;
		__this->___U3CU3Es__2_6 = ((int32_t)il2cpp_codegen_add(L_12, 1));
	}

IL_008b:
	{
		// foreach(var x in Directory.GetDirectories(FullPath))
		int32_t L_13 = __this->___U3CU3Es__2_6;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = __this->___U3CU3Es__1_5;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))))
		{
			goto IL_0040;
		}
	}
	{
		__this->___U3CU3Es__1_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__1_5), (void*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)NULL);
		// }
		return (bool)0;
	}
}
// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.Generic.IEnumerator<UniGLTF.UnityPath>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumeratorU3CUniGLTF_UnityPathU3E_get_Current_mDDE72C83F208ACA35BAE5552CF1735881EB8C9CA (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) 
{
	{
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_Reset_mDD69EDB64602CD1FD701CFBF9D380F8D74B2DFEB (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_Reset_mDD69EDB64602CD1FD701CFBF9D380F8D74B2DFEB_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerator_get_Current_m5705FDA96B7E31ADF0996275EE9DC40503391764 (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_0 = __this->___U3CU3E2__current_1;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_1 = L_0;
		RuntimeObject* L_2 = Box(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mEF13C0755B5C7882DCCF7A7CE6855A0F1957054F (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* V_0 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->___U3CU3El__initialThreadId_2;
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m66483AADCCC13272EBDCD94D31D2E52603C24BDF(NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->___U3CU3E1__state_0 = 0;
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* L_3 = (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74*)il2cpp_codegen_object_new(U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		U3Cget_ChildDirsU3Ed__40__ctor_m075883A29603CBDABA82EA828B9537AB4F2C52BE(L_3, 0, NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* L_4 = V_0;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_5 = __this->___U3CU3E3__U3CU3E4__this_4;
		NullCheck(L_4);
		L_4->___U3CU3E4__this_3 = L_5;
		Il2CppCodeGenWriteBarrier((void**)&(((&L_4->___U3CU3E4__this_3))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityPath/<get_ChildDirs>d__40::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildDirsU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m7865A2ACE534807EB8488D9423CFF04F0B384E06 (U3Cget_ChildDirsU3Ed__40_t4B400481432AC780AB2BEDBFD5A90D2A4A72BD74* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = U3Cget_ChildDirsU3Ed__40_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_mEF13C0755B5C7882DCCF7A7CE6855A0F1957054F(__this, NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42__ctor_m47F9CE437A24FE8B4385933C997775D49A7CB8E9 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___0_U3CU3E1__state;
		__this->___U3CU3E1__state_0 = L_0;
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m66483AADCCC13272EBDCD94D31D2E52603C24BDF(NULL);
		__this->___U3CU3El__initialThreadId_2 = L_1;
		return;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42_System_IDisposable_Dispose_mA99B90F8A0A2C2BD4DDD831CE6DDF7522323A1B8 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean UniGLTF.UnityPath/<get_ChildFiles>d__42::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3Cget_ChildFilesU3Ed__42_MoveNext_mC06971012A0B82828F991C59FB45BCC474710E9B (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_006e;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// foreach (var x in Directory.GetFiles(FullPath))
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* L_3 = (&__this->___U3CU3E4__this_3);
		String_t* L_4;
		L_4 = UnityPath_get_FullPath_mAE03E9200D0D1FAF7EF39AAEB6410D3744287322(L_3, NULL);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5;
		L_5 = Directory_GetFiles_m3E6AA407767C85CD62C5FD2750747274D1C4EA76(L_4, NULL);
		__this->___U3CU3Es__1_5 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__1_5), (void*)L_5);
		__this->___U3CU3Es__2_6 = 0;
		goto IL_008b;
	}

IL_0040:
	{
		// foreach (var x in Directory.GetFiles(FullPath))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = __this->___U3CU3Es__1_5;
		int32_t L_7 = __this->___U3CU3Es__2_6;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		__this->___U3CxU3E5__3_7 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CxU3E5__3_7), (void*)L_9);
		// yield return UnityPath.FromFullpath(x);
		String_t* L_10 = __this->___U3CxU3E5__3_7;
		il2cpp_codegen_runtime_class_init_inline(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_11;
		L_11 = UnityPath_FromFullpath_m9B741FAFE56705ADA157667B1CEB2880293EC6D9(L_10, NULL);
		__this->___U3CU3E2__current_1 = L_11;
		Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CU3E2__current_1))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_006e:
	{
		__this->___U3CU3E1__state_0 = (-1);
		__this->___U3CxU3E5__3_7 = (String_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CxU3E5__3_7), (void*)(String_t*)NULL);
		int32_t L_12 = __this->___U3CU3Es__2_6;
		__this->___U3CU3Es__2_6 = ((int32_t)il2cpp_codegen_add(L_12, 1));
	}

IL_008b:
	{
		// foreach (var x in Directory.GetFiles(FullPath))
		int32_t L_13 = __this->___U3CU3Es__2_6;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = __this->___U3CU3Es__1_5;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))))
		{
			goto IL_0040;
		}
	}
	{
		__this->___U3CU3Es__1_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3Es__1_5), (void*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)NULL);
		// }
		return (bool)0;
	}
}
// UniGLTF.UnityPath UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.Generic.IEnumerator<UniGLTF.UnityPath>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumeratorU3CUniGLTF_UnityPathU3E_get_Current_mAE42A8C49006F9DFE60EEDBD966177C5D1D6620D (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) 
{
	{
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_Reset_m7DCD6738D4EB8D0ECB157C454986048E32DBFD17 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_Reset_m7DCD6738D4EB8D0ECB157C454986048E32DBFD17_RuntimeMethod_var)));
	}
}
// System.Object UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerator_get_Current_mAC7835C890BC67CA8AF6A08C2B4BC74E7F00A3F4 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_0 = __this->___U3CU3E2__current_1;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_1 = L_0;
		RuntimeObject* L_2 = Box(UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UniGLTF.UnityPath> UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.Generic.IEnumerable<UniGLTF.UnityPath>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m78470E87107F961637FE6335558FB1402513DA25 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* V_0 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->___U3CU3El__initialThreadId_2;
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m66483AADCCC13272EBDCD94D31D2E52603C24BDF(NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->___U3CU3E1__state_0 = 0;
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* L_3 = (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35*)il2cpp_codegen_object_new(U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		U3Cget_ChildFilesU3Ed__42__ctor_m47F9CE437A24FE8B4385933C997775D49A7CB8E9(L_3, 0, NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* L_4 = V_0;
		UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044 L_5 = __this->___U3CU3E3__U3CU3E4__this_4;
		NullCheck(L_4);
		L_4->___U3CU3E4__this_3 = L_5;
		Il2CppCodeGenWriteBarrier((void**)&(((&L_4->___U3CU3E4__this_3))->___U3CValueU3Ek__BackingField_0), (void*)NULL);
		U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator UniGLTF.UnityPath/<get_ChildFiles>d__42::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3Cget_ChildFilesU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m0A653C989FA180F5E0D44F58C5147AC9C36DFA51 (U3Cget_ChildFilesU3Ed__42_t27C2D126E4B0C0C8227B824A356E44D776856B35* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = U3Cget_ChildFilesU3Ed__42_System_Collections_Generic_IEnumerableU3CUniGLTF_UnityPathU3E_GetEnumerator_m78470E87107F961637FE6335558FB1402513DA25(__this, NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UniGLTFException::.ctor(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniGLTFException__ctor_m2B242ABD89445241E2C6C8A90BAB1F6E41E8B819 (UniGLTFException_t34581B33464F9CE4820C4F4A2A441F7E9C22365F* __this, String_t* ___0_fmt, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___1_args, const RuntimeMethod* method) 
{
	{
		// public UniGLTFException(string fmt, params object[] args) : this(string.Format(fmt, args)) { }
		String_t* L_0 = ___0_fmt;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = ___1_args;
		String_t* L_2;
		L_2 = String_Format_m918500C1EFB475181349A79989BB79BB36102894(L_0, L_1, NULL);
		UniGLTFException__ctor_m1DB21B5842F46FF3E9D916D20F88C027A333B6FD(__this, L_2, NULL);
		// public UniGLTFException(string fmt, params object[] args) : this(string.Format(fmt, args)) { }
		return;
	}
}
// System.Void UniGLTF.UniGLTFException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniGLTFException__ctor_m1DB21B5842F46FF3E9D916D20F88C027A333B6FD (UniGLTFException_t34581B33464F9CE4820C4F4A2A441F7E9C22365F* __this, String_t* ___0_msg, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public UniGLTFException(string msg) : base(msg) { }
		String_t* L_0 = ___0_msg;
		il2cpp_codegen_runtime_class_init_inline(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(__this, L_0, NULL);
		// public UniGLTFException(string msg) : base(msg) { }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UniGLTFNotSupportedException::.ctor(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniGLTFNotSupportedException__ctor_m9848DC6E9FFDB27330EA6C87906B71B2CFFCAE77 (UniGLTFNotSupportedException_t67ABEDC648B5033BE849FB4637988BDC4A6FCF19* __this, String_t* ___0_fmt, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___1_args, const RuntimeMethod* method) 
{
	{
		// public UniGLTFNotSupportedException(string fmt, params object[] args) : this(string.Format(fmt, args)) { }
		String_t* L_0 = ___0_fmt;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = ___1_args;
		String_t* L_2;
		L_2 = String_Format_m918500C1EFB475181349A79989BB79BB36102894(L_0, L_1, NULL);
		UniGLTFNotSupportedException__ctor_m2A0E272781AEADB0BA1E81CFE83D740F84329E9C(__this, L_2, NULL);
		// public UniGLTFNotSupportedException(string fmt, params object[] args) : this(string.Format(fmt, args)) { }
		return;
	}
}
// System.Void UniGLTF.UniGLTFNotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniGLTFNotSupportedException__ctor_m2A0E272781AEADB0BA1E81CFE83D740F84329E9C (UniGLTFNotSupportedException_t67ABEDC648B5033BE849FB4637988BDC4A6FCF19* __this, String_t* ___0_msg, const RuntimeMethod* method) 
{
	{
		// public UniGLTFNotSupportedException(string msg) : base(msg) { }
		String_t* L_0 = ___0_msg;
		UniGLTFException__ctor_m1DB21B5842F46FF3E9D916D20F88C027A333B6FD(__this, L_0, NULL);
		// public UniGLTFNotSupportedException(string msg) : base(msg) { }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.UniUnlit.Utils::SetRenderMode(UnityEngine.Material,UniGLTF.UniUnlit.UniUnlitRenderMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetRenderMode_m2E8E3D0EDF94F543048B8A7E747A24A718BB1313 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, int32_t ___1_mode, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// material.SetInt(PropNameBlendMode, (int)mode);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ___0_material;
		int32_t L_1 = ___1_mode;
		NullCheck(L_0);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_0, _stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F, L_1, NULL);
		// }
		return;
	}
}
// System.Void UniGLTF.UniUnlit.Utils::SetCullMode(UnityEngine.Material,UniGLTF.UniUnlit.UniUnlitCullMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetCullMode_m7CAC6A2992C7006BE65F828E5D7BD131E6E27282 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, int32_t ___1_mode, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59B259A263D1796F7617C20534034F94A19001FE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// material.SetInt(PropNameCullMode, (int) mode);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ___0_material;
		int32_t L_1 = ___1_mode;
		NullCheck(L_0);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_0, _stringLiteral59B259A263D1796F7617C20534034F94A19001FE, L_1, NULL);
		// }
		return;
	}
}
// UniGLTF.UniUnlit.UniUnlitRenderMode UniGLTF.UniUnlit.Utils::GetRenderMode(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Utils_GetRenderMode_mF247F1C64A92C7FC4EE39071F7E8E7BEB2E1BB2E (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// return (UniUnlitRenderMode)material.GetInt(PropNameBlendMode);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ___0_material;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Material_GetInt_mA772B615274DD11B37A352BC66EFA81BFD9C13EA(L_0, _stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// UniGLTF.UniUnlit.UniUnlitCullMode UniGLTF.UniUnlit.Utils::GetCullMode(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Utils_GetCullMode_mEA09290995F4A2C50DDC7B9F2FC855BCBDB2D570 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59B259A263D1796F7617C20534034F94A19001FE);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// return (UniUnlitCullMode)material.GetInt(PropNameCullMode);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ___0_material;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Material_GetInt_mA772B615274DD11B37A352BC66EFA81BFD9C13EA(L_0, _stringLiteral59B259A263D1796F7617C20534034F94A19001FE, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UniGLTF.UniUnlit.Utils::ValidateProperties(UnityEngine.Material,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_ValidateProperties_m0DD5DF93CA23929DD4BAF819FE602386D81BEB10 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, bool ___1_isRenderModeChangedByUser, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8E54DC0584021FDD77DA842B94FD97F28B5A628);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SetupBlendMode(material, (UniUnlitRenderMode)material.GetFloat(PropNameBlendMode),
		//     isRenderModeChangedByUser);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = ___0_material;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_1 = ___0_material;
		NullCheck(L_1);
		float L_2;
		L_2 = Material_GetFloat_m2A77F10E6AA13EA3FA56166EFEA897115A14FA5A(L_1, _stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F, NULL);
		bool L_3 = ___1_isRenderModeChangedByUser;
		Utils_SetupBlendMode_m67BD9C8AEB6487A1BDDAEE412728A8ED80CD5C30(L_0, il2cpp_codegen_cast_double_to_int<int32_t>(L_2), L_3, NULL);
		// SetupVertexColorBlendOp(material, (UniUnlitVertexColorBlendOp)material.GetFloat(PropeNameVColBlendMode));
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_4 = ___0_material;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_5 = ___0_material;
		NullCheck(L_5);
		float L_6;
		L_6 = Material_GetFloat_m2A77F10E6AA13EA3FA56166EFEA897115A14FA5A(L_5, _stringLiteralC8E54DC0584021FDD77DA842B94FD97F28B5A628, NULL);
		Utils_SetupVertexColorBlendOp_mF04EB4BCCB123D9AF4CC930EF9A4790994BE8614(L_4, il2cpp_codegen_cast_double_to_int<int32_t>(L_6), NULL);
		// }
		return;
	}
}
// System.Void UniGLTF.UniUnlit.Utils::SetupBlendMode(UnityEngine.Material,UniGLTF.UniUnlit.UniUnlitRenderMode,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetupBlendMode_m67BD9C8AEB6487A1BDDAEE412728A8ED80CD5C30 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, int32_t ___1_renderMode, bool ___2_isRenderModeChangedByUser, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3223D004C9379F2B3083B62944F7924A92190F90);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral52CBE4A5A42509939BB9DB58A4B9A2C9BBA81C1C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAC5B3304C047D60B4E3EC2809E3CE3FA62191A79);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCBCD3D866AF896F9B010A0FEE7F961DBC91A08C5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCCE1912E091B2153DFAE28F4F55D34CD3C4EF3D4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF318A9CBF6133558944579D6309707D3FF4760E1);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	{
		// switch (renderMode)
		int32_t L_0 = ___1_renderMode;
		V_1 = L_0;
		int32_t L_1 = V_1;
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0080;
			}
			case 2:
			{
				goto IL_00e5;
			}
		}
	}
	{
		goto IL_014d;
	}

IL_001c:
	{
		// material.SetOverrideTag(TagRenderTypeKey, TagRenderTypeValueOpaque);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = ___0_material;
		NullCheck(L_3);
		Material_SetOverrideTag_mD68833CD28EBAF71CB6AF127B38075629B74FE08(L_3, _stringLiteralCCE1912E091B2153DFAE28F4F55D34CD3C4EF3D4, _stringLiteral3223D004C9379F2B3083B62944F7924A92190F90, NULL);
		// material.SetInt(PropNameSrcBlend, (int)BlendMode.One);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_4 = ___0_material;
		NullCheck(L_4);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_4, _stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F, 1, NULL);
		// material.SetInt(PropNameDstBlend, (int)BlendMode.Zero);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_5 = ___0_material;
		NullCheck(L_5);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_5, _stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A, 0, NULL);
		// material.SetInt(PropNameZWrite, 1);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_6 = ___0_material;
		NullCheck(L_6);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_6, _stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C, 1, NULL);
		// SetKeyword(material, KeywordAlphaTestOn, false);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_7 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_7, _stringLiteralCBCD3D866AF896F9B010A0FEE7F961DBC91A08C5, (bool)0, NULL);
		// SetKeyword(material, KeywordAlphaBlendOn, false);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_8 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_8, _stringLiteral52CBE4A5A42509939BB9DB58A4B9A2C9BBA81C1C, (bool)0, NULL);
		// if (isRenderModeChangedByUser) material.renderQueue = -1;
		bool L_9 = ___2_isRenderModeChangedByUser;
		V_2 = L_9;
		bool L_10 = V_2;
		if (!L_10)
		{
			goto IL_007b;
		}
	}
	{
		// if (isRenderModeChangedByUser) material.renderQueue = -1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_11 = ___0_material;
		NullCheck(L_11);
		Material_set_renderQueue_mFBB72A781DCCF0D4B85670B597788EC2D02D1C14(L_11, (-1), NULL);
	}

IL_007b:
	{
		// break;
		goto IL_014d;
	}

IL_0080:
	{
		// material.SetOverrideTag(TagRenderTypeKey, TagRenderTypeValueTransparentCutout);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_12 = ___0_material;
		NullCheck(L_12);
		Material_SetOverrideTag_mD68833CD28EBAF71CB6AF127B38075629B74FE08(L_12, _stringLiteralCCE1912E091B2153DFAE28F4F55D34CD3C4EF3D4, _stringLiteralAC5B3304C047D60B4E3EC2809E3CE3FA62191A79, NULL);
		// material.SetInt(PropNameSrcBlend, (int)BlendMode.One);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_13 = ___0_material;
		NullCheck(L_13);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_13, _stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F, 1, NULL);
		// material.SetInt(PropNameDstBlend, (int)BlendMode.Zero);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_14 = ___0_material;
		NullCheck(L_14);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_14, _stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A, 0, NULL);
		// material.SetInt(PropNameZWrite, 1);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_15 = ___0_material;
		NullCheck(L_15);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_15, _stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C, 1, NULL);
		// SetKeyword(material, KeywordAlphaTestOn, true);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_16 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_16, _stringLiteralCBCD3D866AF896F9B010A0FEE7F961DBC91A08C5, (bool)1, NULL);
		// SetKeyword(material, KeywordAlphaBlendOn, false);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_17 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_17, _stringLiteral52CBE4A5A42509939BB9DB58A4B9A2C9BBA81C1C, (bool)0, NULL);
		// if (isRenderModeChangedByUser) material.renderQueue = (int)RenderQueue.AlphaTest;
		bool L_18 = ___2_isRenderModeChangedByUser;
		V_3 = L_18;
		bool L_19 = V_3;
		if (!L_19)
		{
			goto IL_00e3;
		}
	}
	{
		// if (isRenderModeChangedByUser) material.renderQueue = (int)RenderQueue.AlphaTest;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_20 = ___0_material;
		NullCheck(L_20);
		Material_set_renderQueue_mFBB72A781DCCF0D4B85670B597788EC2D02D1C14(L_20, ((int32_t)2450), NULL);
	}

IL_00e3:
	{
		// break;
		goto IL_014d;
	}

IL_00e5:
	{
		// material.SetOverrideTag(TagRenderTypeKey, TagRenderTypeValueTransparent);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_21 = ___0_material;
		NullCheck(L_21);
		Material_SetOverrideTag_mD68833CD28EBAF71CB6AF127B38075629B74FE08(L_21, _stringLiteralCCE1912E091B2153DFAE28F4F55D34CD3C4EF3D4, _stringLiteralF318A9CBF6133558944579D6309707D3FF4760E1, NULL);
		// material.SetInt(PropNameSrcBlend, (int)BlendMode.SrcAlpha);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_22 = ___0_material;
		NullCheck(L_22);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_22, _stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F, 5, NULL);
		// material.SetInt(PropNameDstBlend, (int)BlendMode.OneMinusSrcAlpha);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_23 = ___0_material;
		NullCheck(L_23);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_23, _stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A, ((int32_t)10), NULL);
		// material.SetInt(PropNameZWrite, 0);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_24 = ___0_material;
		NullCheck(L_24);
		Material_SetInt_m41DF5404A9942239265888105E1DC83F2FBF901A(L_24, _stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C, 0, NULL);
		// SetKeyword(material, KeywordAlphaTestOn, false);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_25 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_25, _stringLiteralCBCD3D866AF896F9B010A0FEE7F961DBC91A08C5, (bool)0, NULL);
		// SetKeyword(material, KeywordAlphaBlendOn, true);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_26 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_26, _stringLiteral52CBE4A5A42509939BB9DB58A4B9A2C9BBA81C1C, (bool)1, NULL);
		// if (isRenderModeChangedByUser) material.renderQueue = (int)RenderQueue.Transparent;
		bool L_27 = ___2_isRenderModeChangedByUser;
		V_4 = L_27;
		bool L_28 = V_4;
		if (!L_28)
		{
			goto IL_014b;
		}
	}
	{
		// if (isRenderModeChangedByUser) material.renderQueue = (int)RenderQueue.Transparent;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_29 = ___0_material;
		NullCheck(L_29);
		Material_set_renderQueue_mFBB72A781DCCF0D4B85670B597788EC2D02D1C14(L_29, ((int32_t)3000), NULL);
	}

IL_014b:
	{
		// break;
		goto IL_014d;
	}

IL_014d:
	{
		// }
		return;
	}
}
// System.Void UniGLTF.UniUnlit.Utils::SetupVertexColorBlendOp(UnityEngine.Material,UniGLTF.UniUnlit.UniUnlitVertexColorBlendOp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetupVertexColorBlendOp_mF04EB4BCCB123D9AF4CC930EF9A4790994BE8614 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_material, int32_t ___1_vColBlendOp, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral225A37DFF8BC62C67372C9BB874220E7FD56C613);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// switch (vColBlendOp)
		int32_t L_0 = ___1_vColBlendOp;
		V_1 = L_0;
		int32_t L_1 = V_1;
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002e;
	}

IL_0010:
	{
		// SetKeyword(material, KeywordVertexColMul, false);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_4 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_4, _stringLiteral225A37DFF8BC62C67372C9BB874220E7FD56C613, (bool)0, NULL);
		// break;
		goto IL_002e;
	}

IL_001f:
	{
		// SetKeyword(material, KeywordVertexColMul, true);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_5 = ___0_material;
		Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3(L_5, _stringLiteral225A37DFF8BC62C67372C9BB874220E7FD56C613, (bool)1, NULL);
		// break;
		goto IL_002e;
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void UniGLTF.UniUnlit.Utils::SetKeyword(UnityEngine.Material,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetKeyword_mAF80DCA4D393E5553810765E7CB91ED8541F4AC3 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_mat, String_t* ___1_keyword, bool ___2_required, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		// if (required)
		bool L_0 = ___2_required;
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// mat.EnableKeyword(keyword);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_2 = ___0_mat;
		String_t* L_3 = ___1_keyword;
		NullCheck(L_2);
		Material_EnableKeyword_mE8523EF6CF694284DF976D47ADEDE9363A1174AC(L_2, L_3, NULL);
		goto IL_0018;
	}

IL_0010:
	{
		// mat.DisableKeyword(keyword);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_4 = ___0_mat;
		String_t* L_5 = ___1_keyword;
		NullCheck(L_4);
		Material_DisableKeyword_mC123927EBF2F2A19220A4456C8EA19F2BA416E8C(L_4, L_5, NULL);
	}

IL_0018:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.Zip.ZipParseException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipParseException__ctor_m09C74A3829DD1EA07929D6B4427C76405E8995AD (ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7* __this, String_t* ___0_msg, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public ZipParseException(string msg) : base(msg)
		String_t* L_0 = ___0_msg;
		il2cpp_codegen_runtime_class_init_inline(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(__this, L_0, NULL);
		// { }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UniGLTF.Zip.EOCD::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EOCD_ToString_m509BE78B6A851D1F5A48D590920D37D828A6E865 (EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral238A3EDAA39C6330200DAB01655AF427B9A18243);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return string.Format("<EOCD records: {0}, offset: {1}, '{2}'>",
		//     NumberOfCentralDirectoryRecordsOnThisDisk,
		//     OffsetOfStartOfCentralDirectory,
		//     Comment
		//     );
		uint16_t L_0 = __this->___NumberOfCentralDirectoryRecordsOnThisDisk_2;
		uint16_t L_1 = L_0;
		RuntimeObject* L_2 = Box(UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = __this->___OffsetOfStartOfCentralDirectory_5;
		int32_t L_4 = L_3;
		RuntimeObject* L_5 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = __this->___Comment_6;
		String_t* L_7;
		L_7 = String_Format_mA0534D6E2AE4D67A6BD8D45B3321323930EB930C(_stringLiteral238A3EDAA39C6330200DAB01655AF427B9A18243, L_2, L_5, L_6, NULL);
		V_0 = L_7;
		goto IL_002a;
	}

IL_002a:
	{
		// }
		String_t* L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UniGLTF.Zip.EOCD::FindEOCD(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t EOCD_FindEOCD_mE6FA16E8F09A0ABAE86EC9D8E519235F3A9B8049 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t G_B6_0 = 0;
	{
		// for (int i = bytes.Length - 22; i >= 0; --i)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_bytes;
		NullCheck(L_0);
		V_0 = ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_0)->max_length)), ((int32_t)22)));
		goto IL_003c;
	}

IL_000a:
	{
		// if (bytes[i] == 0x50
		//     && bytes[i + 1] == 0x4b
		//     && bytes[i + 2] == 0x05
		//     && bytes[i + 3] == 0x06)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ___0_bytes;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_002d;
		}
	}
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5 = ___0_bytes;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)75)))))
		{
			goto IL_002d;
		}
	}
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_9 = ___0_bytes;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = ((int32_t)il2cpp_codegen_add(L_10, 2));
		uint8_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		if ((!(((uint32_t)L_12) == ((uint32_t)5))))
		{
			goto IL_002d;
		}
	}
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_13 = ___0_bytes;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add(L_14, 3));
		uint8_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		G_B6_0 = ((((int32_t)L_16) == ((int32_t)6))? 1 : 0);
		goto IL_002e;
	}

IL_002d:
	{
		G_B6_0 = 0;
	}

IL_002e:
	{
		V_1 = (bool)G_B6_0;
		bool L_17 = V_1;
		if (!L_17)
		{
			goto IL_0037;
		}
	}
	{
		// return i;
		int32_t L_18 = V_0;
		V_2 = L_18;
		goto IL_0052;
	}

IL_0037:
	{
		// for (int i = bytes.Length - 22; i >= 0; --i)
		int32_t L_19 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract(L_19, 1));
	}

IL_003c:
	{
		// for (int i = bytes.Length - 22; i >= 0; --i)
		int32_t L_20 = V_0;
		V_3 = (bool)((((int32_t)((((int32_t)L_20) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_21 = V_3;
		if (L_21)
		{
			goto IL_000a;
		}
	}
	{
		// throw new ZipParseException("EOCD is not found");
		ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7* L_22 = (ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7_il2cpp_TypeInfo_var)));
		NullCheck(L_22);
		ZipParseException__ctor_m09C74A3829DD1EA07929D6B4427C76405E8995AD(L_22, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral04013643CAA39E451E8DA36D6C67223A195929B7)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EOCD_FindEOCD_mE6FA16E8F09A0ABAE86EC9D8E519235F3A9B8049_RuntimeMethod_var)));
	}

IL_0052:
	{
		// }
		int32_t L_23 = V_2;
		return L_23;
	}
}
// UniGLTF.Zip.EOCD UniGLTF.Zip.EOCD::Parse(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* EOCD_Parse_mF448F8F575BD23EE19FAC2660F16B4528C7D6B05 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* V_1 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* V_2 = NULL;
	int32_t V_3 = 0;
	EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* V_4 = NULL;
	uint16_t V_5 = 0;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_6 = NULL;
	bool V_7 = false;
	EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* V_8 = NULL;
	{
		// var pos = FindEOCD(bytes);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_bytes;
		int32_t L_1;
		L_1 = EOCD_FindEOCD_mE6FA16E8F09A0ABAE86EC9D8E519235F3A9B8049(L_0, NULL);
		V_0 = L_1;
		// using (var ms = new MemoryStream(bytes, pos, bytes.Length - pos, false))
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = ___0_bytes;
		int32_t L_3 = V_0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4 = ___0_bytes;
		NullCheck(L_4);
		int32_t L_5 = V_0;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_6 = (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)il2cpp_codegen_object_new(MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		MemoryStream__ctor_mC2A08AF3FC30A1DF60B2CFC5668637DF88B66444(L_6, L_2, L_3, ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_4)->max_length)), L_5)), (bool)0, NULL);
		V_1 = L_6;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00d2:
			{// begin finally (depth: 1)
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_7 = V_1;
					if (!L_7)
					{
						goto IL_00dc;
					}
				}
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_8 = V_1;
					NullCheck(L_8);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_8);
				}

IL_00dc:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				// using (var r = new BinaryReader(ms))
				MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_9 = V_1;
				BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_10 = (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158*)il2cpp_codegen_object_new(BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
				NullCheck(L_10);
				BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F(L_10, L_9, NULL);
				V_2 = L_10;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_00c7_1:
					{// begin finally (depth: 2)
						{
							BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_11 = V_2;
							if (!L_11)
							{
								goto IL_00d1_1;
							}
						}
						{
							BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_12 = V_2;
							NullCheck(L_12);
							InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_12);
						}

IL_00d1_1:
						{
							return;
						}
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						// var sig = r.ReadInt32();
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_13 = V_2;
						NullCheck(L_13);
						int32_t L_14;
						L_14 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_13);
						V_3 = L_14;
						// if (sig != 0x06054b50) throw new ZipParseException("invalid eocd signature: " + sig);
						int32_t L_15 = V_3;
						V_7 = (bool)((((int32_t)((((int32_t)L_15) == ((int32_t)((int32_t)101010256)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
						bool L_16 = V_7;
						if (!L_16)
						{
							goto IL_004d_2;
						}
					}
					{
						// if (sig != 0x06054b50) throw new ZipParseException("invalid eocd signature: " + sig);
						String_t* L_17;
						L_17 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_3), NULL);
						String_t* L_18;
						L_18 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF62160BCA579C7E0F393DF2FA5C372C809AB4B48)), L_17, NULL);
						ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7* L_19 = (ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7_il2cpp_TypeInfo_var)));
						NullCheck(L_19);
						ZipParseException__ctor_m09C74A3829DD1EA07929D6B4427C76405E8995AD(L_19, L_18, NULL);
						IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EOCD_Parse_mF448F8F575BD23EE19FAC2660F16B4528C7D6B05_RuntimeMethod_var)));
					}

IL_004d_2:
					{
						// var eocd = new EOCD
						// {
						//     NumberOfThisDisk = r.ReadUInt16(),
						//     DiskWhereCentralDirectoryStarts = r.ReadUInt16(),
						//     NumberOfCentralDirectoryRecordsOnThisDisk = r.ReadUInt16(),
						//     TotalNumberOfCentralDirectoryRecords = r.ReadUInt16(),
						//     SizeOfCentralDirectoryBytes = r.ReadInt32(),
						//     OffsetOfStartOfCentralDirectory = r.ReadInt32(),
						// };
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_20 = (EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380*)il2cpp_codegen_object_new(EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380_il2cpp_TypeInfo_var);
						NullCheck(L_20);
						EOCD__ctor_mC33629D5B65648FDAC1D13D0766DB704D595D966(L_20, NULL);
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_21 = L_20;
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_22 = V_2;
						NullCheck(L_22);
						uint16_t L_23;
						L_23 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_22);
						NullCheck(L_21);
						L_21->___NumberOfThisDisk_0 = L_23;
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_24 = L_21;
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_25 = V_2;
						NullCheck(L_25);
						uint16_t L_26;
						L_26 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_25);
						NullCheck(L_24);
						L_24->___DiskWhereCentralDirectoryStarts_1 = L_26;
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_27 = L_24;
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_28 = V_2;
						NullCheck(L_28);
						uint16_t L_29;
						L_29 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_28);
						NullCheck(L_27);
						L_27->___NumberOfCentralDirectoryRecordsOnThisDisk_2 = L_29;
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_30 = L_27;
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_31 = V_2;
						NullCheck(L_31);
						uint16_t L_32;
						L_32 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_31);
						NullCheck(L_30);
						L_30->___TotalNumberOfCentralDirectoryRecords_3 = L_32;
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_33 = L_30;
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_34 = V_2;
						NullCheck(L_34);
						int32_t L_35;
						L_35 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_34);
						NullCheck(L_33);
						L_33->___SizeOfCentralDirectoryBytes_4 = L_35;
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_36 = L_33;
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_37 = V_2;
						NullCheck(L_37);
						int32_t L_38;
						L_38 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_37);
						NullCheck(L_36);
						L_36->___OffsetOfStartOfCentralDirectory_5 = L_38;
						V_4 = L_36;
						// var commentLength = r.ReadUInt16();
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_39 = V_2;
						NullCheck(L_39);
						uint16_t L_40;
						L_40 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_39);
						V_5 = L_40;
						// var commentBytes = r.ReadBytes(commentLength);
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_41 = V_2;
						uint16_t L_42 = V_5;
						NullCheck(L_41);
						ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_43;
						L_43 = VirtualFuncInvoker1< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t >::Invoke(25 /* System.Byte[] System.IO.BinaryReader::ReadBytes(System.Int32) */, L_41, L_42);
						V_6 = L_43;
						// eocd.Comment = Encoding.ASCII.GetString(commentBytes);
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_44 = V_4;
						Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_45;
						L_45 = Encoding_get_ASCII_mCC61B512D320FD4E2E71CC0DFDF8DDF3CD215C65(NULL);
						ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_46 = V_6;
						NullCheck(L_45);
						String_t* L_47;
						L_47 = VirtualFuncInvoker1< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_45, L_46);
						NullCheck(L_44);
						L_44->___Comment_6 = L_47;
						Il2CppCodeGenWriteBarrier((void**)(&L_44->___Comment_6), (void*)L_47);
						// return eocd;
						EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_48 = V_4;
						V_8 = L_48;
						goto IL_00dd;
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00dd:
	{
		// }
		EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_49 = V_8;
		return L_49;
	}
}
// System.Void UniGLTF.Zip.EOCD::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EOCD__ctor_mC33629D5B65648FDAC1D13D0766DB704D595D966 (EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.Zip.CommonHeader::.ctor(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommonHeader__ctor_mC4C89B79E39052AC7FF022A55C70AF64AC1F2BAD (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, int32_t ___1_offset, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* V_3 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* V_4 = NULL;
	{
		// public Encoding Encoding = Encoding.UTF8;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_0;
		L_0 = Encoding_get_UTF8_m9FA98A53CE96FD6D02982625C5246DD36C1235C9(NULL);
		__this->___Encoding_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Encoding_0), (void*)L_0);
		// protected CommonHeader(Byte[] bytes, int offset)
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// var sig = BitConverter.ToInt32(bytes, offset);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ___0_bytes;
		int32_t L_2 = ___1_offset;
		il2cpp_codegen_runtime_class_init_inline(BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		int32_t L_3;
		L_3 = BitConverter_ToInt32_m259B4E62995575B80C4086347C867EB3C8D7DAB3(L_1, L_2, NULL);
		V_0 = L_3;
		// if (sig != Signature)
		int32_t L_4 = V_0;
		int32_t L_5;
		L_5 = VirtualFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UniGLTF.Zip.CommonHeader::get_Signature() */, __this);
		V_2 = (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		// throw new ZipParseException("invalid central directory file signature: " + sig);
		String_t* L_7;
		L_7 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_0), NULL);
		String_t* L_8;
		L_8 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralB00134732BE2DF9721B4BB74F2FB1BEE05CAFF55)), L_7, NULL);
		ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7* L_9 = (ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ZipParseException_t736D9C681175DACD39410CD293EC61FA940E98F7_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		ZipParseException__ctor_m09C74A3829DD1EA07929D6B4427C76405E8995AD(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CommonHeader__ctor_mC4C89B79E39052AC7FF022A55C70AF64AC1F2BAD_RuntimeMethod_var)));
	}

IL_0043:
	{
		// Bytes = bytes;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_10 = ___0_bytes;
		__this->___Bytes_1 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Bytes_1), (void*)L_10);
		// Offset = offset;
		int32_t L_11 = ___1_offset;
		__this->___Offset_2 = L_11;
		// var start = offset + 4;
		int32_t L_12 = ___1_offset;
		V_1 = ((int32_t)il2cpp_codegen_add(L_12, 4));
		// using (var ms = new MemoryStream(bytes, start, bytes.Length - start, false))
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_13 = ___0_bytes;
		int32_t L_14 = V_1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_15 = ___0_bytes;
		NullCheck(L_15);
		int32_t L_16 = V_1;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_17 = (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)il2cpp_codegen_object_new(MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		NullCheck(L_17);
		MemoryStream__ctor_mC2A08AF3FC30A1DF60B2CFC5668637DF88B66444(L_17, L_13, L_14, ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_15)->max_length)), L_16)), (bool)0, NULL);
		V_3 = L_17;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0099:
			{// begin finally (depth: 1)
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_18 = V_3;
					if (!L_18)
					{
						goto IL_00a3;
					}
				}
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_19 = V_3;
					NullCheck(L_19);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_19);
				}

IL_00a3:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				// using (var r = new BinaryReader(ms))
				MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_20 = V_3;
				BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_21 = (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158*)il2cpp_codegen_object_new(BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
				NullCheck(L_21);
				BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F(L_21, L_20, NULL);
				V_4 = L_21;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_008a_1:
					{// begin finally (depth: 2)
						{
							BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_22 = V_4;
							if (!L_22)
							{
								goto IL_0096_1;
							}
						}
						{
							BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_23 = V_4;
							NullCheck(L_23);
							InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_23);
						}

IL_0096_1:
						{
							return;
						}
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					// ReadBefore(r);
					BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_24 = V_4;
					VirtualActionInvoker1< BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* >::Invoke(7 /* System.Void UniGLTF.Zip.CommonHeader::ReadBefore(System.IO.BinaryReader) */, __this, L_24);
					// Read(r);
					BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_25 = V_4;
					CommonHeader_Read_m47229F24CB9C58CD9B5F568F388D0D97E8E179A6(__this, L_25, NULL);
					// ReadAfter(r);
					BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_26 = V_4;
					VirtualActionInvoker1< BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* >::Invoke(8 /* System.Void UniGLTF.Zip.CommonHeader::ReadAfter(System.IO.BinaryReader) */, __this, L_26);
					goto IL_0097_1;
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_0097_1:
			{
				goto IL_00a4;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00a4:
	{
		// }
		return;
	}
}
// System.String UniGLTF.Zip.CommonHeader::get_FileName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CommonHeader_get_FileName_m1B58286382761DBAFD9F5911351D311D09DEF7B3 (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		// return Encoding.GetString(Bytes,
		//     Offset + FixedFieldLength,
		//     FileNameLength);
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_0 = __this->___Encoding_0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = __this->___Bytes_1;
		int32_t L_2 = __this->___Offset_2;
		int32_t L_3;
		L_3 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UniGLTF.Zip.CommonHeader::get_FixedFieldLength() */, __this);
		uint16_t L_4 = __this->___FileNameLength_11;
		NullCheck(L_0);
		String_t* L_5;
		L_5 = VirtualFuncInvoker3< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(34 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, ((int32_t)il2cpp_codegen_add(L_2, L_3)), L_4);
		V_0 = L_5;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		String_t* L_6 = V_0;
		return L_6;
	}
}
// System.ArraySegment`1<System.Byte> UniGLTF.Zip.CommonHeader::get_ExtraField()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 CommonHeader_get_ExtraField_m912086FD5219E309CD169C73C083BE3B5B2EF5D0 (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new ArraySegment<byte>(Bytes,
		//     Offset + FixedFieldLength + FileNameLength,
		//     ExtraFieldLength);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = __this->___Bytes_1;
		int32_t L_1 = __this->___Offset_2;
		int32_t L_2;
		L_2 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UniGLTF.Zip.CommonHeader::get_FixedFieldLength() */, __this);
		uint16_t L_3 = __this->___FileNameLength_11;
		uint16_t L_4 = __this->___ExtraFieldLength_12;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_5;
		memset((&L_5), 0, sizeof(L_5));
		ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20((&L_5), L_0, ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(L_1, L_2)), (int32_t)L_3)), L_4, /*hidden argument*/ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var);
		V_0 = L_5;
		goto IL_0029;
	}

IL_0029:
	{
		// }
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_6 = V_0;
		return L_6;
	}
}
// System.String UniGLTF.Zip.CommonHeader::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CommonHeader_ToString_m92F2685132713F947251F94811819F30A2FEF5E1 (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CompressionMethod_tBFF2C1E75DB65EAC2D266E0D2892BE12C84728B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6512EC05424BE00D0370103E600EE9C9C81C4D9E);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return string.Format("<file {0}({1}/{2} {3})>",
		//     FileName,
		//     CompressedSize,
		//     UncompressedSize,
		//     CompressionMethod
		//     );
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_0 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = L_0;
		String_t* L_2;
		L_2 = CommonHeader_get_FileName_m1B58286382761DBAFD9F5911351D311D09DEF7B3(__this, NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_2);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = L_1;
		int32_t L_4 = __this->___CompressedSize_9;
		int32_t L_5 = L_4;
		RuntimeObject* L_6 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_6);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_7 = L_3;
		int32_t L_8 = __this->___UncompressedSize_10;
		int32_t L_9 = L_8;
		RuntimeObject* L_10 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_10);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_11 = L_7;
		uint16_t L_12 = __this->___CompressionMethod_5;
		uint16_t L_13 = L_12;
		RuntimeObject* L_14 = Box(CompressionMethod_tBFF2C1E75DB65EAC2D266E0D2892BE12C84728B9_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_14);
		String_t* L_15;
		L_15 = String_Format_m918500C1EFB475181349A79989BB79BB36102894(_stringLiteral6512EC05424BE00D0370103E600EE9C9C81C4D9E, L_11, NULL);
		V_0 = L_15;
		goto IL_0047;
	}

IL_0047:
	{
		// }
		String_t* L_16 = V_0;
		return L_16;
	}
}
// System.Void UniGLTF.Zip.CommonHeader::Read(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommonHeader_Read_m47229F24CB9C58CD9B5F568F388D0D97E8E179A6 (CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_r, const RuntimeMethod* method) 
{
	{
		// VersionNeededToExtract = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_0 = ___0_r;
		NullCheck(L_0);
		uint16_t L_1;
		L_1 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_0);
		__this->___VersionNeededToExtract_3 = L_1;
		// GeneralPurposeBitFlag = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_2 = ___0_r;
		NullCheck(L_2);
		uint16_t L_3;
		L_3 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_2);
		__this->___GeneralPurposeBitFlag_4 = L_3;
		// CompressionMethod = (CompressionMethod)r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = ___0_r;
		NullCheck(L_4);
		uint16_t L_5;
		L_5 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_4);
		__this->___CompressionMethod_5 = L_5;
		// FileLastModificationTime = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_6 = ___0_r;
		NullCheck(L_6);
		uint16_t L_7;
		L_7 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_6);
		__this->___FileLastModificationTime_6 = L_7;
		// FileLastModificationDate = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_8 = ___0_r;
		NullCheck(L_8);
		uint16_t L_9;
		L_9 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_8);
		__this->___FileLastModificationDate_7 = L_9;
		// CRC32 = r.ReadInt32();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_10 = ___0_r;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_10);
		__this->___CRC32_8 = L_11;
		// CompressedSize = r.ReadInt32();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_12 = ___0_r;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_12);
		__this->___CompressedSize_9 = L_13;
		// UncompressedSize = r.ReadInt32();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_14 = ___0_r;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_14);
		__this->___UncompressedSize_10 = L_15;
		// FileNameLength = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_16 = ___0_r;
		NullCheck(L_16);
		uint16_t L_17;
		L_17 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_16);
		__this->___FileNameLength_11 = L_17;
		// ExtraFieldLength = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_18 = ___0_r;
		NullCheck(L_18);
		uint16_t L_19;
		L_19 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_18);
		__this->___ExtraFieldLength_12 = L_19;
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UniGLTF.Zip.CentralDirectoryFileHeader::get_Signature()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CentralDirectoryFileHeader_get_Signature_m5898903709155839E68DE70A4E6C2DD1FFFECF14 (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// return 0x02014b50;
		V_0 = ((int32_t)33639248);
		goto IL_0009;
	}

IL_0009:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void UniGLTF.Zip.CentralDirectoryFileHeader::.ctor(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CentralDirectoryFileHeader__ctor_mF7669E89649B2656FC2648DB2096BE0438EDC245 (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, int32_t ___1_offset, const RuntimeMethod* method) 
{
	{
		// public CentralDirectoryFileHeader(Byte[] bytes, int offset) : base(bytes, offset) { }
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_bytes;
		int32_t L_1 = ___1_offset;
		CommonHeader__ctor_mC4C89B79E39052AC7FF022A55C70AF64AC1F2BAD(__this, L_0, L_1, NULL);
		// public CentralDirectoryFileHeader(Byte[] bytes, int offset) : base(bytes, offset) { }
		return;
	}
}
// System.Int32 UniGLTF.Zip.CentralDirectoryFileHeader::get_FixedFieldLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CentralDirectoryFileHeader_get_FixedFieldLength_m926F697A8D7EFBAD499E8A5C529831B1EB12DA58 (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// return 46;
		V_0 = ((int32_t)46);
		goto IL_0006;
	}

IL_0006:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.String UniGLTF.Zip.CentralDirectoryFileHeader::get_FileComment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CentralDirectoryFileHeader_get_FileComment_mB5E1DAC9A63242E446B99704AB86472E42B21FAA (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		// return Encoding.GetString(Bytes,
		//     Offset + 46 + FileNameLength + ExtraFieldLength,
		//     FileCommentLength);
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_0 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___Encoding_0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___Bytes_1;
		int32_t L_2 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___Offset_2;
		uint16_t L_3 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___FileNameLength_11;
		uint16_t L_4 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___ExtraFieldLength_12;
		uint16_t L_5 = __this->___FileCommentLength_14;
		NullCheck(L_0);
		String_t* L_6;
		L_6 = VirtualFuncInvoker3< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(34 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(L_2, ((int32_t)46))), (int32_t)L_3)), (int32_t)L_4)), L_5);
		V_0 = L_6;
		goto IL_0032;
	}

IL_0032:
	{
		// }
		String_t* L_7 = V_0;
		return L_7;
	}
}
// System.Int32 UniGLTF.Zip.CentralDirectoryFileHeader::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CentralDirectoryFileHeader_get_Length_mCAD83AE7E449C2FF013A168532486DC2BA344E8A (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// return FixedFieldLength + FileNameLength + ExtraFieldLength + FileCommentLength;
		int32_t L_0;
		L_0 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UniGLTF.Zip.CommonHeader::get_FixedFieldLength() */, __this);
		uint16_t L_1 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___FileNameLength_11;
		uint16_t L_2 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___ExtraFieldLength_12;
		uint16_t L_3 = __this->___FileCommentLength_14;
		V_0 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(L_0, (int32_t)L_1)), (int32_t)L_2)), (int32_t)L_3));
		goto IL_001f;
	}

IL_001f:
	{
		// }
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void UniGLTF.Zip.CentralDirectoryFileHeader::ReadBefore(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CentralDirectoryFileHeader_ReadBefore_mBB482146300787A1DADB3CCB766401D38AA77FFD (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_r, const RuntimeMethod* method) 
{
	{
		// VersionMadeBy = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_0 = ___0_r;
		NullCheck(L_0);
		uint16_t L_1;
		L_1 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_0);
		__this->___VersionMadeBy_13 = L_1;
		// }
		return;
	}
}
// System.Void UniGLTF.Zip.CentralDirectoryFileHeader::ReadAfter(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CentralDirectoryFileHeader_ReadAfter_m90663820AE0068914028D6EDAF5BEA4E66DB0796 (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_r, const RuntimeMethod* method) 
{
	{
		// FileCommentLength = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_0 = ___0_r;
		NullCheck(L_0);
		uint16_t L_1;
		L_1 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_0);
		__this->___FileCommentLength_14 = L_1;
		// DiskNumberWhereFileStarts = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_2 = ___0_r;
		NullCheck(L_2);
		uint16_t L_3;
		L_3 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_2);
		__this->___DiskNumberWhereFileStarts_15 = L_3;
		// InternalFileAttributes = r.ReadUInt16();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = ___0_r;
		NullCheck(L_4);
		uint16_t L_5;
		L_5 = VirtualFuncInvoker0< uint16_t >::Invoke(14 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_4);
		__this->___InternalFileAttributes_16 = L_5;
		// ExternalFileAttributes = r.ReadInt32();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_6 = ___0_r;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_6);
		__this->___ExternalFileAttributes_17 = L_7;
		// RelativeOffsetOfLocalFileHeader = r.ReadInt32();
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_8 = ___0_r;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_8);
		__this->___RelativeOffsetOfLocalFileHeader_18 = L_9;
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UniGLTF.Zip.LocalFileHeader::get_FixedFieldLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LocalFileHeader_get_FixedFieldLength_mD6A4247EC217DDDE1DCEBB6D59543942615D9698 (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// return 30;
		V_0 = ((int32_t)30);
		goto IL_0006;
	}

IL_0006:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UniGLTF.Zip.LocalFileHeader::get_Signature()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LocalFileHeader_get_Signature_mD586AE9E44F1B4469EA3FA4B5C1DB6B72CB6EDBA (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// return 0x04034b50;
		V_0 = ((int32_t)67324752);
		goto IL_0009;
	}

IL_0009:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UniGLTF.Zip.LocalFileHeader::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LocalFileHeader_get_Length_mFE0A89344DDD1F8DBA579DFFE3190395D07DDD9C (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// return FixedFieldLength + FileNameLength + ExtraFieldLength;
		int32_t L_0;
		L_0 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UniGLTF.Zip.CommonHeader::get_FixedFieldLength() */, __this);
		uint16_t L_1 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___FileNameLength_11;
		uint16_t L_2 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)__this)->___ExtraFieldLength_12;
		V_0 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(L_0, (int32_t)L_1)), (int32_t)L_2));
		goto IL_0018;
	}

IL_0018:
	{
		// }
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UniGLTF.Zip.LocalFileHeader::.ctor(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalFileHeader__ctor_m638E30171367A591F726F0B3FCB7A9C3F597E6EE (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, int32_t ___1_offset, const RuntimeMethod* method) 
{
	{
		// public LocalFileHeader(Byte[] bytes, int offset) : base(bytes, offset)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_bytes;
		int32_t L_1 = ___1_offset;
		CommonHeader__ctor_mC4C89B79E39052AC7FF022A55C70AF64AC1F2BAD(__this, L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void UniGLTF.Zip.LocalFileHeader::ReadBefore(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalFileHeader_ReadBefore_m7756C1C6B72CB8D89843AC66DB2E8C4362753CC8 (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_r, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void UniGLTF.Zip.LocalFileHeader::ReadAfter(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocalFileHeader_ReadAfter_mF947BE5C3533E58328FAE71C0DD5C17E43C23A49 (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_r, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UniGLTF.Zip.ZipArchiveStorage::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ZipArchiveStorage_ToString_mBAD6904415A19C6ACB10E9688E101B3E899E2B59 (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_TisString_t_mC7273138476D2A6E6C69BD0A6CA9F902E90B4619_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisString_t_m3B23EE2DD15B2996E7D2ECA6E74696DA892AA194_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CToStringU3Eb__0_0_m5D303FFA4C09E204453E7DCBB16C7762C9358746_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6ACFE7690361B95BC457B7B5F163EC8F83FC4D37);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* G_B2_0 = NULL;
	List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* G_B1_0 = NULL;
	List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		// return string.Format("<ZIPArchive\n{0}>", String.Join("", Entries.Select(x => x.ToString() + "\n").ToArray()));
		List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* L_0 = __this->___Entries_0;
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var);
		Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* L_1 = ((U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var))->___U3CU3E9__0_0_1;
		Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		G_B1_2 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B1_3 = _stringLiteral6ACFE7690361B95BC457B7B5F163EC8F83FC4D37;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			G_B2_2 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
			G_B2_3 = _stringLiteral6ACFE7690361B95BC457B7B5F163EC8F83FC4D37;
			goto IL_0030;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var);
		U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84* L_3 = ((U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* L_4 = (Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8*)il2cpp_codegen_object_new(Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_2__ctor_m1A10A14901A3DCFAC3D85DE886E7D0817A95C26C(L_4, L_3, (intptr_t)((void*)U3CU3Ec_U3CToStringU3Eb__0_0_m5D303FFA4C09E204453E7DCBB16C7762C9358746_RuntimeMethod_var), NULL);
		Func_2_t44B3D462DDC8ED1C56511253ED4DB7535263B7E8* L_5 = L_4;
		((U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var))->___U3CU3E9__0_0_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var))->___U3CU3E9__0_0_1), (void*)L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0030:
	{
		RuntimeObject* L_6;
		L_6 = Enumerable_Select_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_TisString_t_mC7273138476D2A6E6C69BD0A6CA9F902E90B4619(G_B2_1, G_B2_0, Enumerable_Select_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_TisString_t_mC7273138476D2A6E6C69BD0A6CA9F902E90B4619_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7;
		L_7 = Enumerable_ToArray_TisString_t_m3B23EE2DD15B2996E7D2ECA6E74696DA892AA194(L_6, Enumerable_ToArray_TisString_t_m3B23EE2DD15B2996E7D2ECA6E74696DA892AA194_RuntimeMethod_var);
		String_t* L_8;
		L_8 = String_Join_m557B6B554B87C1742FA0B128500073B421ED0BFD(G_B2_2, L_7, NULL);
		String_t* L_9;
		L_9 = String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8(G_B2_3, L_8, NULL);
		V_0 = L_9;
		goto IL_0047;
	}

IL_0047:
	{
		// }
		String_t* L_10 = V_0;
		return L_10;
	}
}
// UniGLTF.Zip.ZipArchiveStorage UniGLTF.Zip.ZipArchiveStorage::Parse(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* ZipArchiveStorage_Parse_m0D24C0153C218D4D3F2A740CCA7E1A2FB28CF3A9 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_bytes, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mC00BE642E7A70274A2C74253C0E44C69AE0A9F29_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* V_0 = NULL;
	ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* V_4 = NULL;
	bool V_5 = false;
	ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* V_6 = NULL;
	{
		// var eocd = EOCD.Parse(bytes);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_bytes;
		EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_1;
		L_1 = EOCD_Parse_mF448F8F575BD23EE19FAC2660F16B4528C7D6B05(L_0, NULL);
		V_0 = L_1;
		// var archive = new ZipArchiveStorage();
		ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* L_2 = (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820*)il2cpp_codegen_object_new(ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		ZipArchiveStorage__ctor_mF847A1CDD38254B27EDC947A3AE5FDFC398D43B5(L_2, NULL);
		V_1 = L_2;
		// var pos = eocd.OffsetOfStartOfCentralDirectory;
		EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->___OffsetOfStartOfCentralDirectory_5;
		V_2 = L_4;
		// for (int i = 0; i < eocd.NumberOfCentralDirectoryRecordsOnThisDisk; ++i)
		V_3 = 0;
		goto IL_0040;
	}

IL_0019:
	{
		// var file = new CentralDirectoryFileHeader(bytes, pos);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5 = ___0_bytes;
		int32_t L_6 = V_2;
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_7 = (CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C*)il2cpp_codegen_object_new(CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		CentralDirectoryFileHeader__ctor_mF7669E89649B2656FC2648DB2096BE0438EDC245(L_7, L_5, L_6, NULL);
		V_4 = L_7;
		// archive.Entries.Add(file);
		ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* L_8 = V_1;
		NullCheck(L_8);
		List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* L_9 = L_8->___Entries_0;
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_10 = V_4;
		NullCheck(L_9);
		List_1_Add_mC00BE642E7A70274A2C74253C0E44C69AE0A9F29_inline(L_9, L_10, List_1_Add_mC00BE642E7A70274A2C74253C0E44C69AE0A9F29_RuntimeMethod_var);
		// pos += file.Length;
		int32_t L_11 = V_2;
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_12 = V_4;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = VirtualFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UniGLTF.Zip.CommonHeader::get_Length() */, L_12);
		V_2 = ((int32_t)il2cpp_codegen_add(L_11, L_13));
		// for (int i = 0; i < eocd.NumberOfCentralDirectoryRecordsOnThisDisk; ++i)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_0040:
	{
		// for (int i = 0; i < eocd.NumberOfCentralDirectoryRecordsOnThisDisk; ++i)
		int32_t L_15 = V_3;
		EOCD_t0E317F864ECB48DF47586E8378755EECD4D52380* L_16 = V_0;
		NullCheck(L_16);
		uint16_t L_17 = L_16->___NumberOfCentralDirectoryRecordsOnThisDisk_2;
		V_5 = (bool)((((int32_t)L_15) < ((int32_t)L_17))? 1 : 0);
		bool L_18 = V_5;
		if (L_18)
		{
			goto IL_0019;
		}
	}
	{
		// return archive;
		ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* L_19 = V_1;
		V_6 = L_19;
		goto IL_0054;
	}

IL_0054:
	{
		// }
		ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* L_20 = V_6;
		return L_20;
	}
}
// System.Byte[] UniGLTF.Zip.ZipArchiveStorage::Extract(UniGLTF.Zip.CentralDirectoryFileHeader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ZipArchiveStorage_Extract_m3AAD8FDCA8FAD438FCB82B48290D9CE8031577E6 (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* ___0_header, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_2 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* V_3 = NULL;
	DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	bool V_8 = false;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_9 = NULL;
	{
		// var local = new LocalFileHeader(header.Bytes, header.RelativeOffsetOfLocalFileHeader);
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_0 = ___0_header;
		NullCheck(L_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_0)->___Bytes_1;
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_2 = ___0_header;
		NullCheck(L_2);
		int32_t L_3 = L_2->___RelativeOffsetOfLocalFileHeader_18;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_4 = (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6*)il2cpp_codegen_object_new(LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		LocalFileHeader__ctor_m638E30171367A591F726F0B3FCB7A9C3F597E6EE(L_4, L_1, L_3, NULL);
		V_0 = L_4;
		// var pos = local.Offset + local.Length;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_5)->___Offset_2;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = VirtualFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UniGLTF.Zip.CommonHeader::get_Length() */, L_7);
		V_1 = ((int32_t)il2cpp_codegen_add(L_6, L_8));
		// var dst = new Byte[local.UncompressedSize];
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_9)->___UncompressedSize_10;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)L_10);
		V_2 = L_11;
		// using (var s = new MemoryStream(header.Bytes, pos, local.CompressedSize, false))
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_12 = ___0_header;
		NullCheck(L_12);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_13 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_12)->___Bytes_1;
		int32_t L_14 = V_1;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_15)->___CompressedSize_9;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_17 = (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)il2cpp_codegen_object_new(MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		NullCheck(L_17);
		MemoryStream__ctor_mC2A08AF3FC30A1DF60B2CFC5668637DF88B66444(L_17, L_13, L_14, L_16, (bool)0, NULL);
		V_3 = L_17;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0090:
			{// begin finally (depth: 1)
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_18 = V_3;
					if (!L_18)
					{
						goto IL_009a;
					}
				}
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_19 = V_3;
					NullCheck(L_19);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_19);
				}

IL_009a:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				// using (var deflateStream = new DeflateStream(s, CompressionMode.Decompress))
				MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_20 = V_3;
				DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_21 = (DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104*)il2cpp_codegen_object_new(DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104_il2cpp_TypeInfo_var);
				NullCheck(L_21);
				DeflateStream__ctor_m344C1EF8B83E612C4FC662F0152DF1D2A5636829(L_21, L_20, 0, NULL);
				V_4 = L_21;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_0081_1:
					{// begin finally (depth: 2)
						{
							DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_22 = V_4;
							if (!L_22)
							{
								goto IL_008d_1;
							}
						}
						{
							DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_23 = V_4;
							NullCheck(L_23);
							InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_23);
						}

IL_008d_1:
						{
							return;
						}
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						// int dst_pos = 0;
						V_5 = 0;
						// for (int remain = dst.Length; remain > 0;)
						ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_24 = V_2;
						NullCheck(L_24);
						V_6 = ((int32_t)(((RuntimeArray*)L_24)->max_length));
						goto IL_0073_2;
					}

IL_0055_2:
					{
						// var readSize = deflateStream.Read(dst, dst_pos, remain);
						DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_25 = V_4;
						ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_26 = V_2;
						int32_t L_27 = V_5;
						int32_t L_28 = V_6;
						NullCheck(L_25);
						int32_t L_29;
						L_29 = VirtualFuncInvoker3< int32_t, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(22 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_25, L_26, L_27, L_28);
						V_7 = L_29;
						// dst_pos += readSize;
						int32_t L_30 = V_5;
						int32_t L_31 = V_7;
						V_5 = ((int32_t)il2cpp_codegen_add(L_30, L_31));
						// remain -= readSize;
						int32_t L_32 = V_6;
						int32_t L_33 = V_7;
						V_6 = ((int32_t)il2cpp_codegen_subtract(L_32, L_33));
					}

IL_0073_2:
					{
						// for (int remain = dst.Length; remain > 0;)
						int32_t L_34 = V_6;
						V_8 = (bool)((((int32_t)L_34) > ((int32_t)0))? 1 : 0);
						bool L_35 = V_8;
						if (L_35)
						{
							goto IL_0055_2;
						}
					}
					{
						goto IL_008e_1;
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_008e_1:
			{
				goto IL_009b;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_009b:
	{
		// return dst;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_36 = V_2;
		V_9 = L_36;
		goto IL_00a0;
	}

IL_00a0:
	{
		// }
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_37 = V_9;
		return L_37;
	}
}
// System.String UniGLTF.Zip.ZipArchiveStorage::ExtractToString(UniGLTF.Zip.CentralDirectoryFileHeader,System.Text.Encoding)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ZipArchiveStorage_ExtractToString_m0056A06E44D8442FD7794DDDE0C26146409BA5E1 (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* ___0_header, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* V_0 = NULL;
	int32_t V_1 = 0;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* V_2 = NULL;
	DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* V_3 = NULL;
	StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* V_4 = NULL;
	String_t* V_5 = NULL;
	{
		// var local = new LocalFileHeader(header.Bytes, header.RelativeOffsetOfLocalFileHeader);
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_0 = ___0_header;
		NullCheck(L_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_0)->___Bytes_1;
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_2 = ___0_header;
		NullCheck(L_2);
		int32_t L_3 = L_2->___RelativeOffsetOfLocalFileHeader_18;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_4 = (LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6*)il2cpp_codegen_object_new(LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		LocalFileHeader__ctor_m638E30171367A591F726F0B3FCB7A9C3F597E6EE(L_4, L_1, L_3, NULL);
		V_0 = L_4;
		// var pos = local.Offset + local.Length;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_5)->___Offset_2;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = VirtualFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UniGLTF.Zip.CommonHeader::get_Length() */, L_7);
		V_1 = ((int32_t)il2cpp_codegen_add(L_6, L_8));
		// using (var s = new MemoryStream(header.Bytes, pos, local.CompressedSize, false))
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_9 = ___0_header;
		NullCheck(L_9);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_10 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_9)->___Bytes_1;
		int32_t L_11 = V_1;
		LocalFileHeader_t5681BFAB440FE6F01DE886D9C761474BA06FB3B6* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_12)->___CompressedSize_9;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_14 = (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)il2cpp_codegen_object_new(MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		NullCheck(L_14);
		MemoryStream__ctor_mC2A08AF3FC30A1DF60B2CFC5668637DF88B66444(L_14, L_10, L_11, L_13, (bool)0, NULL);
		V_2 = L_14;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_006a:
			{// begin finally (depth: 1)
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_15 = V_2;
					if (!L_15)
					{
						goto IL_0074;
					}
				}
				{
					MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_16 = V_2;
					NullCheck(L_16);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_16);
				}

IL_0074:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				// using (var deflateStream = new DeflateStream(s, CompressionMode.Decompress))
				MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_17 = V_2;
				DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_18 = (DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104*)il2cpp_codegen_object_new(DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104_il2cpp_TypeInfo_var);
				NullCheck(L_18);
				DeflateStream__ctor_m344C1EF8B83E612C4FC662F0152DF1D2A5636829(L_18, L_17, 0, NULL);
				V_3 = L_18;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_005f_1:
					{// begin finally (depth: 2)
						{
							DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_19 = V_3;
							if (!L_19)
							{
								goto IL_0069_1;
							}
						}
						{
							DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_20 = V_3;
							NullCheck(L_20);
							InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_20);
						}

IL_0069_1:
						{
							return;
						}
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						// using (var r = new StreamReader(deflateStream, encoding))
						DeflateStream_tF1758952E9DBAB2F9A15D42971F33A78AB4FC104* L_21 = V_3;
						Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_22 = ___1_encoding;
						StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* L_23 = (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B*)il2cpp_codegen_object_new(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
						NullCheck(L_23);
						StreamReader__ctor_m7712DDC735E99B6833E2666ADFD8A06CB96A58B1(L_23, L_21, L_22, NULL);
						V_4 = L_23;
					}
					{
						auto __finallyBlock = il2cpp::utils::Finally([&]
						{

FINALLY_0052_2:
							{// begin finally (depth: 3)
								{
									StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* L_24 = V_4;
									if (!L_24)
									{
										goto IL_005e_2;
									}
								}
								{
									StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* L_25 = V_4;
									NullCheck(L_25);
									InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_25);
								}

IL_005e_2:
								{
									return;
								}
							}// end finally (depth: 3)
						});
						try
						{// begin try (depth: 3)
							// return r.ReadToEnd();
							StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* L_26 = V_4;
							NullCheck(L_26);
							String_t* L_27;
							L_27 = VirtualFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.TextReader::ReadToEnd() */, L_26);
							V_5 = L_27;
							goto IL_0075;
						}// end try (depth: 3)
						catch(Il2CppExceptionWrapper& e)
						{
							__finallyBlock.StoreException(e.ex);
						}
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0075:
	{
		// }
		String_t* L_28 = V_5;
		return L_28;
	}
}
// System.ArraySegment`1<System.Byte> UniGLTF.Zip.ZipArchiveStorage::Get(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ZipArchiveStorage_Get_m7EAD69633A8FD293B8E78A4C2BCCC5BCEE38FC4A (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, String_t* ___0_url, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_m8A879E5F534A391C62D3D65CDF9B8F0A7E1AED86_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_FirstOrDefault_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_m13A276AEA97B6522EBC40FFC60147FB43E1CD02E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass5_0_U3CGetU3Eb__0_m1B0B45338FC9A07977990BEC148DB1AEC6E8189D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* V_0 = NULL;
	CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* V_1 = NULL;
	bool V_2 = false;
	uint16_t V_3 = 0;
	uint16_t V_4 = 0;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* L_0 = (U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass5_0__ctor_m7ED750F1E4A531C81F17A2DC644B8DE77BDCE1B0(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* L_1 = V_0;
		String_t* L_2 = ___0_url;
		NullCheck(L_1);
		L_1->___url_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___url_0), (void*)L_2);
		// var found = Entries.FirstOrDefault(x => x.FileName == url);
		List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* L_3 = __this->___Entries_0;
		U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* L_4 = V_0;
		Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A* L_5 = (Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A*)il2cpp_codegen_object_new(Func_2_t15E9A60C1F7394F1D116C23A998BB6510257296A_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		Func_2__ctor_mDA06EA659644633B6732E314C1A2934B265C1B88(L_5, L_4, (intptr_t)((void*)U3CU3Ec__DisplayClass5_0_U3CGetU3Eb__0_m1B0B45338FC9A07977990BEC148DB1AEC6E8189D_RuntimeMethod_var), NULL);
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_6;
		L_6 = Enumerable_FirstOrDefault_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_m13A276AEA97B6522EBC40FFC60147FB43E1CD02E(L_3, L_5, Enumerable_FirstOrDefault_TisCentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C_m13A276AEA97B6522EBC40FFC60147FB43E1CD02E_RuntimeMethod_var);
		V_1 = L_6;
		// if (found == null)
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_7 = V_1;
		V_2 = (bool)((((RuntimeObject*)(CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C*)L_7) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		// throw new FileNotFoundException("[ZipArchive]" + url);
		U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = L_9->___url_0;
		String_t* L_11;
		L_11 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral02251F8FB149E78081497DCBE92A7B79B41B4D41)), L_10, NULL);
		FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A* L_12 = (FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FileNotFoundException_t17F1B49AD996E4A60C87C7ADC9D3A25EB5808A9A_il2cpp_TypeInfo_var)));
		NullCheck(L_12);
		FileNotFoundException__ctor_mA8C9C93DB8C5B96D6B5E59B2AE07154F265FB1A1(L_12, L_11, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ZipArchiveStorage_Get_m7EAD69633A8FD293B8E78A4C2BCCC5BCEE38FC4A_RuntimeMethod_var)));
	}

IL_0045:
	{
		// switch (found.CompressionMethod)
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_13 = V_1;
		NullCheck(L_13);
		uint16_t L_14 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_13)->___CompressionMethod_5;
		V_4 = L_14;
		uint16_t L_15 = V_4;
		V_3 = L_15;
		uint16_t L_16 = V_3;
		if (!L_16)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_0055:
	{
		uint16_t L_17 = V_3;
		if ((((int32_t)L_17) == ((int32_t)8)))
		{
			goto IL_005b;
		}
	}
	{
		goto IL_0086;
	}

IL_005b:
	{
		// return new ArraySegment<byte>(Extract(found));
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_18 = V_1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_19;
		L_19 = ZipArchiveStorage_Extract_m3AAD8FDCA8FAD438FCB82B48290D9CE8031577E6(__this, L_18, NULL);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_20;
		memset((&L_20), 0, sizeof(L_20));
		ArraySegment_1__ctor_m8A879E5F534A391C62D3D65CDF9B8F0A7E1AED86((&L_20), L_19, /*hidden argument*/ArraySegment_1__ctor_m8A879E5F534A391C62D3D65CDF9B8F0A7E1AED86_RuntimeMethod_var);
		V_5 = L_20;
		goto IL_009d;
	}

IL_006b:
	{
		// return new ArraySegment<byte>(found.Bytes, found.RelativeOffsetOfLocalFileHeader, found.CompressedSize);
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_21 = V_1;
		NullCheck(L_21);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_22 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_21)->___Bytes_1;
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = L_23->___RelativeOffsetOfLocalFileHeader_18;
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_25 = V_1;
		NullCheck(L_25);
		int32_t L_26 = ((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_25)->___CompressedSize_9;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_27;
		memset((&L_27), 0, sizeof(L_27));
		ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20((&L_27), L_22, L_24, L_26, /*hidden argument*/ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var);
		V_5 = L_27;
		goto IL_009d;
	}

IL_0086:
	{
		// throw new NotImplementedException(found.CompressionMethod.ToString());
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_28 = V_1;
		NullCheck(L_28);
		uint16_t* L_29 = (&((CommonHeader_t0F077CE9938E6817F7FAEE515E3CDC0FA6078D37*)L_28)->___CompressionMethod_5);
		Il2CppFakeBox<uint16_t> L_30(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CompressionMethod_tBFF2C1E75DB65EAC2D266E0D2892BE12C84728B9_il2cpp_TypeInfo_var)), L_29);
		String_t* L_31;
		L_31 = Enum_ToString_m946B0B83C4470457D0FF555D862022C72BB55741((Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2*)(&L_30), NULL);
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_32 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_32);
		NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742(L_32, L_31, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_32, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ZipArchiveStorage_Get_m7EAD69633A8FD293B8E78A4C2BCCC5BCEE38FC4A_RuntimeMethod_var)));
	}

IL_009d:
	{
		// }
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_33 = V_5;
		return L_33;
	}
}
// System.String UniGLTF.Zip.ZipArchiveStorage::GetPath(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ZipArchiveStorage_GetPath_m97904A324480AEE67B87B5926297BEDAE6A46F19 (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, String_t* ___0_url, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		// return null;
		V_0 = (String_t*)NULL;
		goto IL_0005;
	}

IL_0005:
	{
		// }
		String_t* L_0 = V_0;
		return L_0;
	}
}
// System.Void UniGLTF.Zip.ZipArchiveStorage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipArchiveStorage__ctor_mF847A1CDD38254B27EDC947A3AE5FDFC398D43B5 (ZipArchiveStorage_t004312AE1004F7D63DC922ECB813A8C07934E820* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m6BCCFB8857EC3B1BF31B34F9E7CD0B46C8AC0A3C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<CentralDirectoryFileHeader> Entries = new List<CentralDirectoryFileHeader>();
		List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1* L_0 = (List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1*)il2cpp_codegen_object_new(List_1_t28C6C819FB16EF6FA1EDA3D9332634DF4216DAE1_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_m6BCCFB8857EC3B1BF31B34F9E7CD0B46C8AC0A3C(L_0, List_1__ctor_m6BCCFB8857EC3B1BF31B34F9E7CD0B46C8AC0A3C_RuntimeMethod_var);
		__this->___Entries_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Entries_0), (void*)L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m3841DF242A291E961FF0803C5ED8DFF1A1A3DD61 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84* L_0 = (U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84*)il2cpp_codegen_object_new(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__ctor_m6D6C41B3498BF95557C5BDFD94BA73461CA74133(L_0, NULL);
		((U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6D6C41B3498BF95557C5BDFD94BA73461CA74133 (U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.String UniGLTF.Zip.ZipArchiveStorage/<>c::<ToString>b__0_0(UniGLTF.Zip.CentralDirectoryFileHeader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3CToStringU3Eb__0_0_m5D303FFA4C09E204453E7DCBB16C7762C9358746 (U3CU3Ec_tC180EB410EC7150613555B972A05831681D48B84* __this, CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* ___0_x, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("<ZIPArchive\n{0}>", String.Join("", Entries.Select(x => x.ToString() + "\n").ToArray()));
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_0 = ___0_x;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		String_t* L_2;
		L_2 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_1, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD, NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass5_0__ctor_m7ED750F1E4A531C81F17A2DC644B8DE77BDCE1B0 (U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Boolean UniGLTF.Zip.ZipArchiveStorage/<>c__DisplayClass5_0::<Get>b__0(UniGLTF.Zip.CentralDirectoryFileHeader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass5_0_U3CGetU3Eb__0_m1B0B45338FC9A07977990BEC148DB1AEC6E8189D (U3CU3Ec__DisplayClass5_0_t7CCEA93A22341A2E02A2833F4E3815D2C90EBEE3* __this, CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* ___0_x, const RuntimeMethod* method) 
{
	{
		// var found = Entries.FirstOrDefault(x => x.FileName == url);
		CentralDirectoryFileHeader_tBB88937CDB5641D59FD5FEDECE80EFEA44098B5C* L_0 = ___0_x;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = CommonHeader_get_FileName_m1B58286382761DBAFD9F5911351D311D09DEF7B3(L_0, NULL);
		String_t* L_2 = __this->___url_0;
		bool L_3;
		L_3 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_1, L_2, NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.ShaderPropExporter.PreExportShadersAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreExportShadersAttribute__ctor_m4A56EBA7037D7D841BA2D8A90CB3BE86128C87A2 (PreExportShadersAttribute_tFAC060D43264C6113FE055289B240F2FFC03B885* __this, const RuntimeMethod* method) 
{
	{
		Attribute__ctor_m79ED1BF1EE36D1E417BA89A0D9F91F8AAD8D19E2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.ShaderPropExporter.PreExportShaderAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreExportShaderAttribute__ctor_m4CEA7CAAEB6A4C4A4C3C40BD014FF055FB89C50E (PreExportShaderAttribute_t8D9809E8CFB4A4557430A5755A66061EB87269F5* __this, const RuntimeMethod* method) 
{
	{
		Attribute__ctor_m79ED1BF1EE36D1E417BA89A0D9F91F8AAD8D19E2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.ShaderPropExporter.SupportedShader
IL2CPP_EXTERN_C void SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshal_pinvoke(const SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD& unmarshaled, SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_pinvoke& marshaled)
{
	marshaled.___TargetFolder_0 = il2cpp_codegen_marshal_string(unmarshaled.___TargetFolder_0);
	marshaled.___ShaderName_1 = il2cpp_codegen_marshal_string(unmarshaled.___ShaderName_1);
}
IL2CPP_EXTERN_C void SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshal_pinvoke_back(const SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_pinvoke& marshaled, SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD& unmarshaled)
{
	unmarshaled.___TargetFolder_0 = il2cpp_codegen_marshal_string_result(marshaled.___TargetFolder_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___TargetFolder_0), (void*)il2cpp_codegen_marshal_string_result(marshaled.___TargetFolder_0));
	unmarshaled.___ShaderName_1 = il2cpp_codegen_marshal_string_result(marshaled.___ShaderName_1);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___ShaderName_1), (void*)il2cpp_codegen_marshal_string_result(marshaled.___ShaderName_1));
}
// Conversion method for clean up from marshalling of: UniGLTF.ShaderPropExporter.SupportedShader
IL2CPP_EXTERN_C void SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshal_pinvoke_cleanup(SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___TargetFolder_0);
	marshaled.___TargetFolder_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___ShaderName_1);
	marshaled.___ShaderName_1 = NULL;
}
// Conversion methods for marshalling of: UniGLTF.ShaderPropExporter.SupportedShader
IL2CPP_EXTERN_C void SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshal_com(const SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD& unmarshaled, SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_com& marshaled)
{
	marshaled.___TargetFolder_0 = il2cpp_codegen_marshal_bstring(unmarshaled.___TargetFolder_0);
	marshaled.___ShaderName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.___ShaderName_1);
}
IL2CPP_EXTERN_C void SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshal_com_back(const SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_com& marshaled, SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD& unmarshaled)
{
	unmarshaled.___TargetFolder_0 = il2cpp_codegen_marshal_bstring_result(marshaled.___TargetFolder_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___TargetFolder_0), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___TargetFolder_0));
	unmarshaled.___ShaderName_1 = il2cpp_codegen_marshal_bstring_result(marshaled.___ShaderName_1);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___ShaderName_1), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___ShaderName_1));
}
// Conversion method for clean up from marshalling of: UniGLTF.ShaderPropExporter.SupportedShader
IL2CPP_EXTERN_C void SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshal_com_cleanup(SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___TargetFolder_0);
	marshaled.___TargetFolder_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___ShaderName_1);
	marshaled.___ShaderName_1 = NULL;
}
// System.Void UniGLTF.ShaderPropExporter.SupportedShader::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46 (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD* __this, String_t* ___0_targetFolder, String_t* ___1_shaderName, const RuntimeMethod* method) 
{
	{
		// TargetFolder = targetFolder;
		String_t* L_0 = ___0_targetFolder;
		__this->___TargetFolder_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___TargetFolder_0), (void*)L_0);
		// ShaderName = shaderName;
		String_t* L_1 = ___1_shaderName;
		__this->___ShaderName_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___ShaderName_1), (void*)L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46_AdjustorThunk (RuntimeObject* __this, String_t* ___0_targetFolder, String_t* ___1_shaderName, const RuntimeMethod* method)
{
	SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD*>(__this + _offset);
	SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46(_thisAdjusted, ___0_targetFolder, ___1_shaderName, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UniGLTF.ShaderPropExporter.ShaderProps UniGLTF.ShaderPropExporter.PreShaderPropExporter::GetPropsForSupportedShader(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* PreShaderPropExporter_GetPropsForSupportedShader_mC12F64EFB6D6827E64D951E7071D7C196D2AB2CD (String_t* ___0_shaderName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m48A495425B604D5E9111229379AA45DF27806631_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m4C31AFC006F2A82A3464C2C5F620AEE8809B47A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mF1E5D7383BFC26A82A10D42650A4EE167D9B091B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Any_TisRuntimeObject_m67CFBD544CF1D1C0C7E7457FDBDB81649DE26847_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_get_Key_m3E0AEB8F3C3953ABFEC50FE77BF3B32552C80D22_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_get_Value_m0E88FF128ADD89043FB8F078C432663C7BCE4648_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PreExportShaderAttribute_t8D9809E8CFB4A4557430A5755A66061EB87269F5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_0 = NULL;
	bool V_1 = false;
	PropertyInfoU5BU5D_tD81C248B41D0C76207C42DB9C332DC79F490B1D7* V_2 = NULL;
	int32_t V_3 = 0;
	PropertyInfo_t* V_4 = NULL;
	bool V_5 = false;
	KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 V_6;
	memset((&V_6), 0, sizeof(V_6));
	bool V_7 = false;
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_8 = NULL;
	{
		// if (m_shaderPropMap == null)
		il2cpp_codegen_runtime_class_init_inline(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var);
		Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* L_0 = ((PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields*)il2cpp_codegen_static_fields_for(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var))->___m_shaderPropMap_2;
		V_1 = (bool)((((RuntimeObject*)(Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D*)L_0) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0089;
		}
	}
	{
		// m_shaderPropMap = new Dictionary<string, ShaderProps>();
		Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* L_2 = (Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D*)il2cpp_codegen_object_new(Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Dictionary_2__ctor_mF1E5D7383BFC26A82A10D42650A4EE167D9B091B(L_2, Dictionary_2__ctor_mF1E5D7383BFC26A82A10D42650A4EE167D9B091B_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var);
		((PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields*)il2cpp_codegen_static_fields_for(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var))->___m_shaderPropMap_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields*)il2cpp_codegen_static_fields_for(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var))->___m_shaderPropMap_2), (void*)L_2);
		// foreach (var prop in typeof(PreShaderPropExporter).GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_3 = { reinterpret_cast<intptr_t> (PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_4;
		L_4 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_3, NULL);
		NullCheck(L_4);
		PropertyInfoU5BU5D_tD81C248B41D0C76207C42DB9C332DC79F490B1D7* L_5;
		L_5 = VirtualFuncInvoker1< PropertyInfoU5BU5D_tD81C248B41D0C76207C42DB9C332DC79F490B1D7*, int32_t >::Invoke(107 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_4, ((int32_t)56));
		V_2 = L_5;
		V_3 = 0;
		goto IL_0082;
	}

IL_002f:
	{
		// foreach (var prop in typeof(PreShaderPropExporter).GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
		PropertyInfoU5BU5D_tD81C248B41D0C76207C42DB9C332DC79F490B1D7* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		PropertyInfo_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_4 = L_9;
		// if (prop.GetCustomAttributes(typeof(PreExportShaderAttribute), true).Any())
		PropertyInfo_t* L_10 = V_4;
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_11 = { reinterpret_cast<intptr_t> (PreExportShaderAttribute_t8D9809E8CFB4A4557430A5755A66061EB87269F5_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_12;
		L_12 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_11, NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_13;
		L_13 = VirtualFuncInvoker2< ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*, Type_t*, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_12, (bool)1);
		bool L_14;
		L_14 = Enumerable_Any_TisRuntimeObject_m67CFBD544CF1D1C0C7E7457FDBDB81649DE26847((RuntimeObject*)L_13, Enumerable_Any_TisRuntimeObject_m67CFBD544CF1D1C0C7E7457FDBDB81649DE26847_RuntimeMethod_var);
		V_5 = L_14;
		bool L_15 = V_5;
		if (!L_15)
		{
			goto IL_007d;
		}
	}
	{
		// var kv = (KeyValuePair<string, ShaderProps>)prop.GetValue(null, null);
		PropertyInfo_t* L_16 = V_4;
		NullCheck(L_16);
		RuntimeObject* L_17;
		L_17 = VirtualFuncInvoker2< RuntimeObject*, RuntimeObject*, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_16, NULL, (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)NULL);
		V_6 = ((*(KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802*)((KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802*)(KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802*)UnBox(L_17, KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802_il2cpp_TypeInfo_var))));
		// m_shaderPropMap.Add(kv.Key, kv.Value);
		il2cpp_codegen_runtime_class_init_inline(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var);
		Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* L_18 = ((PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields*)il2cpp_codegen_static_fields_for(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var))->___m_shaderPropMap_2;
		String_t* L_19;
		L_19 = KeyValuePair_2_get_Key_m3E0AEB8F3C3953ABFEC50FE77BF3B32552C80D22_inline((&V_6), KeyValuePair_2_get_Key_m3E0AEB8F3C3953ABFEC50FE77BF3B32552C80D22_RuntimeMethod_var);
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_20;
		L_20 = KeyValuePair_2_get_Value_m0E88FF128ADD89043FB8F078C432663C7BCE4648_inline((&V_6), KeyValuePair_2_get_Value_m0E88FF128ADD89043FB8F078C432663C7BCE4648_RuntimeMethod_var);
		NullCheck(L_18);
		Dictionary_2_Add_m48A495425B604D5E9111229379AA45DF27806631(L_18, L_19, L_20, Dictionary_2_Add_m48A495425B604D5E9111229379AA45DF27806631_RuntimeMethod_var);
	}

IL_007d:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_21, 1));
	}

IL_0082:
	{
		// foreach (var prop in typeof(PreShaderPropExporter).GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
		int32_t L_22 = V_3;
		PropertyInfoU5BU5D_tD81C248B41D0C76207C42DB9C332DC79F490B1D7* L_23 = V_2;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length)))))
		{
			goto IL_002f;
		}
	}
	{
	}

IL_0089:
	{
		// if (m_shaderPropMap.TryGetValue(shaderName, out props))
		il2cpp_codegen_runtime_class_init_inline(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var);
		Dictionary_2_tCF7516CE51B39B9C8416CEB844633D6AC735739D* L_24 = ((PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields*)il2cpp_codegen_static_fields_for(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var))->___m_shaderPropMap_2;
		String_t* L_25 = ___0_shaderName;
		NullCheck(L_24);
		bool L_26;
		L_26 = Dictionary_2_TryGetValue_m4C31AFC006F2A82A3464C2C5F620AEE8809B47A8(L_24, L_25, (&V_0), Dictionary_2_TryGetValue_m4C31AFC006F2A82A3464C2C5F620AEE8809B47A8_RuntimeMethod_var);
		V_7 = L_26;
		bool L_27 = V_7;
		if (!L_27)
		{
			goto IL_00a2;
		}
	}
	{
		// return props;
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_28 = V_0;
		V_8 = L_28;
		goto IL_00a7;
	}

IL_00a2:
	{
		// return null;
		V_8 = (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*)NULL;
		goto IL_00a7;
	}

IL_00a7:
	{
		// }
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_29 = V_8;
		return L_29;
	}
}
// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps> UniGLTF.ShaderPropExporter.PreShaderPropExporter::get_Standard()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 PreShaderPropExporter_get_Standard_mB095865F25B64466D4084077D9C649C1DBC8CEAE (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2620038B8A71EF21A205CC921576171A3CA9B0F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral326129114EB43E5A03AD980A3709D55FE7934916);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral38B8C8C0BD25C5963BB0276F350E52AE4F3288D3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral40BFB095782D36D1B276A40A276C76911EF9318F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4FC108C6FF0A706B87BCEE07E7584DC45FD16B4B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51E77BE760A981E17113C4BA376BD85874B58A45);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral58D77E1ECF3579ADA2EDE01E1640D1E1CA4A37E8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60BECFD9EBE7638FECA5779A69100169C8558400);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6E08E58AD13E5769D3AFD33FB33C17E306122492);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7D61FA9D9BE7581D7E2EE28C775ABE0D4B8C3D69);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F94A8E8715AB28B4A3A63F016AFABE058C94FF0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral915923FF23DAC30B2D70516B4F5D56256E060201);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9BF9FDA83C54B3E4D921C2ABBCF673E2E2E37FBA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA2A3DF99788A73C2F0FE7267B988915F72D2C1F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAC2205CB3AEC457605CBAE18F9FEEECC950BD105);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB32F137D7398FFB53E5E7ACA2526882ADE8473A6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB92EF51E45166C91E2762DB6C9F27C8BD6EBE466);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCB06293E3070D888955542383617A31852FFF8DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDF17A9BFA8A9CE193E1BCDFA78953986FDA621F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147FC8F66BE740F2F8674E00CBC75BC21B73934);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEA079692DED56FA02201B916BF75CCB06628ED3B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0D9104AB624D4BF63F12ED168216DC1948D19B8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF42B6EC895E3DC70F8183E72033DF05F5B5CF6D2);
		s_Il2CppMethodInitialized = true;
	}
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_0 = NULL;
	KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		//                 return new KeyValuePair<string, ShaderProps>(
		//                     "Standard",
		//                     new ShaderProps
		//                     {
		//                         Properties = new ShaderProperty[]{
		// new ShaderProperty("_Color", ShaderPropertyType.Color)
		// ,new ShaderProperty("_MainTex", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_Cutoff", ShaderPropertyType.Range)
		// ,new ShaderProperty("_Glossiness", ShaderPropertyType.Range)
		// ,new ShaderProperty("_GlossMapScale", ShaderPropertyType.Range)
		// ,new ShaderProperty("_SmoothnessTextureChannel", ShaderPropertyType.Float)
		// ,new ShaderProperty("_Metallic", ShaderPropertyType.Range)
		// ,new ShaderProperty("_MetallicGlossMap", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_SpecularHighlights", ShaderPropertyType.Float)
		// ,new ShaderProperty("_GlossyReflections", ShaderPropertyType.Float)
		// ,new ShaderProperty("_BumpScale", ShaderPropertyType.Float)
		// ,new ShaderProperty("_BumpMap", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_Parallax", ShaderPropertyType.Range)
		// ,new ShaderProperty("_ParallaxMap", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_OcclusionStrength", ShaderPropertyType.Range)
		// ,new ShaderProperty("_OcclusionMap", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_EmissionColor", ShaderPropertyType.Color)
		// ,new ShaderProperty("_EmissionMap", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_DetailMask", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_DetailAlbedoMap", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_DetailNormalMapScale", ShaderPropertyType.Float)
		// ,new ShaderProperty("_DetailNormalMap", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_UVSec", ShaderPropertyType.Float)
		// ,new ShaderProperty("_Mode", ShaderPropertyType.Float)
		// ,new ShaderProperty("_SrcBlend", ShaderPropertyType.Float)
		// ,new ShaderProperty("_DstBlend", ShaderPropertyType.Float)
		// ,new ShaderProperty("_ZWrite", ShaderPropertyType.Float)
		// 
		//                         }
		//                     }
		//                 );
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_0 = (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*)il2cpp_codegen_object_new(ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9(L_0, NULL);
		V_0 = L_0;
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_1 = V_0;
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_2 = (ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)SZArrayNew(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var, (uint32_t)((int32_t)27));
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_3 = L_2;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_4;
		memset((&L_4), 0, sizeof(L_4));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_4), _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, 1, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_4);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_5 = L_3;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_6;
		memset((&L_6), 0, sizeof(L_6));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_6), _stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_6);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_7 = L_5;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_8;
		memset((&L_8), 0, sizeof(L_8));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_8), _stringLiteral326129114EB43E5A03AD980A3709D55FE7934916, 2, /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_8);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_9 = L_7;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_10;
		memset((&L_10), 0, sizeof(L_10));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_10), _stringLiteralA2A3DF99788A73C2F0FE7267B988915F72D2C1F1, 2, /*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_10);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_11 = L_9;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_12;
		memset((&L_12), 0, sizeof(L_12));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_12), _stringLiteralAC2205CB3AEC457605CBAE18F9FEEECC950BD105, 2, /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_12);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_13 = L_11;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_14;
		memset((&L_14), 0, sizeof(L_14));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_14), _stringLiteral51E77BE760A981E17113C4BA376BD85874B58A45, 3, /*hidden argument*/NULL);
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_14);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_15 = L_13;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_16;
		memset((&L_16), 0, sizeof(L_16));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_16), _stringLiteral7F94A8E8715AB28B4A3A63F016AFABE058C94FF0, 2, /*hidden argument*/NULL);
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_16);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_17 = L_15;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_18;
		memset((&L_18), 0, sizeof(L_18));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_18), _stringLiteralE147FC8F66BE740F2F8674E00CBC75BC21B73934, 0, /*hidden argument*/NULL);
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(7), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_18);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_19 = L_17;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_20;
		memset((&L_20), 0, sizeof(L_20));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_20), _stringLiteral38B8C8C0BD25C5963BB0276F350E52AE4F3288D3, 3, /*hidden argument*/NULL);
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(8), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_20);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_21 = L_19;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_22;
		memset((&L_22), 0, sizeof(L_22));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_22), _stringLiteral6E08E58AD13E5769D3AFD33FB33C17E306122492, 3, /*hidden argument*/NULL);
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_22);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_23 = L_21;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_24;
		memset((&L_24), 0, sizeof(L_24));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_24), _stringLiteral915923FF23DAC30B2D70516B4F5D56256E060201, 3, /*hidden argument*/NULL);
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_24);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_25 = L_23;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_26;
		memset((&L_26), 0, sizeof(L_26));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_26), _stringLiteral9BF9FDA83C54B3E4D921C2ABBCF673E2E2E37FBA, 0, /*hidden argument*/NULL);
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_26);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_27 = L_25;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_28;
		memset((&L_28), 0, sizeof(L_28));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_28), _stringLiteralF0D9104AB624D4BF63F12ED168216DC1948D19B8, 2, /*hidden argument*/NULL);
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_28);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_29 = L_27;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_30;
		memset((&L_30), 0, sizeof(L_30));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_30), _stringLiteralCB06293E3070D888955542383617A31852FFF8DF, 0, /*hidden argument*/NULL);
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_30);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_31 = L_29;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_32;
		memset((&L_32), 0, sizeof(L_32));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_32), _stringLiteral4FC108C6FF0A706B87BCEE07E7584DC45FD16B4B, 2, /*hidden argument*/NULL);
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_32);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_33 = L_31;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_34;
		memset((&L_34), 0, sizeof(L_34));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_34), _stringLiteral40BFB095782D36D1B276A40A276C76911EF9318F, 0, /*hidden argument*/NULL);
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_34);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_35 = L_33;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_36;
		memset((&L_36), 0, sizeof(L_36));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_36), _stringLiteralB92EF51E45166C91E2762DB6C9F27C8BD6EBE466, 1, /*hidden argument*/NULL);
		NullCheck(L_35);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_36);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_37 = L_35;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_38;
		memset((&L_38), 0, sizeof(L_38));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_38), _stringLiteralB32F137D7398FFB53E5E7ACA2526882ADE8473A6, 0, /*hidden argument*/NULL);
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_38);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_39 = L_37;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_40;
		memset((&L_40), 0, sizeof(L_40));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_40), _stringLiteralEA079692DED56FA02201B916BF75CCB06628ED3B, 0, /*hidden argument*/NULL);
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_40);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_41 = L_39;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_42;
		memset((&L_42), 0, sizeof(L_42));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_42), _stringLiteral58D77E1ECF3579ADA2EDE01E1640D1E1CA4A37E8, 0, /*hidden argument*/NULL);
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_42);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_43 = L_41;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_44;
		memset((&L_44), 0, sizeof(L_44));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_44), _stringLiteralDF17A9BFA8A9CE193E1BCDFA78953986FDA621F1, 3, /*hidden argument*/NULL);
		NullCheck(L_43);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_44);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_45 = L_43;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_46;
		memset((&L_46), 0, sizeof(L_46));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_46), _stringLiteral60BECFD9EBE7638FECA5779A69100169C8558400, 0, /*hidden argument*/NULL);
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_46);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_47 = L_45;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_48;
		memset((&L_48), 0, sizeof(L_48));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_48), _stringLiteralF42B6EC895E3DC70F8183E72033DF05F5B5CF6D2, 3, /*hidden argument*/NULL);
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_48);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_49 = L_47;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_50;
		memset((&L_50), 0, sizeof(L_50));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_50), _stringLiteral7D61FA9D9BE7581D7E2EE28C775ABE0D4B8C3D69, 3, /*hidden argument*/NULL);
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_50);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_51 = L_49;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_52;
		memset((&L_52), 0, sizeof(L_52));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_52), _stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F, 3, /*hidden argument*/NULL);
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_52);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_53 = L_51;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_54;
		memset((&L_54), 0, sizeof(L_54));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_54), _stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A, 3, /*hidden argument*/NULL);
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_54);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_55 = L_53;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_56;
		memset((&L_56), 0, sizeof(L_56));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_56), _stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C, 3, /*hidden argument*/NULL);
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_56);
		NullCheck(L_1);
		L_1->___Properties_0 = L_55;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___Properties_0), (void*)L_55);
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_57 = V_0;
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_58;
		memset((&L_58), 0, sizeof(L_58));
		KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209((&L_58), _stringLiteral2620038B8A71EF21A205CC921576171A3CA9B0F4, L_57, /*hidden argument*/KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		V_1 = L_58;
		goto IL_021a;
	}

IL_021a:
	{
		// }
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_59 = V_1;
		return L_59;
	}
}
// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps> UniGLTF.ShaderPropExporter.PreShaderPropExporter::get_UniGLTF_UniUnlit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 PreShaderPropExporter_get_UniGLTF_UniUnlit_mCE5DE1A4C6B99C0A1E1CDD95591AC8D2D418A84A (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral326129114EB43E5A03AD980A3709D55FE7934916);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59B259A263D1796F7617C20534034F94A19001FE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D069221DE352372E43A85A2868AE71709AFBC3F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8E54DC0584021FDD77DA842B94FD97F28B5A628);
		s_Il2CppMethodInitialized = true;
	}
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_0 = NULL;
	KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		//                 return new KeyValuePair<string, ShaderProps>(
		//                     "UniGLTF/UniUnlit",
		//                     new ShaderProps
		//                     {
		//                         Properties = new ShaderProperty[]{
		// new ShaderProperty("_MainTex", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_Color", ShaderPropertyType.Color)
		// ,new ShaderProperty("_Cutoff", ShaderPropertyType.Range)
		// ,new ShaderProperty("_BlendMode", ShaderPropertyType.Float)
		// ,new ShaderProperty("_CullMode", ShaderPropertyType.Float)
		// ,new ShaderProperty("_VColBlendMode", ShaderPropertyType.Float)
		// ,new ShaderProperty("_SrcBlend", ShaderPropertyType.Float)
		// ,new ShaderProperty("_DstBlend", ShaderPropertyType.Float)
		// ,new ShaderProperty("_ZWrite", ShaderPropertyType.Float)
		// 
		//                         }
		//                     }
		//                 );
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_0 = (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*)il2cpp_codegen_object_new(ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9(L_0, NULL);
		V_0 = L_0;
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_1 = V_0;
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_2 = (ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)SZArrayNew(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_3 = L_2;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_4;
		memset((&L_4), 0, sizeof(L_4));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_4), _stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_4);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_5 = L_3;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_6;
		memset((&L_6), 0, sizeof(L_6));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_6), _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, 1, /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_6);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_7 = L_5;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_8;
		memset((&L_8), 0, sizeof(L_8));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_8), _stringLiteral326129114EB43E5A03AD980A3709D55FE7934916, 2, /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_8);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_9 = L_7;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_10;
		memset((&L_10), 0, sizeof(L_10));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_10), _stringLiteralA69C83831B4753F9D2B4F65C16372EA1A6F0482F, 3, /*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_10);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_11 = L_9;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_12;
		memset((&L_12), 0, sizeof(L_12));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_12), _stringLiteral59B259A263D1796F7617C20534034F94A19001FE, 3, /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_12);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_13 = L_11;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_14;
		memset((&L_14), 0, sizeof(L_14));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_14), _stringLiteralC8E54DC0584021FDD77DA842B94FD97F28B5A628, 3, /*hidden argument*/NULL);
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_14);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_15 = L_13;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_16;
		memset((&L_16), 0, sizeof(L_16));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_16), _stringLiteral3708CDBCC9F390AB99D52FE7DEE4724401B69B9F, 3, /*hidden argument*/NULL);
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_16);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_17 = L_15;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_18;
		memset((&L_18), 0, sizeof(L_18));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_18), _stringLiteral73B13DE9817379145386BC6ECC87E983FC8ED41A, 3, /*hidden argument*/NULL);
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(7), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_18);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_19 = L_17;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_20;
		memset((&L_20), 0, sizeof(L_20));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_20), _stringLiteralB25CF1C6B74339FBFCE846454A70688CE58C094C, 3, /*hidden argument*/NULL);
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(8), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_20);
		NullCheck(L_1);
		L_1->___Properties_0 = L_19;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___Properties_0), (void*)L_19);
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_21 = V_0;
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_22;
		memset((&L_22), 0, sizeof(L_22));
		KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209((&L_22), _stringLiteral9D069221DE352372E43A85A2868AE71709AFBC3F, L_21, /*hidden argument*/KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		V_1 = L_22;
		goto IL_00c4;
	}

IL_00c4:
	{
		// }
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_23 = V_1;
		return L_23;
	}
}
// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps> UniGLTF.ShaderPropExporter.PreShaderPropExporter::get_Unlit_Color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 PreShaderPropExporter_get_Unlit_Color_m93986E368971B5194DF2496ABE49C7B865B9AA59 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3D93A89666F831FB9324883A9347EA29365E69DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		s_Il2CppMethodInitialized = true;
	}
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_0 = NULL;
	KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		//                 return new KeyValuePair<string, ShaderProps>(
		//                     "Unlit/Color",
		//                     new ShaderProps
		//                     {
		//                         Properties = new ShaderProperty[]{
		// new ShaderProperty("_Color", ShaderPropertyType.Color)
		// 
		//                         }
		//                     }
		//                 );
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_0 = (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*)il2cpp_codegen_object_new(ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9(L_0, NULL);
		V_0 = L_0;
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_1 = V_0;
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_2 = (ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)SZArrayNew(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var, (uint32_t)1);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_3 = L_2;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_4;
		memset((&L_4), 0, sizeof(L_4));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_4), _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, 1, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_4);
		NullCheck(L_1);
		L_1->___Properties_0 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___Properties_0), (void*)L_3);
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_5 = V_0;
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_6;
		memset((&L_6), 0, sizeof(L_6));
		KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209((&L_6), _stringLiteral3D93A89666F831FB9324883A9347EA29365E69DF, L_5, /*hidden argument*/KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		V_1 = L_6;
		goto IL_0033;
	}

IL_0033:
	{
		// }
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_7 = V_1;
		return L_7;
	}
}
// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps> UniGLTF.ShaderPropExporter.PreShaderPropExporter::get_Unlit_Texture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 PreShaderPropExporter_get_Unlit_Texture_m959E25F41FC1A14BD3136BFAC4A92FC57CEC2C6F (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral684F9F5743C3D1DE5AF358333F20040FBA298574);
		s_Il2CppMethodInitialized = true;
	}
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_0 = NULL;
	KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		//                 return new KeyValuePair<string, ShaderProps>(
		//                     "Unlit/Texture",
		//                     new ShaderProps
		//                     {
		//                         Properties = new ShaderProperty[]{
		// new ShaderProperty("_MainTex", ShaderPropertyType.TexEnv)
		// 
		//                         }
		//                     }
		//                 );
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_0 = (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*)il2cpp_codegen_object_new(ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9(L_0, NULL);
		V_0 = L_0;
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_1 = V_0;
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_2 = (ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)SZArrayNew(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var, (uint32_t)1);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_3 = L_2;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_4;
		memset((&L_4), 0, sizeof(L_4));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_4), _stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_4);
		NullCheck(L_1);
		L_1->___Properties_0 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___Properties_0), (void*)L_3);
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_5 = V_0;
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_6;
		memset((&L_6), 0, sizeof(L_6));
		KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209((&L_6), _stringLiteral684F9F5743C3D1DE5AF358333F20040FBA298574, L_5, /*hidden argument*/KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		V_1 = L_6;
		goto IL_0033;
	}

IL_0033:
	{
		// }
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_7 = V_1;
		return L_7;
	}
}
// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps> UniGLTF.ShaderPropExporter.PreShaderPropExporter::get_Unlit_Transparent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 PreShaderPropExporter_get_Unlit_Transparent_mC40D52AEEC010A48CC957F963D320C52CEFC51BB (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB9AD78CBFE96EFE3227B6F467DA563E5F6136C6B);
		s_Il2CppMethodInitialized = true;
	}
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_0 = NULL;
	KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		//                 return new KeyValuePair<string, ShaderProps>(
		//                     "Unlit/Transparent",
		//                     new ShaderProps
		//                     {
		//                         Properties = new ShaderProperty[]{
		// new ShaderProperty("_MainTex", ShaderPropertyType.TexEnv)
		// 
		//                         }
		//                     }
		//                 );
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_0 = (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*)il2cpp_codegen_object_new(ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9(L_0, NULL);
		V_0 = L_0;
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_1 = V_0;
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_2 = (ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)SZArrayNew(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var, (uint32_t)1);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_3 = L_2;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_4;
		memset((&L_4), 0, sizeof(L_4));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_4), _stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_4);
		NullCheck(L_1);
		L_1->___Properties_0 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___Properties_0), (void*)L_3);
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_5 = V_0;
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_6;
		memset((&L_6), 0, sizeof(L_6));
		KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209((&L_6), _stringLiteralB9AD78CBFE96EFE3227B6F467DA563E5F6136C6B, L_5, /*hidden argument*/KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		V_1 = L_6;
		goto IL_0033;
	}

IL_0033:
	{
		// }
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_7 = V_1;
		return L_7;
	}
}
// System.Collections.Generic.KeyValuePair`2<System.String,UniGLTF.ShaderPropExporter.ShaderProps> UniGLTF.ShaderPropExporter.PreShaderPropExporter::get_Unlit_Transparent_Cutout()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 PreShaderPropExporter_get_Unlit_Transparent_Cutout_m8DEC8CDF0C548C02C13032305E89AC59D936DB6C (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral326129114EB43E5A03AD980A3709D55FE7934916);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5EAA19E6B3D79923759AC0F3C611403170C03B32);
		s_Il2CppMethodInitialized = true;
	}
	ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* V_0 = NULL;
	KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		//                 return new KeyValuePair<string, ShaderProps>(
		//                     "Unlit/Transparent Cutout",
		//                     new ShaderProps
		//                     {
		//                         Properties = new ShaderProperty[]{
		// new ShaderProperty("_MainTex", ShaderPropertyType.TexEnv)
		// ,new ShaderProperty("_Cutoff", ShaderPropertyType.Range)
		// 
		//                         }
		//                     }
		//                 );
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_0 = (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD*)il2cpp_codegen_object_new(ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9(L_0, NULL);
		V_0 = L_0;
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_1 = V_0;
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_2 = (ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88*)SZArrayNew(ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88_il2cpp_TypeInfo_var, (uint32_t)2);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_3 = L_2;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_4;
		memset((&L_4), 0, sizeof(L_4));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_4), _stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_4);
		ShaderPropertyU5BU5D_t76DB773FA1AC219BD55C783B3E816C7AF743DB88* L_5 = L_3;
		ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F L_6;
		memset((&L_6), 0, sizeof(L_6));
		ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4((&L_6), _stringLiteral326129114EB43E5A03AD980A3709D55FE7934916, 2, /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F)L_6);
		NullCheck(L_1);
		L_1->___Properties_0 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___Properties_0), (void*)L_5);
		ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* L_7 = V_0;
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_8;
		memset((&L_8), 0, sizeof(L_8));
		KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209((&L_8), _stringLiteral5EAA19E6B3D79923759AC0F3C611403170C03B32, L_7, /*hidden argument*/KeyValuePair_2__ctor_mABF7BF43082168BB4EA4276DAD0834701EAE7209_RuntimeMethod_var);
		V_1 = L_8;
		goto IL_0045;
	}

IL_0045:
	{
		// }
		KeyValuePair_2_tC3860D570AA1803EA9796F1D8FC88FF820D35802 L_9 = V_1;
		return L_9;
	}
}
// System.Void UniGLTF.ShaderPropExporter.PreShaderPropExporter::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreShaderPropExporter__cctor_mF6B25D5731C5AA983305EDF5A76F37835E2C1040 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2620038B8A71EF21A205CC921576171A3CA9B0F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3D93A89666F831FB9324883A9347EA29365E69DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5EAA19E6B3D79923759AC0F3C611403170C03B32);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral684F9F5743C3D1DE5AF358333F20040FBA298574);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D069221DE352372E43A85A2868AE71709AFBC3F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB9AD78CBFE96EFE3227B6F467DA563E5F6136C6B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static SupportedShader[] SupportedShaders = new SupportedShader[]
		// {
		//     new SupportedShader(TARGET_FOLDER, "Standard"),
		//     new SupportedShader(TARGET_FOLDER, "Unlit/Color"),
		//     new SupportedShader(TARGET_FOLDER, "Unlit/Texture"),
		//     new SupportedShader(TARGET_FOLDER, "Unlit/Transparent"),
		//     new SupportedShader(TARGET_FOLDER, "Unlit/Transparent Cutout"),
		//     new SupportedShader(TARGET_FOLDER, "UniGLTF/UniUnlit"),
		// };
		SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* L_0 = (SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C*)(SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C*)SZArrayNew(SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C_il2cpp_TypeInfo_var, (uint32_t)6);
		SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* L_1 = L_0;
		SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD L_2;
		memset((&L_2), 0, sizeof(L_2));
		SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46((&L_2), _stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E, _stringLiteral2620038B8A71EF21A205CC921576171A3CA9B0F4, /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD)L_2);
		SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* L_3 = L_1;
		SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD L_4;
		memset((&L_4), 0, sizeof(L_4));
		SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46((&L_4), _stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E, _stringLiteral3D93A89666F831FB9324883A9347EA29365E69DF, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD)L_4);
		SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* L_5 = L_3;
		SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD L_6;
		memset((&L_6), 0, sizeof(L_6));
		SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46((&L_6), _stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E, _stringLiteral684F9F5743C3D1DE5AF358333F20040FBA298574, /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD)L_6);
		SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* L_7 = L_5;
		SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD L_8;
		memset((&L_8), 0, sizeof(L_8));
		SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46((&L_8), _stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E, _stringLiteralB9AD78CBFE96EFE3227B6F467DA563E5F6136C6B, /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD)L_8);
		SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* L_9 = L_7;
		SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD L_10;
		memset((&L_10), 0, sizeof(L_10));
		SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46((&L_10), _stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E, _stringLiteral5EAA19E6B3D79923759AC0F3C611403170C03B32, /*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD)L_10);
		SupportedShaderU5BU5D_t9AF4D633766F24E2E986D043F44B96099E2AEC4C* L_11 = L_9;
		SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD L_12;
		memset((&L_12), 0, sizeof(L_12));
		SupportedShader__ctor_m4B124BA4FD1C9016E02B29151C87AFF920CF0A46((&L_12), _stringLiteral6B53D108B835E0DFF5B2DCB4F56BA198075BD11E, _stringLiteral9D069221DE352372E43A85A2868AE71709AFBC3F, /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (SupportedShader_tDF6B0456824F5659BCE7DD2C6288B8575CD91CCD)L_12);
		((PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields*)il2cpp_codegen_static_fields_for(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var))->___SupportedShaders_1 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&((PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_StaticFields*)il2cpp_codegen_static_fields_for(PreShaderPropExporter_t33693D745BAEF8D0D7281EB7F8A08627BB61A5D7_il2cpp_TypeInfo_var))->___SupportedShaders_1), (void*)L_11);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UniGLTF.ShaderPropExporter.ShaderProperty
IL2CPP_EXTERN_C void ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshal_pinvoke(const ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F& unmarshaled, ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_pinvoke& marshaled)
{
	marshaled.___Key_0 = il2cpp_codegen_marshal_string(unmarshaled.___Key_0);
	marshaled.___ShaderPropertyType_1 = unmarshaled.___ShaderPropertyType_1;
}
IL2CPP_EXTERN_C void ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshal_pinvoke_back(const ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_pinvoke& marshaled, ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F& unmarshaled)
{
	unmarshaled.___Key_0 = il2cpp_codegen_marshal_string_result(marshaled.___Key_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___Key_0), (void*)il2cpp_codegen_marshal_string_result(marshaled.___Key_0));
	int32_t unmarshaledShaderPropertyType_temp_1 = 0;
	unmarshaledShaderPropertyType_temp_1 = marshaled.___ShaderPropertyType_1;
	unmarshaled.___ShaderPropertyType_1 = unmarshaledShaderPropertyType_temp_1;
}
// Conversion method for clean up from marshalling of: UniGLTF.ShaderPropExporter.ShaderProperty
IL2CPP_EXTERN_C void ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshal_pinvoke_cleanup(ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Key_0);
	marshaled.___Key_0 = NULL;
}
// Conversion methods for marshalling of: UniGLTF.ShaderPropExporter.ShaderProperty
IL2CPP_EXTERN_C void ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshal_com(const ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F& unmarshaled, ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_com& marshaled)
{
	marshaled.___Key_0 = il2cpp_codegen_marshal_bstring(unmarshaled.___Key_0);
	marshaled.___ShaderPropertyType_1 = unmarshaled.___ShaderPropertyType_1;
}
IL2CPP_EXTERN_C void ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshal_com_back(const ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_com& marshaled, ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F& unmarshaled)
{
	unmarshaled.___Key_0 = il2cpp_codegen_marshal_bstring_result(marshaled.___Key_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___Key_0), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___Key_0));
	int32_t unmarshaledShaderPropertyType_temp_1 = 0;
	unmarshaledShaderPropertyType_temp_1 = marshaled.___ShaderPropertyType_1;
	unmarshaled.___ShaderPropertyType_1 = unmarshaledShaderPropertyType_temp_1;
}
// Conversion method for clean up from marshalling of: UniGLTF.ShaderPropExporter.ShaderProperty
IL2CPP_EXTERN_C void ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshal_com_cleanup(ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Key_0);
	marshaled.___Key_0 = NULL;
}
// System.Void UniGLTF.ShaderPropExporter.ShaderProperty::.ctor(System.String,UniGLTF.ShaderPropExporter.ShaderPropertyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4 (ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F* __this, String_t* ___0_key, int32_t ___1_propType, const RuntimeMethod* method) 
{
	{
		// Key = key;
		String_t* L_0 = ___0_key;
		__this->___Key_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Key_0), (void*)L_0);
		// ShaderPropertyType = propType;
		int32_t L_1 = ___1_propType;
		__this->___ShaderPropertyType_1 = L_1;
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4_AdjustorThunk (RuntimeObject* __this, String_t* ___0_key, int32_t ___1_propType, const RuntimeMethod* method)
{
	ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ShaderProperty_t8AF0BC217D0AAB6AEB3677FF49525DC026A7617F*>(__this + _offset);
	ShaderProperty__ctor_mF395A1784DE7CB9F30FAF5A3DD50082D54C7B7B4(_thisAdjusted, ___0_key, ___1_propType, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniGLTF.ShaderPropExporter.ShaderProps::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderProps__ctor_m5D520C4E1B5972147C2E598508BBFFF21346F4E9 (ShaderProps_t9D85E1200BE9F93CE93B7565562D2AE0798DE1CD* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t U3CPrivateImplementationDetailsU3E_ComputeStringHash_m6EA1F233618497AEFF8902A5EDFA24C74E2F2876 (String_t* ___0_s, const RuntimeMethod* method) 
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___0_s;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		V_0 = ((int32_t)-2128831035);
		V_1 = 0;
		goto IL_0021;
	}

IL_000d:
	{
		String_t* L_1 = ___0_s;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppChar L_3;
		L_3 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_1, L_2, NULL);
		uint32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_multiply(((int32_t)((int32_t)L_3^(int32_t)L_4)), ((int32_t)16777619)));
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0021:
	{
		int32_t L_6 = V_1;
		String_t* L_7 = ___0_s;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_7, NULL);
		if ((((int32_t)L_6) >= ((int32_t)L_8)))
		{
			goto IL_002c;
		}
	}
	{
		goto IL_000d;
	}

IL_002c:
	{
		uint32_t L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UnityPath_get_Value_m18F0BD7C9105F992163CF73668CAEFD35BFA288C_inline (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, const RuntimeMethod* method) 
{
	{
		// get;
		String_t* L_0 = __this->___U3CValueU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityPath_set_Value_m78AD9ACD3EBE2712996FC92B09D5C681648681D6_inline (UnityPath_tBB06837EBF3994538D4CB0D1C3DAC6A80D37E044* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		// private set;
		String_t* L_0 = ___0_value;
		__this->___U3CValueU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CValueU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___0_item;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* KeyValuePair_2_get_Key_mBD8EA7557C27E6956F2AF29DA3F7499B2F51A282_gshared_inline (KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->___key_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* KeyValuePair_2_get_Value_mC6BD8075F9C9DDEF7B4D731E5C38EC19103988E7_gshared_inline (KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->___value_1;
		return L_0;
	}
}
