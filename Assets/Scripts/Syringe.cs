using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Syringe : MonoBehaviour
{
    public Rigidbody SyringeRigidbody;
    public bool syringeActicated = false;
    private void Start()
    {
        SyringeRigidbody = GetComponent<Rigidbody>();
    }

    public void SyringeEvent(bool syringeActicated)
    {
        
    }
    private void Update()
    {
        if (syringeActicated)
        {
            SyringeRigidbody.constraints = RigidbodyConstraints.FreezePosition;
            Debug.Log("Activated");
            StartCoroutine(SyringeDisable());
        }
        else
        {
            SyringeRigidbody.constraints = RigidbodyConstraints.None;
            Debug.Log("Deactivated");
        }
    }
    private IEnumerator SyringeDisable()
    {
        yield return new WaitForSeconds(3f);
        syringeActicated = false;
    }
}
